#!/bin/bash
#version:1.0
######################################
#arguments: cluster --retain-state
#for example: new-cluster.tsk k1000
##########################################
#set general variables
#some fictional change
source .generic-code/variables/start-variables

active_scripts=0
clusters=$empty
clusters_count=0
retain_state=$false
argument_retain_state=$empty
successes=$empty
fails=$empty
clusterfails=0 #different from failed by name for specific use cases
failed_clusters_by_name=$empty
fail_after_evaluate=$false
time_taken=0
uplist=$empty
arguments=$empty
k8s_initiation_failed=$false
clusterbreaks=$empty
breaking_for_testing=$false
breaking_reason=$empty
debug_setting=$false
aboards=$empty
early_aboard=$false
early_reason=$empty
evaluation_timeout=0
evaluation_has_timedout=$false
generic_list=$empty
fails_result=$empty
verbose_evaluation_loop=$false
leave_state_for_testing=$false
only_evaluate_for_testing=$false
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function report_time() {
    text=$*
    time_taken=$(( $(get_now) - $timer_start ))
    echo $text" "$time_taken" seconds"

}
function report_delta_time() {
    delta_start_time=$1
    itter=2
    text=$empty
    while [[ $itter -lt $(( $# + 1 )) ]]; do
        if [[ $text != $empty ]]; then text=$text" "; fi
        text=$text$(eval echo "\${$itter}")
        itter=$(( $itter + 1 ))
    done

    time_taken=$(( $(get_now) - $delta_start_time ))
    echo -e ${WHITE}${ITALIC}$text"${NC} "$time_taken" seconds"

    report_time "Total time taken so far is"
}
function do_fail_after_evaluation() {
    exit 99
}
function get_fails() {
    criterium=$1
    fails=$empty
    pids=$empty
    fails_result=$(cat $progress_file | grep fail | grep $criterium)

    for fail in $fails_result; do
        fail=${fail//_/ }
        for argument in $fail; do 
            fail_pid=$empty
            if [[ $argument == *"):"* ]]; then
                fail_pid=${argument//\):/}
                fail_pid=${fail_pid//\(/}
            fi
            if [[ $fail_pid == $empty && $argument == *"),"* ]]; then
                fail_pid=${argument//\),/}
                fail_pid=${fail_pid//\(/}
            fi
            if [[ $fail_pid != $empty ]]; then
                pids=$pids" "$fail_pid
            fi
        done
    done
    pids=$(make_unique $pids)
    fails=$pids
    create_list_count fails
}
function get_successes() {
    criterium=$1
    successes=$empty
    pids=$empty
    s_results=$(cat $progress_file | grep "successfully" | grep $criterium)

    for success in $s_results; do
        success=${success//_/ }
        for argument in $success; do 
            success_pid=$empty
            if [[ $argument == *"):"* ]]; then
                success_pid=${argument//\):/}
                success_pid=${success_pid//\(/}
            fi
            if [[ $success_pid == $empty && $argument == *"),"* ]]; then
                success_pid=${argument//\),/}
                success_pid=${success_pid//\(/}
            fi
            if [[ $success_pid != $empty ]]; then
                pids=$pids" "$success_pid
            fi
        done
    done
    pids=$(make_unique $pids)
    successes=$pids
    create_list_count successes
}
function get_failed_clusters_by_name_by_name() {
    pids=$empty
    failures=$(cat $progress_file | grep fail)
    failed_clusters_by_name=$empty

    for fail in $failures; do
        #if [[ $fail != *"dns_registry_failed"* ]]; then
            fail=${fail//_/ }
            for cluster in $clusters; do
                if [[ $fail == *"$cluster"* ]]; then
                    failed_clusters_by_name=$failed_clusters_by_name" "$cluster
                fi
            done
        #fi
    done
    failed_clusters_by_name=$(make_unique $failed_clusters_by_name)
    create_list_count failed_clusters_by_name
}
function test_for_early_aboard() {
    aboards=$(cat $progress_file | grep "early_aboard_ordered")
    early_reason=$aboards
    create_list_count aboards
    
    if [[ $aboards_count -eq $clusters_count ]]; then  
        early_aboard=$true

        #extract reason
        early_reason=${early_reason#*:} #get all after :
        early_reason=${early_reason%%_enabled,*} #get all before first ,
        early_reason=${early_reason:1} #remove first character
    fi
}
function test_stop_after_storage() {
    is_stop_after_storage=$false

    generic_list=$(cat $progress_file | grep "successfully" | grep "debug_setting_only-print-join-initiation_is_true")
    create_list_count generic_list

    if [[ $generic_list_count -eq $clusters_count ]]; then    
        is_stop_after_storage=$true        
        breaking_for_testing=$true    
    fi

    generic_list=$(cat $progress_file | grep "successfully" | grep "only-print-joining-nodes_is_true")
    create_list_count generic_list

    if [[ $generic_list_count -eq $clusters_count ]]; then    
        is_stop_after_storage=$true        
        breaking_for_testing=$true    
    fi
}
function kubernetes_initiation_failed() {
    initiation_did_fail=$false
    clusterfails=0

    clusterfails=$(cat $progress_file | grep "fail" | grep "k8s_initiation_failed,_unable_to_continue")
    create_list_count clusterfails

    if [[ $clusterfails_count -eq $clusters_count ]]; then    
        initiation_did_fail=$true        
    fi

    echo $initiation_did_failed
}
function breaking_for_test() {

    clusterbreaks=$(cat $progress_file | grep "breaking_out_for_testing")
    breaking_reason=$clusterbreaks
    create_list_count clusterbreaks

    if [[ $clusterbreaks_count -eq $clusters_count ]]; then    
        breaking_for_testing=$true

        #extract reason
        breaking_reason=${breaking_reason#*:} #get all after :
        breaking_reason=${breaking_reason%%_encountered,*} #get all before first ,
        breaking_reason=${breaking_reason:1} #remove first character
    fi
}
function evaluate_progress() {
    evaluation_criterium=$1
    evaluation_loop_count=0
    fail_after_evaluate=$false
    create_list_count clusters
    if [[ $clusters_count -eq 0 ]]; then
        echo "$me ($pid), evaluate progress: no clusters anymore in clusters-list, nothing to do, aboarding install"
        exit 11
    fi

    finished=$false
    result_count=0
    shout=0
    evaluation_startime=$(get_now)
    evaluation_has_timedout=$false

    k8s_initiation_failed=$false
    breaking_for_testing=$false
    early_aboard=$false
    while [[ $finished == $false ]]; do
        if [[ $verbose_evaluation_loop == $true ]]; then
            echo "$me ($pid), evaluate progress: result count is $result_count, clusters count is $clusters_count, loopcount $evaluation_loop_count - $(date)"
        fi
        if [[ $result_count -eq $clusters_count || $result_count -gt $clusters_count ]]; then
            if [[ $clusters_count -eq 1 && $fails_count -gt 0 ]]; then
                non_breaking_failure=${fails_result//_/ }
                non_breaking_failure=${non_breaking_failure#*$nbf -} #get all after -
                non_breaking_failure=${non_breaking_failure%%-*} #get all before first ,
                echo -e "${EXCLAMATION} ${RED}${BOLD}$me ($pid): failure ${NC}${BLUE}${BOLD}${ITALIC}$non_breaking_failure${NC}${RED}${BOLD} elevated too breaking${NC}${YELLOW}${BOLD}, aboarding provisioning${NC}"
                exit 99
            elif [[ $fails_count -gt 0 ]]; then
                check_nbf_on_breaking_failures $evaluation_criterium
            fi

            finished=$true
        elif [[ $early_aboard == $true ]]; then
            finished=$true
        elif [[ $breaking_for_testing == $true ]]; then
            finished=$true
        else
            sleep 10
            evaluation_loop_count=$(( $evaluation_loop_count + 1 ))
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium evaluation loopcount $evaluation_loop_count - $(date)"
            fi
            
            #succes and failures
            get_successes $evaluation_criterium
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium successes count is $successes_count (loopcount $evaluation_loop_count)"
            fi
            get_fails $evaluation_criterium
            if [[ $fails_count -gt 0 ]]; then
                get_failed_clusters_by_name_by_name
            fi
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium fails count is $fails_count (loopcount $evaluation_loop_count)"
            fi
            #succes and failures end

            #specific fail cases
            if [[ $evaluation_criterium = "initiate-kubernetes-cluster" ]]; then
                k8s_initiation_failed=$(kubernetes_initiation_failed)
                if [[ $verbose_evaluation_loop == $true ]]; then
                    echo "$me ($pid), evaluate progress: $evaluation_criterium kubernetes-initiations-failed is $kubernetes_initiation_failed $ (loopcount $evaluation_loop_count)"
                fi
                if [[ $k8s_initiation_failed == $true ]];then
                    if [[ $clusters_count -eq 1 ]]; then
                        echo -e "${CHECK_MARK} ${RED}${BOLD}$me ($pid), $evaluation_criterium evaluate: kubernetes cluster ${clusters[0]} initiation failed, aboarding provisioning ${NC}"
                    elif [[ $k8s_initiation_failed == $true && $clusters_count -eq 1 ]]; then
                        echo -e "${CHECK_MARK} ${RED}${BOLD}$me ($pid), $evaluation_criterium evaluate: all kubernetes clusters iniatation failed, so none of the cluster ${cluster[@]} is avialable, aboarding provisioning ${NC}"
                    fi
                    fail_after_evaluate=$true
                fi
            fi
            #specific fail cases end

            #generic stops checks
            test_for_early_aboard
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium test-for-early-aboard is $early_aboard (loopcount $evaluation_loop_count)"
            fi
            test_stop_after_storage
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium test-stop-after-storage is $is_stop_after_storage (loopcount $evaluation_loop_count)"
            fi
            breaking_for_test
            if [[ $verbose_evaluation_loop == $true ]]; then
                echo "$me ($pid), evaluate progress: $evaluation_criterium breaking-for-test is $breaking_for_testing (loopcount $evaluation_loop_count)"
            fi
            #generic stops checks end

            result_count=$(( $successes_count + $fails_count ))
            shouttime=$(( $(get_now) - $shout ))
            if [[ $shouttime -gt 100 ]]; then
                shout=$(get_now)
                timein=$(delta_time)" "seconds
                echo "$me ($pid) $timein in: fase $evaluation_criterium  $result_count/$clusters_count finished clusters (successes $successes_count - fails $fails_count)"
            fi
        fi

        if [[ $evaluation_timeout -gt 0 ]]; then
            the_now=$(get_now)
            elapsed_evaluation_time=$(( $the_now - $evaluation_starttime ))
            if [[ $elapsed_evaluation_time -gt $evaluation_timeout ]]; then
                evaluation_has_timedout=$true
                break;
            fi
        fi
    done
    
    if [[ $evaluation_has_timedout == $true ]]; then
        echo -e "${CHECK_MARK} ${RED}${BOLD}$me ($pid), evaluate progress: failed delivering clusters $cluster, evaluating timeout for step $evaluation_criterium has exceeded $evaluation_timeout seconds limit; cluster $clusters deployment has timeout. ($successes_count successfull - pids: $successes, $fails_count failed - pids: $fails)${NC}"
        exit 50
    fi
    if [[ $verbose_evaluation_loop == $true ]]; then
        echo "$me ($pid), evaluate progress: finished evaluating in $timein seconds, final status: $result_count/$clusters_count finished clusters (successes $successes_count - fails $fails_count)"
    fi
    if [[ $fails_count -gt 0 ]]; then
        exclude_fails_from_clusterlist
    fi

    #finish evaluation actions
    r_message=$empty
    case $evaluation_criterium in

    start-new-cluster-infrastructure)
        r_message="initiating cluster server infrastructure has taken"
        success_message="successfully provisioned infrastructure for clusters: $clusters"
        ;;

    install-server-software)
        r_message="installing cluster servers has taken"
        success_message="server software provisioning succeeded, successfully installed software on infrastructure servers for clusters ($clusters)."
        ;;

    infrastructure-prepare-storage)
        r_message="preparing infrastructure for storage has taken"
        success_message="successfully prepared infrastructure for federated storage (clusters: $clusters)"
        ;;

    initiate-kubernetes-cluster)
        r_message="initiating & configuring base Kubernetes cluster(s) has taken"
        success_message="successfully initiated Kubernetes clusters ($clusters)"
        ;;

    add-clusters-software)
        r_message="installing software on Kubernetes has taken"
        success_message="successfully provisioned Kubernetes clusters for clusters $clusters."
        ;;

    esac
    report_delta_time $fase_count_start_time $r_message


    #break out criteria
    if [[ $fail_after_evaluate == $true ]]; then do_fail_after_evaluation; fi
    if [[ $breaking_for_testing == $true ]]; then
        echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$me ($pid): executing $evaluation_criterium, $breaking_reason triggered, ${NC}${RED}${BOLD}'breaking for testing' ${NC}${LIGHT_GREEN}${BOLD}enabled, aboarding provisioning${NC}"
        exit 0
    fi
    if [[ $early_aboard == $true ]]; then
        echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$me ($pid): executing $evaluation_criterium, $early_reason triggered, ${NC}${RED}${BOLD}'early aboard' ${NC}${LIGHT_GREEN}${BOLD}, aboarding provisioning${NC}"
        exit 0
    fi
    
    success_message="$me ($pid): $success_message"
    echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$success_message, $successes_count successfull - pids: $successes, $fails_count failed - pids: $fails${NC}"
    reset_progress_administration
    #initiate count for new step
    fase_count_start_time=$(get_now)
}
function exclude_fails_from_clusterlist() {
    echo "excluding failed clusters '$failed_clusters_by_name' from cluster list: $clusters"
    new_clusters=$empty
    for cluster in $clusters; do
        failedcluster=$false
        for failed_cluster in $failed_clusters_by_name; do
            if [[ $cluster == $failed_cluster ]]; then
                failedcluster=$true
            fi
        done
        if [[ $failedcluster == $false ]]; then
            new_clusters=$new_clusters" "$cluster
        fi
    done
    clusters=$new_clusters
    create_list_count clusters
}
function initiate_state() {
    if [[ $leave_state_for_testing == $false ]]; then
        remove_state_dir
        secure_state_dir
        if [[ -d $state_dir.old ]]; then mv $state_dir.old $state_dir/$state_dir.old; fi
    fi
}
function secure_state_dir() {
    if [[ ! -d $state_dir ]]; then
        mkdir $state_dir
    else
        rm $state_dir/.progress
    fi
    touch $state_dir/.progress
    start_cluster_state
}
function remove_state_dir() {
    if [[ -d $state_dir/$cluster ]]; then 
        if [[ $retain_state == $true ]]; then
            cp -r $state_dir $state_dir.old
        fi
        for cluster in $clusters; do
            rm -rf $state_dir/$cluster
        done
    fi
}
function start_cluster_state(){
    i_runtime_state=$runtime_state
    active_loadbalancers_list=$(bash .utility/loadbalancer-wrappers/api/get_active_loadbalancers_list.tsk)
    active_loadbalancers_list=${active_loadbalancers_list// /#}

    for cluster in $clusters; do
        runtime_state_dir=$state_dir/$cluster
        runtime_state=$runtime_state_dir/$i_runtime_state
	    if [[ ! -d $runtime_state_dir ]]; then mkdir -p $runtime_state_dir; fi
	    if [[ ! -f $runtime_state ]]; then touch $runtime_state; fi
        echo "loadbalancers=\"$active_loadbalancers_list\"" >> $runtime_state
    done
}
function reset_progress_administration() {
    rm $state_dir/.progress
    touch $state_dir/.progress
}
function get_activescripts() {
	active_scripts=0

	find="$me.tsk"
	scriptsactive=$(ps aux | grep $find | wc -l)
	active_scripts=$(( $scriptsactive - 3 ))
	echo $active_scripts
}
function check_bussinesslogic() {
    for cluster in $clusters; do
        if [[ $kubernetes_container_runtime == "crio" && $kubernetes_version == $empty ]]; then
            echo "$me ($pid), cluster $cluster: dependency conflict, incompliant: when kubernetes runtime cri-o is used, the kubernetes version has to be specified. unable to proceed, aboarding provisioning."
            exit 0
        fi
    done
}
function set_clusters_process_data() {
    #we have two or more clusters to deploy, so multiple config files: determine processs triggers!
    #rule: a (process-)triggers is true, when true in all configs
    if [[ $clusters_count -eq 1 ]]; then
        
        load_cluster_settings_dynamic ${clusters[0]}

    else #determine to use variable value based on all clusters settings
        
        #initiate all variable lists
        stop_after_infrastructure_provisioning_list=$empty
        initialize_cluster=$empty

        #create lists
        for cluster in $clusters; do
            list=($(cat config/$cluster/settings))
            for variable in ${list[@]}; do
                first_character=${variable:0:1}
                if [[ $first_character != "#" ]]; then
                    variable=${variable//#/ }
                    key=${variable%=*} #get all before =
                    value=${variable#*=} #get all after =
                    if [[ $key == "stop_after_infrastructure_provisioning" ]]; then
                        stop_after_infrastructure_provisioning_list=$stop_after_infrastructure_provisioning_list" "$value
                    elif [[ $key == "initialize_cluster" ]]; then
                        initialize_cluster_list=$initialize_cluster_list" "$value
                    fi
                fi
            done
        done

        #set variables by evaluating results
        stop_after_infrastructure_provisioning=$true
        for stop_provisioning in $stop_after_infrastructure_provisioning_list; do
            if [[ $stop_provisioning == "false" ]];then
                stop_after_infrastructure_provisioning=$false
            fi
        done
        initialize_cluster=$true
        for before_initiation in $initialize_cluster_list; do
            if [[ $before_initiation == "false" ]];then
                initialize_cluster=$false
            fi
        done
    fi
}
function check_nbf_on_breaking_failures(){
    #non-breaking-failures become breaking when the same one happens in all clusters being provisioned
    evaluation_criterium=$1

    #collect nbf causes
    nbf_list=$empty
    nbf_results=$(cat $progress_file | grep $nbf)
    for non_breaking_failure in $nbf_results; do
        non_breaking_failure=${non_breaking_failure//_/ }
        non_breaking_failure=${non_breaking_failure#*$nbf -} #get all after -
        non_breaking_failure=${non_breaking_failure%%-*} #get all before first ,
        nbf_list=$nbf_list" "$non_breaking_failure
    done
    nbf_list=$(make_unique $nbf_list)

    for non_breaking_failure in $nbf_list; do
        generic_list=$(cat $progress_file | grep $non_breaking_failure)
        create_list_count generic_list
        if [[ $generic_list_count -eq $clusters_count ]]; then
            #to simpel solution for now: get out by the same number
            #should check if all deployed clusters are represented in list
            echo -e "${EXCLAMATION} ${RED}${BOLD}$me ($pid): check_nbf_on_breaking_failures $evaluation_criterium, failure ${NC}${BLUE}${BOLD}${ITALIC}$non_breaking_failure${NC}${RED}${BOLD} elevated too breaking${NC}${YELLOW}${BOLD}, aboarding provisioning${NC}"
            exit 99
        fi
    done
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
max_arguments=$(( $# + 1 ))
while [[ $itter -lt $max_arguments ]]; do
    argument=$(eval echo "\${$itter}")
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--retain-state" ]]; then
            retain_state=$true
            argument_retain_state=--retain-state
        fi
    else
        if [[ $clusters == $empty ]]; then
            clusters=$argument
        else 
            clusters=$clusters" "$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
create_list_count clusters

initiate_state

first_cluster=($clusters)
first_cluster=${first_cluster//k/}
if [[ $first_cluster -lt 50 ]]; then
    #delegate execution to gitlab provisioning
    bash .gitlab-ci/google-training.tsk
    exit 0
fi

load_default_settings

scriptsactive=$(ps aux | grep $me.tsk | wc -l)
active_scripts=$(( $scriptsactive - 3 ))
if [[ $active_scripts -gt 0 ]]; then
    active_scripts_error
fi

check_bussinesslogic
set_clusters_process_data
#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"

if [[ $only_evaluate_for_testing == $true ]]; then
    echo "$me ($pid), before evaluation testing start"
    evaluate_progress initiate-kubernetes-cluster
    
    echo "$me ($pid), evaluation testing: evaluation ended leaving test"
    exit 99
fi

fase_count_start_time=$(get_now)
arguments=$empty
if [[ $retain_state == $true ]]; then
    arguments=$arguments" "$argument_retain_state
fi
if [[ $kuber_logon_required == $true ]]; then arguments=$arguments" --logon-needs-k"; fi

#creating infrastructure
for cluster in $clusters; do
    #echo "$me ($pid): bash infrastructure/start-new-cluster.tsk $cluster $arguments &"
    bash infrastructure/start-new-cluster-infrastructure.tsk $cluster $arguments &
done
evaluate_progress start-new-cluster-infrastructure
if [[ $stop_after_infrastructure_provisioning == $true ]]; then
        echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$me ($pid): 'stop_after_infrastructure_provisioning' triggered, ${NC}${RED}${BOLD}'early aboard' ${NC}${LIGHT_GREEN}${BOLD}, aboarding provisioning${NC}"
        exit 0
fi

for cluster in $clusters; do
    echo "bash cluster-software-and-initiation/install-server-software.tsk $cluster $arguments &"
    bash cluster-software-and-initiation/install-server-software.tsk $cluster $arguments &
done
evaluate_progress install-server-software
if [[ $initialize_cluster == $false ]]; then
        echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$me ($pid): 'initialize_cluster' triggered, ${NC}${RED}${BOLD}'early aboard' ${NC}${LIGHT_GREEN}${BOLD}, aboarding provisioning${NC}"
        exit 0
fi

for cluster in $clusters; do
    echo "bash cluster-software-and-initiation/infrastructure-prepare-storage.tsk $cluster $arguments &"
    bash cluster-software-and-initiation/infrastructure-prepare-storage.tsk $cluster $arguments &
done
evaluate_progress infrastructure-prepare-storage

for cluster in $clusters; do
    echo "bash cluster-software-and-initiation/initiate-kubernetes-cluster.tsk $cluster $arguments &"
    bash cluster-software-and-initiation/initiate-kubernetes-cluster.tsk $cluster $arguments &
done
evaluate_progress initiate-kubernetes-cluster

if [[ $stop_before_kubernetes_provisioning == $true ]]; then
        echo -e "${CHECK_MARK} ${LIGHT_GREEN}${BOLD}$me ($pid): 'stop_before_kubernetes_provisioning' triggered, ${NC}${RED}${BOLD}'early aboard' ${NC}${LIGHT_GREEN}${BOLD}, aboarding provisioning${NC}"
        exit 0
fi

for cluster in $clusters; do
    echo "bash cluster-software-and-initiation/add-clusters-software.tsk $cluster $arguments &"
    bash cluster-software-and-initiation/add-clusters-software.tsk $cluster $arguments &
done
evaluate_progress add-clusters-software

echo -e "${DARK_GRAY}${ITALIC}successfully delivered Kubernetes for clusters $clusters${NC}"
exit 0