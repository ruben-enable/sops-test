function k8s_initiate() {
    if [[ ! -d $state_dir ]]; then mkdir $state_dir; fi
    if [[ ! -f $root_progress_file ]]; then touch $root_progress_file; fi
    if [[ ! -d $state_dir/$cluster ]]; then mkdir -p $state_dir/$cluster; fi
    if [[ ! -f $state_dir/$cluster/.progress ]]; then touch $state_dir/$cluster/.progress; fi
}
function get_internet_file() {
    internet_file=$1
    to_dir=$2
    if [[ $# -gt 2 ]]; then
        filename=$3
    else
        filename=$(basename $internet_file)
    fi
    internet_filename=$(basename $internet_file)
    internet_dir=$(dirname $internet_file)

	echo "$me ($pid), $cluster: retrieving $internet_filename from $internet_dir"
	result=$(curl --silent -sL $internet_file > $to_dir/$filename)
	if [[ ! -f $to_dir/$filename ]]; then
		end_result="failed to $internet_filename from $internet_dir: $result"
		result_value=11
		finish
	fi
}
function place_file_on_controlplaneserver() {
    source_file=$1
    if [[ $# -gt 1 ]]; then
        filename=$2
        sourcefile_basename=$(basename $source_file)
    else
        filename=$(basename $source_file)
        sourcefile_basename=$filename
    fi
    
    echo "$me ($pid), $cluster: scp $sourcefile_basename to cluster controlplane ($cluster_controlplane)"
	result=$(scp $source_file $cluster_controlplane:$filename 2>&1)
	if [[ $result != $empty ]]; then
		end_result="failed to scp $sourcefile to controlplane($cluster_controlplane): $result"
		result_value=12
		finish
	fi
}
function apply_yaml_to_cluster() {
    no_namespace=$false
    filename=$1
    if [[ $# -gt 1 ]]; then
        if [[ $2 != "--no-namespace" ]]; then
            inamespace=$2
        else
            no_namespace=$true
        fi
    else
        inamespace=default
    fi
    
	echo "$me ($pid), $cluster: adding $filename to cluster"
    if [[ $no_namespace == $false ]]; then
        echo "$me ($pid), $cluster: kubectl apply -n $inamespace -f $filename"
    	result=$(ssh $cluster_controlplane "kubectl apply -n $inamespace -f $filename" 2>&1)
    else
        echo "$me ($pid), $cluster: kubectl apply -f $filename"
    	result=$(ssh $cluster_controlplane "kubectl apply -f $filename" 2>&1)
    fi
    echo "$result"
	if [[ $result == $empty ]]; then
		end_result="adding $filename to cluster $cluster failed! returned-result is emptystring..."
		result_value=13
		finish
    elif [[ $result != *"created"* ]]; then
        if [[ $result != *"unchanged"* && $result != *"configured"* ]]; then
			end_result="adding $filename to cluster $cluster failed! returned-result is: $result"
			result_value=13
			finish
        fi
	fi

	result=$(ssh $cluster_controlplane "rm $filename")
}
function internetfile_to_cluster (){
    source=$1
    localdir=$2
    inamespace=$3
    if [[ $# -gt 3 ]]; then
        controlplane_filename=$4
    else
        controlplane_filename=$(basename $source)
    fi


	get_internet_file $source $localdir 
	place_file_on_controlplaneserver $localdir/$(basename $source) $controlplane_filename
	apply_yaml_to_cluster $controlplane_filename $inamespace
}
function sa_add_dockerhub_login() {
    if [[ $# -gt 0 ]]; then
        service_account=$1
    else
        service_account=default
    fi
    if [[ $# -gt 1 ]]; then
        i_namespace=$2
    else
        i_namespace=default
    fi

    result=$(scp cluster-software-and-initiation/initiating/docker/docker.account.yaml $cluster_controlplane:~/ 2>&1)
	result=$(ssh $cluster_controlplane "kubectl -n $i_namespace apply -f docker.account.yaml" 2>&1)
	result=$(ssh $cluster_controlplane "kubectl -n $i_namespace patch serviceaccount $service_account -p '{\"imagePullSecrets\": [{\"name\": \"regcred\"}]}'" 2>&1)
	result=$(ssh $cluster_controlplane "rm docker.account.yaml" 2>&1)
}
function check_existence_namespace() {
	i_namespace_exists=$false

	result=$(ssh $cluster_controlplane "kubectl get namespaces | grep \"$namespace\"")
	if [[ $result != $empty ]]; then  #result: namespace exists
		i_namespace_exists=$true
	fi
    echo $i_namespace_exists
}
function create_namespace(){
	enamespace=$1
	
	echo "$me ($pid), $cluster: creating $enamespace namespace"
	result=$(ssh $cluster_controlplane "kubectl create namespace $enamespace" 2>&1)
	if [[ $result != *"created"* && $result != *"already exists"* ]]; then
		end_result="creating namespace $enamespace failed: $result"
		result_value=87
		finish
	fi
}
function validate_existence_namespace() {
	enamespace=$1

	result=$(ssh $cluster_controlplane "kubectl get namespaces | grep \"$enamespace\"")
	if [[ $result == $empty ]]; then  #nothing returned namespace does not exists
		create_namespace $enamespace
	else
		new_namespace=$true
		for item in $result; do
			if [[ $item == $enamespace ]]; then
				new_namespace=$false
			fi 
		done
		if [[ $new_namespace == $true ]]; then
			create_namespace $enamespace
		fi
	fi
}
function execute_script_on_controlplane() {
    script=$1
    logfile=$2
    
    result=$(ssh $cluster_controlplane "bash $script" 2>&1)
    echo "$result"

    if [[ $logfile != $empty ]]; then
        result=$(scp $cluster_controlplane:.$logfile $state_app_dir/ 2>&1)
        if [[ $result != $empty ]]; then
            end_result="$nbf - gk8s-esoc-2 - failed to retrieve .$logfile from controlplane $cluster_controlplane: $result"
            result_value=21
            finish
        fi
    fi
	result=$(ssh $cluster_controlplane "rm $script .$logfile $state_app_dir" 2>&1)
}