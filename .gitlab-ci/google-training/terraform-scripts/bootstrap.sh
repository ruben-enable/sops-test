#!/bin/bash
#version:1.0
true=true;false=false;empty=""
me=$(basename $0)
user="#user#"
kubernetes_version="#kubernetes_version#"
container_runtime="#container_runtime#"
crio_os="#crio_os#"
crio_version="#crio_version#"
storage_provider="#storage-provider#"

kubernetes_release=${kubernetes_version//\*/}
kubernetes_release=(${kubernetes_release//./ })
kubernetes_major_version=${kubernetes_release[0]}
kubernetes_minor_release=${kubernetes_release[1]}

#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function get_distribution_release() {
  grep=$1
  echo $(sudo cat /etc/*-release | grep $grep)
}
function return_os_version() {
  result=$(get_distribution_release VERSION_ID)
  result=${result//\"/}
  result=${result//VERSION_ID=/}
  echo $result
}
function return_os_distribution() {
  result=$(get_distribution_release DISTRIB_ID)
  result=${result//\"/}
  result=${result//DISTRIB_ID=/}
  echo $result
}
function return_latest_kubernetes_release() {
  lr=$(curl --silent -sL https://api.github.com/repos/kubernetes/kubernetes/releases/latest | jq -r ".tag_name")
  lr=${lr//v/}
  echo $lr
}
function return_latest_crio_release() {
  lr=$(curl --silent -sL https://api.github.com/repos/cri-o/cri-o/releases/latest | jq -r ".tag_name")
  lr=${lr//v/}
  echo $lr
}
function crio_version_is_provisioned() {
  result=$(curl http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$version/ | grep 404)
  if [[ $result != $empty ]]; then
    echo $false
  else
    echo $true
  fi 
}
function confirm_systemctl_service_running(){
    service_name=$1
    
    result=$(sudo systemctl status $service_name)
    status=(${result#*Active:})
    status=${status[0]}
    echo "$service_name systemctl status: $status"
    if [[ $status != "active" ]]; then
        echo $false
    fi
    echo $true
}
function add_k8s() {
  echo "function add_k8s" >> /initial_install/k8s.txt
  #if [[ -f /initial_install/k8s.txt ]]; then rm /initial_install/k8s.txt; fi

  kv=$(kubadm version)
  echo "before k8s install, kubeadm version: $kv" >> /initial_install/k8s.txt
  sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg >> /initial_install/k8s.txt
  echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list >> /initial_install/k8s.txt
  sudo apt-get update >> /initial_install/k8s.txt
  if [[ $kubernetes_version == $empty ]]; then
    echo "installing latest kubernetes version" >> /initial_install/k8s.txt
    echo "sudo apt-get install -y kubelet kubeadm kubectl" >> /initial_install/k8s.txt
    sudo apt-get install -y kubelet kubeadm kubectl >> /initial_install/k8s.txt
  else
    echo "installing kubernetes version: $kubernetes_version" >> /initial_install/k8s.txt
    echo "sudo apt-get install -y kubelet=$kubernetes_version kubeadm=$kubernetes_version kubectl=$kubernetes_version" >> /initial_install/k8s.txt
    sudo apt-get install -y kubelet=$kubernetes_version kubeadm=$kubernetes_version kubectl=$kubernetes_version >> /initial_install/k8s.txt
  fi
}
function install_kubernetes() {
  echo "function install_kubernetes" >> /initial_install/k8s.txt
  teller=1
  kubernetes_installed=$false

  while [[ $kubernetes_installed == $false ]]; do
    if [[ $teller -gt 3 ]]; then
      echo "installing kubernetes software failed, aboarding install" >> /initial_install/bootstrap_failure
      echo "installing kubernetes software failed, aboarding install"
      exit 12
    fi
    add_k8s

    kubeadm_version=$(kubeadm version)
    echo "kubeadm version: $kubeadm_version" >> /initial_install/k8s.txt
    if [[ $kubeadm_version == *"Major"* && $kubeadm_version == *"Minor"* ]]; then
      echo "successfully installed kubeadm" >> /initial_install/k8s.txt
      kubernetes_installed=$true 
      break;
    else
      echo "installing kubeadm failed ($teller)" >> /initial_install/k8s.txt
      sleep 5 
    fi
    teller=$(( $teller + 1 ))
  done
}
function add_docker() {
  echo "#### installing docker0 runtime ####" >> /initial_install/container_runtime.txt
  sudo apt-get install -y docker.io >> /initial_install/container_runtime.txt
  if [[ $kubernetes_major_version -eq 1 && $kubernetes_minor_release -gt 21 ]]; then
    #configure cgroup systemd as driver for docker
    sudo bash -c 'cat > /etc/docker/daemon.json <<EOF
{
"exec-opts": ["native.cgroupdriver=systemd"]
}
EOF'
  fi
  sudo systemctl enable docker.service >> /initial_install/container_runtime.txt
  sudo systemctl restart docker.service >> /initial_install/container_runtime.txt
  sudo systemctl status docker.service >> /initial_install/container_runtime.txt
}
function install_docker() {
  teller=1
  docker_installed=$false

  while [[ $docker_installed == $false ]]; do
    if [[ $teller -gt 3 ]]; then
      echo "installing docker software failed, aboarding install" >> /initial_install/bootstrap_failure
      echo "installing docker software failed, aboarding install"
      exit 13
    fi
    add_docker

    docker_version=$(docker version)
    echo "docker version: $docker_version" >> /initial_install/container_runtime.txt
    if [[ $docker_version == *"Go version"* && $docker_version == *"Git commit"* ]]; then
      echo "successfully installed docker" >> /initial_install/container_runtime.txt
      docker_installed=$true 
    else
      echo "installing docker failed ($teller)" >> /initial_install/container_runtime.txt 
      sleep 5
    fi
    teller=$(( $teller + 1 ))
  done
}
#end functions
######################################
if [[ -f /initial_install/install_started.txt ]]; then
  echo "restarting $me $(date), not allowed leaving script" >> /initial_install/install_started.txt
  exit 0
fi

echo "starting $me: $(date)" >> /initial_install/install_started.txt
mkdir /initial_install

sudo timedatectl set-timezone Europe/Amsterdam >> /initial_install/install_started.txt

if [[ $kubernetes_version == $empty ]]; then
  echo "installing latest kubernetes version" >> /initial_install/install_started.txt
else
  echo "installing kubernetes version: $kubernetes_version" >> /initial_install/install_started.txt
fi

sudo apt-get update >> /initial_install/update.txt
sudo apt-get upgrade -y >> /initial_install/step1a.txt
sudo apt-get --purge autoremove -y >> /initial_install/step1b.txt
#sudo add-apt-repository ppa:gluster/glusterfs-7 -y
sudo apt-get install -y software-properties-common jq curl nano nfs-common expect bash-completion dnsutils gcc build-essential >> /initial_install/step2.txt

if [[ $storage_provider == "openebs" ]]; then
  #add openebs requirement
  sudo DEBIAN_FRONTEND=noninteractive apt-get install open-iscsi -y
  sleep 4
  sudo systemctl enable --now iscsid
  sudo systemctl restart iscsid
fi

if [[ $container_runtime == "docker" ]]; then
  install_docker

  #install cri-dockerd
  #compensate for cri runtime socket setting needed
  if [[ $kubernetes_major_version -eq 1 && $kubernetes_minor_release -gt 23 ]]; then
    #execute following script as sudo
    cat <<EOF > i-cri-dockerd.sh
##Install GO###
wget https://storage.googleapis.com/golang/getgo/installer_linux
sudo chmod +x ./installer_linux
sudo ./installer_linux
source ~/.bash_profile

#get cri-dockerd project
git clone https://github.com/Mirantis/cri-dockerd.git
cd cri-dockerd
mkdir bin
go build -o bin/cri-dockerd
mkdir -p /usr/local/bin
install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
cp -a packaging/systemd/* /etc/systemd/system
bash -c "sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service"
systemctl daemon-reload
systemctl enable cri-docker.service
systemctl enable --now cri-docker.socket
EOF
  sudo bash -c "bash i-cri-dockerd.sh"
  fi

elif [[ $container_runtime == "crio" ]]; then
  echo "#### installing crio runtime ####" >> /initial_install/container_runtime.txt
  echo "[TASK 2] Enable and Load Kernel modules" >> /initial_install/container_runtime.txt
  sudo bash -c 'cat >>/etc/modules-load.d/containerd.conf<<EOF
overlay
br_netfilter
EOF'
  sudo modprobe overlay >> /initial_install/container_runtime.txt
  sudo modprobe br_netfilter >> /initial_install/container_runtime.txt

  echo "[TASK 3] Add Kernel settings" >> /initial_install/container_runtime.txt
  sudo bash -c 'cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
EOF'
  sudo sysctl --system >> /initial_install/container_runtime.txt

  echo "[TASK 4] Install crio runtime" >> /initial_install/container_runtime.txt
  sudo bash -c 'cat >>/etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list<<EOF
deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/'$crio_os'/ /
EOF'
  sudo bash -c 'cat >>/etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:'$version'.list<<EOF
deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/'$crio_version'/'$crio_os'/ /
EOF'
  curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$crio_os/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers.gpg add - >> /initial_install/container_runtime.txt
  curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$version/$crio_os/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers-cri-o.gpg add - >> /initial_install/container_runtime.txt
  sudo apt update && sudo apt install -qq -y cri-o cri-o-runc >> /initial_install/container_runtime.txt

  sudo systemctl daemon-reload >> /initial_install/container_runtime.txt
  sudo systemctl enable crio >> /initial_install/container_runtime.txt
  sudo systemctl start crio >> /initial_install/container_runtime.txt

  teller=0
  while [[ $(confirm_systemctl_service_running crio) == $false ]]; do
    if [[ $teller -gt 3 ]]; then
      echo "server $server provision failed: unable to start crio.service" >> /initial_install/container_runtime.txt
      echo "server $server provision failed: unable to start crio.service"
      exit 11
    fi

    sudo systemctl restart crio
    sleep 2
    teller=$(( $teller + 1 ))
  done
  
  #compensate for cri runtime socket setting needed
  if [[ $kubernetes_major_version -eq 1 && $kubernetes_minor_release -gt 23 ]]; then
    #kubelet for crio
    #set toml listen
    sudo sed -i 's/# listen =/listen =/g;' /etc/crio/crio.conf
  fi
elif [[ $container_runtime == "containerd" ]]; then
  echo "#### installing containerd runtime ####" >> /initial_install/container_runtime.txt
  echo "[TASK 3] Enable and Load Kernel modules" >> /initial_install/container_runtime.txt
  sudo bash -c 'cat >>/etc/modules-load.d/containerd.conf<<EOF
overlay
br_netfilter
EOF'
  sudo modprobe overlay >> /initial_install/container_runtime.txt
  sudo modprobe br_netfilter >> /initial_install/container_runtime.txt

  echo "[TASK 4] Add Kernel settings" >> /initial_install/container_runtime.txt
  sudo bash -c 'cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
EOF'
  sudo sysctl --system >/dev/null 2>&1

  echo "[TASK 5] Install containerd runtime" >> /initial_install/container_runtime.txt
  sudo apt update -qq >/dev/null 2>&1
  sudo apt install -qq -y containerd apt-transport-https >/dev/null 2>&1
  sudo mkdir /etc/containerd
  containerd config default | sudo tee /etc/containerd/config.toml
  sudo sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
  sudo bash -c 'echo "runtime-endpoint: unix:///run/containerd/containerd.sock" > /etc/crictl.yaml'
  sudo systemctl restart containerd >> /initial_install/container_runtime.txt
  sudo systemctl enable containerd >/dev/null 2>&1
  sudo systemctl status containerd  >> /initial_install/container_runtime.txt
fi #end container runtime install

echo "disabling swap" >> /initial_install/k8s.txt
sudo swapoff -a >> /initial_install/k8s.txt

install_kubernetes

if [[ $storage_provider == "gluster" ]]; then
  sudo modprobe dm_thin_pool >> /initial_install/step4.txt
  sudo modprobe dm_mirror >> /initial_install/step4a.txt
  sudo modprobe dm_snapshot >> /initial_install/step4b.txt
  #use systemctl to make settings permanent (in case of reboot)
  sudo cat > /etc/systemd/system/loop_gluster.service << EOF
[Unit]
Description=Create the loopback device for GlusterFS
DefaultDependencies=false
Before=local-fs.target
After=systemd-udev-settle.service
Requires=systemd-udev-settle.service
[Service]
Type=oneshot
ExecStart=/bin/bash -c "modprobe dm_thin_pool && modprobe dm_mirror && modprobe dm_snapshot"
[Install]
WantedBy=local-fs.target
EOF
  systemctl enable /etc/systemd/system/loop_gluster.service >> /initial_install/step4d.txt
fi

cat > /home/$user/.bash_aliases << EOF
export KUBE_EDITOR="nano"
alias rkh='rm ~/.ssh/known_hosts'
alias w1='watch kubectl get pods --all-namespaces -o wide'
alias w2='watch kubectl get ing,services --all-namespaces -o wide'
alias w3='watch kubectl get endpoints --all-namespaces -o wide'
alias w4='watch kubectl get deployments,sts,daemonsets,rc --all-namespaces -o wide'
alias w5='watch kubectl get rs,cm --all-namespaces -o wide'
alias w6='watch kubectl get clusterissuers,secrets --all-namespaces -o wide'
alias w7='watch kubectl get pvc,pv --all-namespaces -o wide'
alias w8='watch kubectl get sc --all-namespaces -o wide'
alias w9='watch kubectl get sa --all-namespaces -o wide'
alias w10='watch kubectl get rolebindigs,roles --all-namespaces -o wide'
alias w11='watch kubectl get clusterroles --all-namespaces -o wide'
alias w97='watch kubectl get all --all-namespaces -o wide'
alias w98='watch kubectl get all,ing,ep --all-namespaces -o wide'
alias w99='watch kubectl get all,ing,ep,secrets --all-namespaces -o wide'
alias kr='kubectl run \$@ --dry-run=client -o yaml'
alias kd='kubectl create \$@ --dry-run=client -o yaml'
alias ka='kubectl get all \$@ '
alias kaa='kubectl get all \$@ --all-namespaces '
alias wkaa='watch kubectl get all \$@ --all-namespaces '
alias wkaaw='watch kubectl get all \$@ --all-namespaces -o wide'alias wka='watch kubectl get all \$@ '
alias kos='kubectl -n kube-system get all,ing,secrets,cm \$@ -o wide'
alias wkos='watch kubectl -n kube-system get all,ing,secrets,cm \$@ -o wide'
alias kn='kubectl get nodes -o wide'
alias wkn='watch kubectl get nodes -o wide'
alias klapi="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-apiserver -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias klcm="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-controller-manager -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias kls="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-scheduler -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias kldns="PODNAME=\$(kubectl -n kube-system get pod -l k8s-app=kube-dns -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c coredns"
alias kletc="PODNAME=\$(kubectl -n kube-system get pod -l component=etcd -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c etcd  --tail 100"
alias klkp="PODNAME=\$(kubectl -n kube-system get pod -l k8s-app=kube-proxy -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c kube-proxy  --tail 100"
alias kc='kubectl config use-context \$@'
alias cba='cat ~/.bash_aliases'
alias cbh='cat ~/.bash_history'
alias ksk='bash ~/kubectl_config_synchronize.tsk \$@'
alias psk='bash ~/participants_clusters_to_kubectl.tsk \$@'
#alias kn='kubens'
alias kctx='kubectx'
alias bh='cat ~/.bash_history'
alias bhg='cat ~/.bash_history| grep \$@'
alias uu='sudo apt-get update --allow-releaseinfo-change && sudo apt-get upgrade -y && sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias cc='sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias op='sudo ss -ltnp'
alias op2='sudo lsof -i -P -n | grep LISTEN'
alias bh='cat ~/.bash_history'
alias bhg='cat ~/.bash_history| grep \$@'
alias cba='cat ~/.bash_aliases'
alias bag='cat ~/.bash_aliases| grep \$@'
alias nba='nano ~/.bash_aliases'
alias sba='source .bash_aliases'
alias cbf='cat ~/.bash_functions'
alias gbf='cat ~/.bash_functions| grep $@'
alias nbf='nano ~/.bash_functions'
alias sbf='source .bash_functions'
alias sba='source .bash_aliases'
alias cbf='cat ~/.bash_functions'
alias gbf='cat ~/.bash_functions| grep $@'
alias nbf='nano ~/.bash_functions'
alias sbf='source .bash_functions'
alias jl='sudo journalctl -f -u '
alias jp='sudo journalctl -xeu '
alias j5m='sudo journalctl --since "5 minutes ago"'
alias j1h='sudo journalctl --since "1 hour ago"'
alias sa='systemctl list-units --type=service --state=running'
alias k=kubectl
complete -F __start_kubectl k
alias uu='sudo apt-get update --allow-releaseinfo-change && sudo apt-get upgrade -y && sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias cc='sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias home='cd ~'
alias ownhome='me=$(whoami);sudo chown -R $me:$me ~'
alias sa='systemctl list-units --type=service --state=running'
alias open-ports='sudo lsof -i -P -n | grep LISTEN'
alias op='sudo ss -ltnp'
alias op2='sudo lsof -i -P -n | grep LISTEN'
alias rnd='bash ~/.n-able/random_strings.tsk $@'
alias cwig='sudo cat /etc/wireguard/wg0.conf'
alias rwig='sudo systemctl restart wg-quick@wg0'
alias swig='sudo systemctl status wg-quick@wg0'
alias nwig='sudo nano /etc/wireguard/wg0.conf'
alias wwig='sudo watch wg show'
EOF
if [[ $container_runtime == "docker" ]]; then
  cat >> /home/$user/.bash_aliases << EOF
alias crictl='sudo crictl --runtime-endpoint=unix:///var/run/cri-dockerd.sock '
alias wcc='watch sudo crictl --runtime-endpoint=unix:///var/run/cri-dockerd.sock ps '
EOF
elif [[ $container_runtime == "crio" ]]; then
  cat >> /home/$user/.bash_aliases << EOF
alias crictl='sudo crictl --runtime-endpoint=unix:///var/run/crio/crio.sock '
alias wcc='watch sudo crictl --runtime-endpoint=unix:///var/run/crio/crio.sock '
EOF
elif [[ $container_runtime == "containerd" ]]; then
  cat >> /home/$user/.bash_aliases << EOF
alias crictl='sudo crictl --runtime-endpoint=unix:///var/run/containerd/containerd.sock '
alias wcc='watch sudo crictl --runtime-endpoint=unix:///var/run/cri-dockerd.sock ps '
EOF
fi

cat > /home/$user/.nanorc << EOF
# include all the preexisting configs

include "/usr/share/nano/*.nanorc"

set linenumbers
EOF
chown -R $user:$user /home/$user

echo "source /usr/share/bash-completion/bash_completion" >> /home/$user/.bashrc
echo 'source <(kubectl completion bash)' >> /home/$user/.bashrc
echo 'source <(helm completion bash)' >> /home/$user/.bashrc
sudo usermod -aG docker $user
sudo chown -R $user:$user /home/$user
echo "succesfully executed bootstrap install script" >> /initial_install/bootstrap_success