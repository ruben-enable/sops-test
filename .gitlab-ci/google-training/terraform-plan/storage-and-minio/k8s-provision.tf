resource "local_file" "logon_masters" {
    #logons: for gitlab-ci cluster install
    count    = var.masters_count
    content = <<EOF

####m${count.index +1}-${var.cluster_sub_domain}
Host m${count.index +1}-${var.cluster_sub_domain}
  User ${var.users[var.user_id]}
  Hostname m${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/logon_masters-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "logon_workers" {
    #logons for gitlab-ci cluster install
    count    = var.nodes_count
    content = <<EOF

####w${count.index +1}-${var.cluster_sub_domain}
Host w${count.index +1}-${var.cluster_sub_domain}
  User ${var.users[var.user_id]}
  Hostname w${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/logon_workers-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_masters" {
    #server config files for cluster internal ssh
    count    = var.masters_count
    content = <<EOF

Host m${count.index +1}
  User ${var.users[var.user_id]}
  Hostname m${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/config_masters-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_workers" {
    #server config files for cluster internal ssh
    count    = var.nodes_count
    content = <<EOF

Host w${count.index +1}
  User ${var.users[var.user_id]}
  Hostname w${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/config_workers-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_z_identity" {
    content = <<EOF
Host *
  IdentitiesOnly=yes

Host gitlab.com
HostName gitlab.com
User git
Port 22
IdentityFile ~/.ssh/p${var.user_id +1}

    EOF
    filename = "${path.root}/local_files/config_identity"
    }

resource "google_compute_instance" "master" {
  count    = var.masters_count
  name         = "m${count.index + 1}-${var.cluster_sub_domain}"
  machine_type = var.master_server
  zone         = var.region
   boot_disk {
      initialize_params {
      image = var.master_os
    }
   }

  metadata_startup_script = file("./scripts/bootstrap.sh")
    network_interface {
        network = "default"
        access_config {
            // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
        }
    }
}

output "masters_ip" {
  value = google_compute_instance.master.*.network_interface.0.access_config.0.nat_ip
}

output "master_name" {
  value = google_compute_instance.master.*.name
}

#gluster nodes
resource "google_compute_disk" "seconddisk" {
  count        = var.nodes_count
    name  = "seconddisk-${count.index + 1}-${var.cluster_sub_domain}"
    zone  = var.region
    type  =  var.worker_extra_hdd_type
    size = var.worker_extra_hdd_size
}
resource "google_compute_disk" "miniodisk" {
  count    = var.minio_worker_count
    name  = "miniodisk-${count.index + 1}-${var.cluster_sub_domain}"
    zone  = var.region
    type  =  var.worker_minio_hdd_type
    size = var.worker_minio_hdd_size
}

resource "google_compute_instance" "worker" {
  count        = var.nodes_count
  name         = "w${count.index + 1}-${var.cluster_sub_domain}"  
  machine_type = var.worker_server
  zone         = var.region
   boot_disk {
      initialize_params {
      image = var.worker_os
      type  = var.worker_bootdisk_hdd_type
      size = var.worker_bootdisk_hdd_size
    }
   }
  attached_disk {
    source      = element(google_compute_disk.seconddisk.*.self_link, count.index)
    device_name = element(google_compute_disk.seconddisk.*.name, count.index)
  }
  attached_disk {
     source      = element(google_compute_disk.miniodisk.*.self_link, count.index)
     device_name = element(google_compute_disk.miniodisk.*.name, count.index)
  }

  metadata_startup_script = file("./scripts/bootstrap.sh")
  network_interface {
      network = "default"
      access_config {
          // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
      }
  }
}

output "workers_ip" {
  value = google_compute_instance.worker.*.network_interface.0.access_config.0.nat_ip
}

output "workers_name" {
  value = google_compute_instance.worker.*.name
}