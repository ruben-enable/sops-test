resource "local_file" "logon_masters" {
    #logons: for gitlab-ci cluster install
    count    = var.masters_count
    content = <<EOF

####m${count.index +1}-${var.cluster_sub_domain}
Host m${count.index +1}-${var.cluster_sub_domain}
  User ${var.users[var.user_id]}
  Hostname m${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/logon_masters-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "logon_workers" {
    #logons for gitlab-ci cluster install
    count    = var.nodes_count
    content = <<EOF

####w${count.index +1}-${var.cluster_sub_domain}
Host w${count.index +1}-${var.cluster_sub_domain}
  User ${var.users[var.user_id]}
  Hostname w${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/logon_workers-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_masters" {
    #server config files for cluster internal ssh
    count    = var.masters_count
    content = <<EOF

Host m${count.index +1}
  User ${var.users[var.user_id]}
  Hostname m${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/config_masters-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_workers" {
    #server config files for cluster internal ssh
    count    = var.nodes_count
    content = <<EOF

Host w${count.index +1}
  User ${var.users[var.user_id]}
  Hostname w${count.index +1}.${var.user_domain}
  IdentityFile ~/.ssh/p${var.user_id +1}
    EOF
    filename = "${path.root}/local_files/config_workers-${var.cluster_sub_domain}-${count.index +1}"
    }

resource "local_file" "config_z_identity" {
    content = <<EOF
Host *
  IdentitiesOnly=yes

Host gitlab.com
HostName gitlab.com
User git
Port 22
IdentityFile ~/.ssh/p${var.user_id +1}

    EOF
    filename = "${path.root}/local_files/config_identity"
    }

resource "google_compute_instance" "master" {
  count    = var.masters_count
  name         = "m${count.index + 1}-${var.cluster_sub_domain}"
  machine_type = var.master_server
  zone         = var.region
   boot_disk {
      initialize_params {
      image = var.master_os
    }
   }
  metadata_startup_script = file("./scripts/bootstrap.sh")
  network_interface {
      network = "default"
      access_config {
          // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
      }
  }
}

output "masters_ip" {
  value = google_compute_instance.master.*.network_interface.0.access_config.0.nat_ip
}

output "master_name" {
  value = google_compute_instance.master.*.name
}

#gluster nodes
resource "google_compute_disk" "seconddisk" {
  count        = var.nodes_count
    name  = "seconddisk-${count.index + 1}-${var.cluster_sub_domain}"
    zone  = var.region
    type  =  var.worker_extra_hdd_type
    size = var.worker_extra_hdd_size
}

resource "google_compute_instance" "worker" {
  count        = var.nodes_count
  name         = "w${count.index + 1}-${var.cluster_sub_domain}"  
  machine_type = var.worker_server
  zone         = var.region
   boot_disk {
      initialize_params {
      image = var.worker_os
      type  = var.worker_bootdisk_hdd_type
      size = var.worker_bootdisk_hdd_size
    }
   }
  attached_disk {
    source      = element(google_compute_disk.seconddisk.*.self_link, count.index)
    device_name = element(google_compute_disk.seconddisk.*.name, count.index)
  }

    metadata_startup_script =<<SCRIPT
#!/bin/bash
#version:1.0
kubernetes_version="${var.kubernetes_version}"

mkdir /initial_install
sudo timedatectl set-timezone Europe/Amsterdam
echo "starting" >> /initial_install/install_started.txt
sudo apt-get update >> /initial_install/update.txt
sudo apt-get upgrade -y >> /initial_install/step1a.txt
sudo apt-get --purge autoremove -y >> /initial_install/step1b.txt
#sudo add-apt-repository ppa:gluster/glusterfs-7 -y
sudo apt-get install -y software-properties-common docker.io jq curl nano nfs-common expect bash-completion dnsutils gcc build-essential glusterfs-client >> /initial_install/step2.txt
sudo systemctl enable docker.service
sudo swapoff -a
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
if [[ $kubernetes_version == "" ]]; then
  echo "installing latest kubernetes version" >> /initial_install/k8s.txt
  sudo apt-get install -y kubelet kubeadm >> /initial_install/k8s.txt
else
  echo "installing kubernetes version: $kubernetes_version" >> /initial_install/k8s.txt
  sudo apt-get install -y kubelet=$kubernetes_version kubeadm=$kubernetes_version >> /initial_install/k8s.txt
fi

mkdir /gluster_storage
#prepare glusterfs storage
sudo modprobe dm_thin_pool >> /initial_install/step4.txt
sudo modprobe dm_mirror >> /initial_install/step4a.txt
sudo modprobe dm_snapshot >> /initial_install/step4b.txt
#sudo dd if=/dev/zero of=/gluster_storage bs=1M count=230000
#sudo losetup /dev/loop10 /gluster_storage

#use systemctl to make settings permanent (in case of reboot)
sudo cat > /etc/systemd/system/loop_gluster.service << EOF
[Unit]
Description=Create the loopback device for GlusterFS
DefaultDependencies=false
Before=local-fs.target
After=systemd-udev-settle.service
Requires=systemd-udev-settle.service
[Service]
Type=oneshot
ExecStart=/bin/bash -c "modprobe dm_thin_pool && modprobe dm_mirror && modprobe dm_snapshot"
[Install]
WantedBy=local-fs.target
EOF
systemctl enable /etc/systemd/system/loop_gluster.service >> /initial_install/step4d.txt

cat > /home/${var.users[var.user_id]}/.bash_aliases << EOF
export KUBE_EDITOR="nano"
alias rkh='rm ~/.ssh/known_hosts'
alias k='kubectl \$@'
alias kr='kubectl run --generator="run-pod/v1" \$@ --dry-run -o yaml'
alias kd='kubectl run \$@ --dry-run -o yaml'
alias ka='kubectl get all \$@ '
alias kaa='kubectl get all \$@ --all-namespaces '
alias wkaa='watch kubectl get all \$@ --all-namespaces '
alias wkaaw='watch kubectl get all \$@ --all-namespaces -o wide'alias wka='watch kubectl get all \$@ '
alias kos='kubectl -n kube-system get all,ing,secrets,cm \$@ -o wide'
alias wkos='watch kubectl -n kube-system get all,ing,secrets,cm \$@ -o wide'
alias kn='kubectl get nodes -o wide'
alias wkn='watch kubectl get nodes -o wide'
alias klapi="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-apiserver -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias klcm="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-controller-manager -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias kls="PODNAME=\$(kubectl -n kube-system get pod -l component=kube-scheduler -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME --tail 100"
alias kldns="PODNAME=\$(kubectl -n kube-system get pod -l k8s-app=kube-dns -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c coredns"
alias kletc="PODNAME=\$(kubectl -n kube-system get pod -l component=etcd -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c etcd  --tail 100"
alias klkp="PODNAME=\$(kubectl -n kube-system get pod -l k8s-app=kube-proxy -o jsonpath='{.items[0].metadata.name}');kubectl -n kube-system logs \$PODNAME -c kube-proxy  --tail 100"
alias w1='watch kubectl get pods --all-namespaces -o wide'
alias w2='watch kubectl get ing,services --all-namespaces -o wide'
alias w3='watch kubectl get endpoints --all-namespaces -o wide'
alias w4='watch kubectl get deployments,sts,daemonsets,rc --all-namespaces -o wide'
alias w5='watch kubectl get rs,cm --all-namespaces -o wide'
alias w6='watch kubectl get clusterissuers,secrets --all-namespaces -o wide'
alias w7='watch kubectl get pvc,pv --all-namespaces -o wide'
alias w8='watch kubectl get sc --all-namespaces -o wide'
alias w9='watch kubectl get sa --all-namespaces -o wide'
alias w10='watch kubectl get rolebindings,roles --all-namespaces -o wide'
alias w11='watch kubectl get clusterroles --all-namespaces -o wide'
alias w97='watch kubectl get all --all-namespaces -o wide'
alias w98='watch kubectl get all,ing,ep --all-namespaces -o wide'
alias w99='watch kubectl get all,ing,ep,secrets --all-namespaces -o wide'
alias w1o='watch kubectl get pods --all-namespaces -o wide'
alias w2o='watch kubectl get ing,services --all-namespaces -o wide'
alias w3o='watch kubectl get endpoints --all-namespaces -o wide'
alias w4o='watch kubectl get deployments,sts,daemonsets,rc --all-namespaces -o wide'
alias w5o='watch kubectl get rs,cm --all-namespaces -o wide'
alias w6o='watch kubectl get clusterissuers,secrets --all-namespaces -o wide'
alias w7o='watch kubectl get pvc,pv --all-namespaces -o wide'
alias w8o='watch kubectl get sc --all-namespaces -o wide'
alias w9o='watch kubectl get sa --all-namespaces -o wide'
alias w10o='watch kubectl get rolebindings,roles --all-namespaces -o wide'
alias w11o='watch kubectl get clusterroles --all-namespaces -o wide -o wide'
alias k-api-man='sudo cat /etc/kubernetes/manifests/kube-apiserver.yaml -o wide'
alias k-api-man='sudo cat /etc/kubernetes/manifests/kube-apiserver.yaml'
alias uu='sudo apt-get update --allow-releaseinfo-change && sudo apt-get upgrade -y && sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias cc='sudo apt-get --purge autoremove -y && sudo apt-get autoclean -y'
alias op='sudo ss -ltnp'
alias op2='sudo lsof -i -P -n | grep LISTEN'
alias jl='sudo journalctl -f -u '
alias jp='sudo journalctl -xeu '
alias j5m='sudo journalctl --since "5 minutes ago"'
alias j1h='sudo journalctl --since "1 hour ago"'
alias sa='systemctl list-units --type=service --state=running'
alias home='cd ~'
alias ownhome='me=$(whoami);sudo chown -R $me:$me ~'
alias rnd='bash ~/.n-able/random_strings.tsk $@'
alias cwig='sudo cat /etc/wireguard/wg0.conf'
alias rwig='sudo systemctl restart wg-quick@wg0'
alias swig='sudo systemctl status wg-quick@wg0'
alias nwig='sudo nano /etc/wireguard/wg0.conf'
alias wwig='sudo watch wg show'
alias bh='cat ~/.bash_history'
alias bhg='cat ~/.bash_history| grep \$@'
alias cba='cat ~/.bash_aliases'
alias bag='cat ~/.bash_aliases| grep \$@'
alias nba='nano ~/.bash_aliases'
alias sba='source .bash_aliases'
alias cbf='cat ~/.bash_functions'
alias gbf='cat ~/.bash_functions| grep $@'
alias nbf='nano ~/.bash_functions'
alias sbf='source .bash_functions'
alias cha='cat /etc/haproxy/haproxy.cfg'
alias nha='sudo nano /etc/haproxy/haproxy.cfg'
alias rha='sudo systemctl restart haproxy'
alias sha='sudo systemctl status haproxy'
alias k=kubectl
complete -F __start_kubectl k
EOF
cat > /home/${var.users[var.user_id]}/.nanorc << EOF
# include all the preexisting configs

include "/usr/share/nano/*.nanorc"

set linenumbers
EOF
chown -R ${var.users[var.user_id]}:${var.users[var.user_id]} /home/${var.users[var.user_id]}

echo "source /usr/share/bash-completion/bash_completion" >> /home/${var.users[var.user_id]}/.bashrc
echo 'source <(kubectl completion bash)' >> /home/${var.users[var.user_id]}/.bashrc
alias k=kubectl
complete -F __start_kubectl k
echo 'source <(helm completion bash)' >> /home/${var.users[var.user_id]}/.bashrc
echo "initial install ready" >> /initial_install/bootstrap_success
SCRIPT

    network_interface {
        network = "default"
        access_config {
            // Ephemeral IP - leaving this block empty will generate a new external IP and assign it to the machine
        }
    }
}

output "workers_ip" {
  value = google_compute_instance.worker.*.network_interface.0.access_config.0.nat_ip
}

output "workers_name" {
  value = google_compute_instance.worker.*.name
}