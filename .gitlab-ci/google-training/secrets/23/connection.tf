provider "google" {
  credentials = file("23/account.json")
  project = "p23-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p23"
    prefix = "terraform"
    credentials = "23/account.json"
  }
}

variable "user_id" {
  type = string
  default = 22 // Training user
}

