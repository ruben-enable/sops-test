provider "google" {
  credentials = file("4/account.json")
  project = "snappy-benefit-238107"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p4"
    prefix = "terraform"
    credentials = "4/account.json"
  }
}

variable "user_id" {
  type = string
  default = 3 // Training user
}

