provider "google" {
  credentials = file("18/account.json")
  project = "p18-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p18"
    prefix = "terraform"
    credentials = "18/account.json"
  }
}

variable "user_id" {
  type = string
  default = 17 // Training user
}
