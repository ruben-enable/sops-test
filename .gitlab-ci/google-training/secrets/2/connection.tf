provider "google" {
  credentials = file("2/account.json")
  project = "valued-etching-235006"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p2"
    prefix = "terraform"
    credentials = "2/account.json"
  }
}

variable "user_id" {
  type = string
  default = 1 // Training user
}
