provider "google" {
  credentials = file("14/account.json")
  project = "active-campus-259409"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p14"
    prefix = "terraform"
    credentials = "14/account.json"
  }
}

variable "user_id" {
  type = string
  default = 13 // Training user
}

