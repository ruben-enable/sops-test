provider "google" {
  credentials = file("8/account.json")
  project = "p-eighth-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p8"
    prefix = "terraform"
    credentials = "8/account.json"
  }
}

variable "user_id" {
  type = string
  default = 7 // Training user
}

