provider "google" {
  credentials = file("22/account.json")
  project = "p22-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p22"
    prefix = "terraform"
    credentials = "22/account.json"
  }
}

variable "user_id" {
  type = string
  default = 21 // Training user
}

