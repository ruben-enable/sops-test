provider "google" {
  credentials = file("10/account.json")
  project = "p-ten-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p10"
    prefix = "terraform"
    credentials = "10/account.json"
  }
}

variable "user_id" {
  type = string
  default = 9 // Training user
}

