provider "google" {
  credentials = file("13/account.json")
  project = "molten-rex-259409"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p13"
    prefix = "terraform"
    credentials = "13/account.json"
  }
}

variable "user_id" {
  type = string
  default = 12 // Training user
}

