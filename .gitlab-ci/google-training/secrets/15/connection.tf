provider "google" {
  credentials = file("15/account.json")
  project = "p15-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p15"
    prefix = "terraform"
    credentials = "15/account.json"
  }
}

variable "user_id" {
  type = string
  default = 14 // Training user
}

