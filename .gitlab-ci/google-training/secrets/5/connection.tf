provider "google" {
  credentials = file("5/account.json")
  project = "ferrous-cipher-238107"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p5"
    prefix = "terraform"
    credentials = "5/account.json"
  }
}

variable "user_id" {
  type = string
  default = 4 // Training user
}

