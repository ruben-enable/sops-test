provider "google" {
  project = "engaged-stage-234905"
  credentials = file("1/account.json")
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p1"
    prefix = "terraform"
    credentials = "1/account.json"
  }
}
variable "user_id" {
  type = string
  default = 0 // Training user
}
