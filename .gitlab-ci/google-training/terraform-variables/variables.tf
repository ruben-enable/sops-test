variable "region" {
  default = "europe-west4-a" // We're going to need it in several places in this config
}

variable "users" {
    type = list
    default = [ "participant.one", "participant.two", "participant.three", "participant.four", "participant.five", "participant.six", "participant.seven", "participant.eighth", "participant.nine", "participant.ten", "participant.eleven", "participant.twelf", "participant.thirteen", "participant.fourteen", "participant.fifteen", "participant.sixteen", "participant.seventeen", "participant.eighteen", "participant.nineteen", "participant.twenty", "participant.twentyone", "participant.twentytwo", "participant.twentythree", "participant.twentyfour", "participant.twentyfive", "participant.twentysix", "participant.twentyseven", "participant.twentyeight", "participant.twentynine", "participant.thirty" ]
}

variable "masters_count" {
    type = string
    default = "#mastercount#"
}

variable "nodes_count" {
    type = string
    default = "#nodecount#"
}

variable "user_domain" {
    type = string
    default = "#userdomain#"
}

variable "cluster_sub_domain" {
    type = string
    default = "k#cluster_subdomain_nr#"
}

variable "gluster_REPLICAS" {
    type = string
    default = "2"
}

variable "kubernetes_version" {
    type = string
    default = "#kubernetes_version#"
}

variable "master_server" {
    type = string
    default = "#master_server#"
}

variable "master_os" {
    type = string
    default = "#master_os#"
}

variable "worker_server" {
    type = string
    default = "#worker_server#"
}

variable "worker_os" {
    type = string
    default = "#worker_os#"
}

variable "worker_extra_hdd_size" {
    type = string
    default = "#worker_extra_hdd_size#"
}

variable "worker_extra_hdd_type" {
    type = string
    default = "#worker_extra_hdd_type#"
}

variable "worker_bootdisk_hdd_size" {
    type = string
    default = "#worker_bootdisk_hdd_size#"
}

variable "worker_bootdisk_hdd_type" {
    type = string
    default = "#worker_bootdisk_hdd_type#"
}
variable "container_runtime"{
    type = string
    default = "#container_runtime#"
}
variable "crio_os"{
    type = string
    default = "#crio_os#"
}
variable "crio_version"{
    type = string
    default = "#crio_version#"
}
variable "minio_storage_enabled"{
    type = string
    default = "#minio_storage_enabled#"
}
variable "minio_worker_count"{
    type = string
    default = "#minio_worker_count#"
}
variable "worker_minio_hdd_size" {
    type = string
    default = "#worker_minio_hdd_size#"
}

variable "worker_minio_hdd_type" {
    type = string
    default = "#worker_minio_hdd_type#"
}