provider "google" {
  credentials = file("../secrets/22/account.json")
  project = "p22-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p22"
    prefix = "terraform"
    credentials = "../secrets/22/account.json"
  }
}

variable "user_id" {
  type = string
  default = 21 // Training user
}

