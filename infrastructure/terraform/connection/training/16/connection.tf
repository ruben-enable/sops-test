provider "google" {
  credentials = file("../secrets/16/account.json")
  project = "p16-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p16"
    prefix = "terraform"
    credentials = "../secrets/16/account.json"
  }
}

variable "user_id" {
  type = string
  default = 15 // Training user
}
