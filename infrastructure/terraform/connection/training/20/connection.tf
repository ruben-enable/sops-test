provider "google" {
  credentials = file("../secrets/20/account.json")
  project = "p20-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p20"
    prefix = "terraform"
    credentials = "../secrets/20/account.json"
  }
}

variable "user_id" {
  type = string
  default = 19 // Training user
}

