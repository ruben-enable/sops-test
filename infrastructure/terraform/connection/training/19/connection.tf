provider "google" {
  credentials = file("../secrets/19/account.json")
  project = "p19-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p19"
    prefix = "terraform"
    credentials = "../secrets/19/account.json"
  }
}

variable "user_id" {
  type = string
  default = 18 // Training user
}

