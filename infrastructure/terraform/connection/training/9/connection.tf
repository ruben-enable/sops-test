provider "google" {
  credentials = file("../secrets/9/account.json")
  project = "p-nine-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p9"
    prefix = "terraform"
    credentials = "../secrets/9/account.json"
  }
}

variable "user_id" {
  type = string
  default = 8 // Training user
}

