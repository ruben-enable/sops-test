provider "google" {
  credentials = file("../secrets/12/account.json")
  project = "onyx-sequencer-259409"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p12"
    prefix = "terraform"
    credentials = "../secrets/12/account.json"
  }
}

variable "user_id" {
  type = string
  default = 11 // Training user
}

