provider "google" {
  credentials = file("../secrets/7/account.json")
  project = "p-seven-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p7"
    prefix = "terraform"
    credentials = "../secrets/7/account.json"
  }
}

variable "user_id" {
  type = string
  default = 6 // Training user
}

