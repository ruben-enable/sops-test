provider "google" {
  credentials = file("../secrets/17/account.json")
  project = "p17-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p17"
    prefix = "terraform"
    credentials = "../secrets/17/account.json"
  }
}

variable "user_id" {
  type = string
  default = 16 // Training user
}
