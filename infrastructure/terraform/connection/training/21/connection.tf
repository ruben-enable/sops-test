provider "google" {
  credentials = file("../secrets/21/account.json")
  project = "p21-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p21"
    prefix = "terraform"
    credentials = "../secrets/21/account.json"
  }
}

variable "user_id" {
  type = string
  default = 20 // Training user
}

