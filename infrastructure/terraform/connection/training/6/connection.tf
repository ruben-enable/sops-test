provider "google" {
  credentials = file("../secrets/6/account.json")
  project = "p-six-training-project"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p6"
    prefix = "terraform"
    credentials = "../secrets/6/account.json"
  }
}

variable "user_id" {
  type = string
  default = 5 // Training user
}

