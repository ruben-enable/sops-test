provider "google" {
  credentials = file("../secrets/3/account.json")
  project = "fluent-crossbar-238010"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p3"
    prefix = "terraform"
    credentials = "../secrets/3/account.json"
  }
}

variable "user_id" {
  type = string
  default = 2 // Training user
}

