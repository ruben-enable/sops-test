provider "google" {
  credentials = file("../secrets/11/account.json")
  project = "training-project-p11-two"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p11-2"
    prefix = "terraform"
    credentials = "../secrets/11/account.json"
  }
}

variable "user_id" {
  type = string
  default = 10 // Training user
}

