provider "google" {
  credentials = file("../secrets/11/account.json")
  project = "nth-transformer-259409	"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p11"
    prefix = "terraform"
    credentials = "../secrets/11/account.json"
  }
}

variable "user_id" {
  type = string
  default = 10 // Training user
}

