provider "google" {
  project = "engaged-stage-234905"
  credentials = file("../secrets/1/account.json")
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p1"
    prefix = "terraform"
    credentials = "../secrets/1/account.json"
  }
}
variable "user_id" {
  type = string
  default = 0 // Training user
}
