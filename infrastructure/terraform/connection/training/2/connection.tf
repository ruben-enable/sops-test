provider "google" {
  credentials = file("../secrets/2/account.json")
  project = "valued-etching-235006"
}

terraform {
  backend "gcs" {
    bucket = "n-able-bucket-p2"
    prefix = "terraform"
    credentials = "../secrets/2/account.json"
  }
}

variable "user_id" {
  type = string
  default = 1 // Training user
}
