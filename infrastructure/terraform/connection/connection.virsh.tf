
terraform {
 required_version = ">= #terraform-version#"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
    }
  }
}


# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}