variable "users" {
    type = list
    default = [ "participant.one", "participant.two", "participant.three", "participant.four", "participant.five", "participant.six", "participant.seven", "participant.eighth", "participant.nine", "participant.ten", "participant.eleven", "participant.twelf", "participant.thirteen", "participant.fourteen", "participant.fifteen", "participant.sixteen", "participant.seventeen", "participant.eighteen", "participant.nineteen", "participant.twenty", "participant.twentyone", "participant.twentytwo", "participant.twentythree", "participant.twentyfour", "participant.twentyfive", "participant.twentysix", "participant.twentyseven", "participant.twentyeight", "participant.twentynine", "participant.thirty" ]
}

# variables that can be overriden
variable "hostname" { default = "#cluster#" }
variable "domain" { default = "#domain#" }
variable "memoryMB" { default = 1024 }
variable "hddGB" { default = 1073980306 }

variable "controlplanes_count" {
    type = string
    default = "#controlplane_count#"
}

variable "controlplane_startcount" {
    type = string
    default = "#controlplane_startcount#"
}

variable "workers_count" {
    type = string
    default = "#worker_count#"
}

variable "worker_startcount" {
    type = string
    default = "#worker_startcount#"
}

variable "user_domain" {
    type = string
    default = "#cluster_operational_tld#"
}

variable "cluster" {
    type = string
    default = "#cluster#"
}

variable "gluster_REPLICAS" {
    type = string
    default = "2"
}

variable "kubernetes_version" {
    type = string
    default = "#kubernetes_version#"
}

variable "controlplane_server" {
    type = string
    default = "#controlplane_server#"
}

variable "controlplane_os" {
    type = string
    default = "#controlplane_os#"
}

variable "controlplane_cpus" {
    type = string
    default = "#controlplane_cpus#"
}

variable "controlplane_memory" {
    type = string
    default = "#controlplane_memory#"
}

variable "controlplane_bootdisk_hdd_size" {
    type = string
    default = "#controlplane_bootdisk_hdd_size#"
}

variable "worker_server" {
    type = string
    default = "#worker_server#"
}

variable "worker_os" {
    type = string
    default = "#worker_os#"
}

variable "worker_cpus" {
    type = string
    default = "#worker_cpus#"
}

variable "worker_memory" {
    type = string
    default = "#worker_memory#"
}

variable "worker_extra_hdd_size" {
    type = string
    default = "#worker_extra_hdd_size#"
}

variable "worker_extra_hdd_type" {
    type = string
    default = "#worker_extra_hdd_type#"
}

variable "worker_bootdisk_hdd_size" {
    type = string
    default = "#worker_bootdisk_hdd_size#"
}

variable "worker_bootdisk_hdd_type" {
    type = string
    default = "#worker_bootdisk_hdd_type#"
}

###
variable "openstack_public_key" {
    type = string
    default = "#openstack-public-key#"
}

variable "cluster_number" {
    type = number
    default = #cluster-number#
}

variable "external_network_id" {
    type = string
    default = "093ae4f0-caf5-49ad-9a51-7e29747b7468"
}
variable "mobilea_network_id" {
    type = string
    default = "bc5750ca-dacc-4361-af54-d5948215c7a5"
}
variable "third_party_network_id" {
    type = string
    default = "b1b5c1c0-91c4-4811-a033-d017050a2385"
}

variable "controlplane_image_id" {
    type = string
    default = "#controlplane-image-id#"
}

variable "worker_image_id" {
    type = string
    default = "#worker-image-id#"
}

variable "controlplane_compute_instance" {
    type = string
    default = "#controlplane-compute-instance#"
}

variable "worker_compute_instance" {
    type = string
    default = "#worker-compute-instance#"
}

variable "server_autostart"{
    type = string
    default = "#server-autostart#"
}
variable "container_runtime"{
    type = string
    default = "#container_runtime#"
}
variable "crio_os"{
    type = string
    default = "#crio_os#"
}
variable "crio_version"{
    type = string
    default = "#crio_version#"
}
variable "minio_storage_enabled"{
    type = string
    default = "#minio_storage_enabled#"
}
variable "minio_worker_count"{
    type = string
    default = "#minio_worker_count#"
}
variable "worker_minio_hdd_size" {
    type = string
    default = "#worker_minio_hdd_size#"
}

variable "worker_minio_hdd_type" {
    type = string
    default = "#worker_minio_hdd_type#"
}