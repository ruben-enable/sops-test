


# fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "cp-base-vol" {
  count    = var.controlplanes_count
  name     = "m${count.index + var.controlplane_startcount}.${var.hostname}-cp-base-vol"
  pool = "default"
  source = var.controlplane_server
  format = "qcow2"
}

resource "libvirt_volume" "cp-resized-root-vol" {
  count    = var.controlplanes_count
  name = "m${count.index + var.controlplane_startcount}.${var.hostname}-volume"
  pool = "default"
  base_volume_id = element(libvirt_volume.cp-base-vol.*.id, count.index)
  base_volume_pool = "default"
  format = "qcow2"
  size     = var.hddGB * var.controlplane_bootdisk_hdd_size
}

resource "libvirt_volume" "w-base-vol" {
  count    = var.workers_count
  name     = "w${count.index + var.worker_startcount}.${var.hostname}-w-base-vol"
  pool = "default"
  source = var.worker_server
  format = "qcow2"
}

resource "libvirt_volume" "w-resized-root-vol" {
  count    = var.workers_count
  name = "w${count.index + var.worker_startcount}.${var.hostname}-volume"
  pool = "default"
  base_volume_id = element(libvirt_volume.w-base-vol.*.id, count.index)
  base_volume_pool = "default"
  format = "qcow2"
  size     = var.hddGB * var.worker_bootdisk_hdd_size
}

resource "libvirt_volume" "second_disk" {
  count    = var.workers_count
  name     = "w${count.index + var.worker_startcount}.${var.cluster}-second-disk"
  pool     = "default"
  size     = var.hddGB * var.worker_extra_hdd_size
  format   = "qcow2"
}

# Use CloudInit ISO to add ssh-key to the instance
resource "libvirt_cloudinit_disk" "cp_commoninit" {
  count    = var.controlplanes_count
  name     = "m${count.index + var.controlplane_startcount}.${var.cluster}-commoninit.iso"
  pool = "default"
  user_data = element(data.template_file.controlplane_user_data.*.rendered, count.index)
  network_config = data.template_file.network_config.rendered
}

# Use CloudInit ISO to add ssh-key to the instance
resource "libvirt_cloudinit_disk" "w_commoninit" {
  count    = var.workers_count
  name     = "w${count.index + var.worker_startcount}.${var.cluster}-commoninit.iso"
  pool     = "default"
  user_data = element(data.template_file.worker_user_data.*.rendered, count.index)
  network_config = data.template_file.network_config.rendered
}

data "template_file" "controlplane_user_data" {
  count    = var.controlplanes_count
  template = file("${path.module}/cp_cloud_init.cfg")
  vars = {
    hostname = "m${count.index + var.controlplane_startcount}.${var.hostname}"
    domain = var.domain
    fqdn = "${var.hostname}.${var.domain}"
  }
}

data "template_file" "worker_user_data" {
  count    = var.workers_count
  template = file("${path.module}/w_cloud_init.cfg")
  vars = {
    hostname = "w${count.index + var.worker_startcount}.${var.hostname}"
    domain = var.domain
    fqdn = "${var.hostname}.${var.domain}"
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config_dhcp.cfg")
}

# Create the machine
resource "libvirt_domain" "controlplane_server" {
  count    = var.controlplanes_count
  name     = "m${count.index + var.controlplane_startcount}.${var.cluster}"
  memory = var.memoryMB * var.controlplane_memory
  vcpu = var.controlplane_cpus
  autostart = var.server_autostart

  disk {
       volume_id = element(libvirt_volume.cp-resized-root-vol.*.id, count.index)
  }

  network_interface {
       network_name = "host-bridge0"
       #wait_for_lease = true
  }
  network_interface {
       network_name = "host-bridge1"
       #wait_for_lease = true
  }
  cloudinit = element(libvirt_cloudinit_disk.cp_commoninit.*.id, count.index)

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

/* output "controlplane_ips" {
  value = libvirt_domain.controlplane_server.*.network_interface.0.ip_address
} */

output "controlplane_names" {
  value = libvirt_domain.controlplane_server.*.name
}

# Create the machine
resource "libvirt_domain" "worker_server" {
  count    = var.workers_count
  name     = "w${count.index + var.worker_startcount}.${var.cluster}"
  memory = var.memoryMB * var.worker_memory
  vcpu = var.worker_cpus
  autostart = var.server_autostart

  disk {
       volume_id = element(libvirt_volume.w-resized-root-vol.*.id, count.index)
  }
  disk {
      volume_id = element(libvirt_volume.second_disk.*.id, count.index)
  }

  network_interface {
       network_name = "host-bridge0"
       #wait_for_lease = true
  }
  network_interface {
       network_name = "host-bridge1"
       #wait_for_lease = true
  }
  cloudinit = element(libvirt_cloudinit_disk.w_commoninit.*.id, count.index)

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}

/* output "worker_ips" {
  value = libvirt_domain.worker_server.*.network_interface.0.ip_address
} */

output "worker_names" {
  value = libvirt_domain.worker_server.*.name
}
