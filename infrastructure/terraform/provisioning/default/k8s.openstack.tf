
data "openstack_networking_network_v2" "public" {
  name = "public"
}


resource "openstack_networking_network_v2" "network" {
  name           = var.cluster
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name       = var.cluster
  network_id = openstack_networking_network_v2.network.id
  cidr       = "10.0.${var.cluster_number}.0/24"
  ip_version = 4
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]
}

resource "openstack_networking_router_v2" "internet-router" {
  name                = var.cluster
  admin_state_up      = "true"
  external_network_id = var.external_network_id
}

resource "openstack_networking_router_interface_v2" "internet-interface" {
  router_id = openstack_networking_router_v2.internet-router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

# resource "openstack_networking_router_v2" "company-router" {
#   name                = "mobilea-${var.cluster}"
#   admin_state_up      = "true"
# }

# resource "openstack_networking_router_interface_v2" "mobilea-interface" {
#   router_id = openstack_networking_router_v2.company-router.id
#   subnet_id = openstack_networking_subnet_v2.subnet.id
# }

# resource "openstack_networking_router_v2" "third-party-router" {
#   name                = "external-access-${var.cluster}"
#   admin_state_up      = "true"
# }

# resource "openstack_networking_router_interface_v2" "thirt-party-interface" {
#   router_id = openstack_networking_router_v2.third-party-router.id
#   subnet_id = openstack_networking_subnet_v2.subnet.id
# }

resource "openstack_compute_secgroup_v2" "secgroup_internal" {
  name        = "${var.cluster}-internal"
	description = "Kubernetes cluster ${var.cluster} network internal security group"

  rule {
    from_port	= 1
    to_port		= 65535
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }

  rule {
    from_port       = 1
    to_port         = 65535
    ip_protocol     = "udp"
    cidr            = "0.0.0.0/0"
  }

  rule {
    from_port   	= -1
    to_port     	= -1
    ip_protocol 	= "icmp"
    cidr        	= "0.0.0.0/0"
  }
}

resource "openstack_compute_secgroup_v2" "secgroup_users" {
  name        = "${var.cluster}-users"
	description = "Kubernetes cluster ${var.cluster} security group for operational users"

  rule {
    from_port	= 80
    to_port		= 80
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }
  rule {
    from_port	= 443
    to_port		= 443
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }
  rule {
    from_port	= 22
    to_port		= 22
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }
}

resource "openstack_compute_secgroup_v2" "secgroup_admins" {
  name        = "${var.cluster}-admins"
	description = "Kubernetes cluster ${var.cluster} security group for cluster administrators"

  rule {
    from_port	= 6443
    to_port		= 6443
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }
  rule {
    from_port	= 22
    to_port		= 22
    ip_protocol	= "tcp"
    cidr		= "0.0.0.0/0"
  }
}

data "template_file" "controlplane_user_data" {
  count    = var.controlplanes_count
  template = file("${path.module}/cp_cloud_init.cfg")
  vars = {
    hostname = "m${count.index + 1}.${var.hostname}"
    domain = var.domain
    fqdn = var.cluster
  }
}

### controlplane provisioning ###
resource "openstack_blockstorage_volume_v3" "boot_controlplane" {
  count    = var.controlplanes_count
  name        = "boot_controlplane_${count.index + 1}.${var.cluster}"
  size        = var.controlplane_bootdisk_hdd_size
  image_id    = var.controlplane_image_id
}

resource "openstack_compute_instance_v2" "controlplane_server" {
  count    = var.controlplanes_count
  name            = "m${count.index + 1}.${var.hostname}"
  flavor_name     = var.controlplane_compute_instance
  key_pair        = var.openstack_public_key
  user_data = element(data.template_file.controlplane_user_data.*.rendered, count.index)
  security_groups = [
    openstack_compute_secgroup_v2.secgroup_internal.id,
    openstack_compute_secgroup_v2.secgroup_admins.id,
    openstack_compute_secgroup_v2.secgroup_users.id
    ]
  block_device {
    uuid                  = element(openstack_blockstorage_volume_v3.boot_controlplane.*.id, count.index)
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }
  # provisioner "remote-exec" {
  #   inline = [
  #     "echo 'the time is now' >> /var/log/cloud-init-output.log",
  #     "sudo bash -c \"echo 'the time is now' >> /var/log/cloud-init-output.log\"",
  #   ]
  #   connection {
  #       user = "kuber"
  #   }
  # }  

  network {
    name = var.cluster
  }
}

resource "openstack_networking_floatingip_v2" "controlplane_public_ip" {
  count    = var.controlplanes_count
  pool = data.openstack_networking_network_v2.public.name
}

resource "openstack_compute_floatingip_associate_v2" "controlplane_external_link" {
  count    = var.controlplanes_count
  floating_ip = element(openstack_networking_floatingip_v2.controlplane_public_ip.*.address, count.index)
  instance_id = element(openstack_compute_instance_v2.controlplane_server.*.id, count.index)
}
### end controlplane provisioning ###
### workers provisioning ###
data "template_file" "worker_user_data" {
  count    = var.workers_count
  template = file("${path.module}/cp_cloud_init.cfg")
  vars = {
    hostname = "w${count.index + 1}.${var.hostname}"
    domain = var.domain
    fqdn = var.cluster
  }
}

resource "openstack_blockstorage_volume_v3" "boot_worker" {
  count    = var.workers_count
  name        = "boot_worker_${count.index + 1}.${var.cluster}"
  size        = var.worker_bootdisk_hdd_size
  image_id    = var.worker_image_id
}

resource "openstack_compute_instance_v2" "worker_server" {
  count    = var.workers_count
  name            = "w${count.index + 1}.${var.hostname}"
  flavor_name     = var.worker_compute_instance
  key_pair        = var.openstack_public_key
  user_data = element(data.template_file.worker_user_data.*.rendered, count.index)
  security_groups = [
    openstack_compute_secgroup_v2.secgroup_internal.id,
    openstack_compute_secgroup_v2.secgroup_users.id
    ]
  block_device {
    uuid                  = element(openstack_blockstorage_volume_v3.boot_worker.*.id, count.index)
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }
  # provisioner "remote-exec" {
  #   inline = [
  #     "echo 'the time is now' >> /var/log/cloud-init-output.log",
  #     "sudo bash -c \"echo 'the time is now' >> /var/log/cloud-init-output.log\"",
  #   ]
  #   connection {
  #       user = "kuber"
  #   }
  # }  

  network {
    name = var.cluster
  }
}

resource "openstack_networking_floatingip_v2" "worker_public_ip" {
  count    = var.workers_count
  pool = data.openstack_networking_network_v2.public.name
}

resource "openstack_compute_floatingip_associate_v2" "worker_external_link" {
  count    = var.workers_count
  floating_ip = element(openstack_networking_floatingip_v2.worker_public_ip.*.address, count.index)
  instance_id = element(openstack_compute_instance_v2.worker_server.*.id, count.index)
}
### end workers provisioning ###
### print output for automation ###

output "internal_ips" {
  value = zipmap(concat(openstack_compute_instance_v2.controlplane_server.*.name, openstack_compute_instance_v2.worker_server.*.name), concat(openstack_compute_instance_v2.controlplane_server.*.network.0.fixed_ip_v4, openstack_compute_instance_v2.worker_server.*.network.0.fixed_ip_v4))
}

locals {
  cp_private_ips = zipmap(openstack_compute_instance_v2.controlplane_server.*.name, openstack_compute_instance_v2.controlplane_server.*.network.0.fixed_ip_v4)
  worker_private_ips = zipmap(openstack_compute_instance_v2.worker_server.*.name, openstack_compute_instance_v2.worker_server.*.network.0.fixed_ip_v4)
  #public_ips = "${zipmap(slice(concat(openstack_compute_instance_v2.controlplane_server.*.name, openstack_compute_instance_v2.worker_server.*.name), 0, length(openstack_compute_floatingip_associate_v2.controlplane_external_link.*.floating_ip)), openstack_compute_floatingip_associate_v2.worker_external_link.*.floating_ip)}"
  worker_public_ips = zipmap(slice(openstack_compute_instance_v2.worker_server.*.name, 0, length(openstack_compute_floatingip_associate_v2.worker_external_link.*.floating_ip)), openstack_compute_floatingip_associate_v2.worker_external_link.*.floating_ip)
  cp_public_ips = zipmap(slice(openstack_compute_instance_v2.controlplane_server.*.name, 0, length(openstack_compute_floatingip_associate_v2.controlplane_external_link.*.floating_ip)), openstack_compute_floatingip_associate_v2.controlplane_external_link.*.floating_ip)
}

output "internet_access_ips" {
  value = merge(local.cp_public_ips, local.worker_public_ips)
}

output "ids" {
  value = zipmap(concat(openstack_compute_instance_v2.controlplane_server.*.name, openstack_compute_instance_v2.worker_server.*.name), concat(openstack_compute_instance_v2.controlplane_server.*.id, openstack_compute_instance_v2.worker_server.*.id))
}