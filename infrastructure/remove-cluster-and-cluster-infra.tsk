#!/bin/bash
#version:1.0
######################################
#arguments: cluster --retain-state
#for example: start-new-cluster.tsk k1000
##########################################
#set general variables
source .generic-code/variables/start-variables
project_dir=$PWD
project_dir=${project_dir%\/$state_dir*} #get all before
root_progress_file="$state_dir/.progress"

return=0
retain_state=$false
argument_retain_state=$empty
retain_existing_state=$true
successes=0
fails=0
time_taken=0
end_result=$empty
result_value=0
cluster_settings=$empty
cluster_state_dir=$empty
servers=$empty
iplist=$empty
iplist_count=0
maclist=$empty
config_backup_time=$empty
arguments=$empty
resupply_state=$true
virsh_destroy=$true
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source $scriptpath/terraform/prepare_terraform.tsk
source $scriptpath/terraform/execute_terraform.tsk
source $scriptpath/terraform/execute_virsh.tsk
function initiate_state() {
    echo "$me ($pid), $cluster: initiate state"
    retain_existing_state=$true

    terraform_dir="$state_dir/$cluster/terraform"
    terraform_operational=$terraform_dir/terraform
    terraform_output=$terraform_operational/output
    if [[ -f $terraform_operational/k8s.tf && -f $terraform_operational/variables.tf && -f $terraform_operational/connection.tf ]]; then
        resupply_state=$false
    fi

    if [[ $resupply_state == $true ]]; then
        if [[ -d $terraform_dir ]]; then rm -rf $terraform_dir; fi
    fi

    state_app_dir=$state_dir/$cluster/infrastructure
    progress_file=$state_app_dir/.progress
    if [[ $retain_existing_state == $false ]]; then
        if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    fi
    mkdir -p $state_app_dir
    touch $progress_file
    touch $root_progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function get_activescripts() {
	active_scripts=0

	find="$me.tsk"
	scriptsactive=$(ps aux | grep $find | grep $cluster | wc -l)
	active_scripts=$(( $scriptsactive - 1 ))
	echo $active_scripts
}
function lb_delete_cluster() { 
    if [[ $virtualisation_type == "virsh" ]]; then
            echo "bash .utility/loadbalancer-wrappers/cluster/remove-cluster.tsk $cluster"
            bash .utility/loadbalancer-wrappers/cluster/remove-cluster.tsk $cluster
    fi
}
function remove_ssh_cluster_admin() {
    itter=0
    for server in $servers; do
        echo "removing ssh entry $server-$cluster from config ~/.ssh/config"
        sed -i "/Host $server-$cluster/,+3d" ~/.ssh/config
    done
}
function deregister_local_infrastructure() {
    if [[ $virtualisation_type == "virsh" ]]; then
        if [[ $add_metallb == $true ]]; then
            echo "bash .utility/dns-intranet/delete_subdomain.tsk $local_hardwaredomain ingress.$cluster"
            bash .utility/dns-intranet/delete_subdomain.tsk $local_hardwaredomain ingress.$cluster
        fi

        itter=0
        t_ips=($iplist)
        dmz_ips=($dmz_iplist)
        for node in $servers; do
            ip=${t_ips[$itter]}
            echo "registring $node.$cluster.$local_hardwaredomain ($ip) for n-able.hosts"
            echo "bash .utility/dns-intranet/delete_subdomain.tsk $local_hardwaredomain $node.$cluster"
            bash .utility/dns-intranet/delete_subdomain.tsk $local_hardwaredomain $node.$cluster

            dmz_ip=${dmz_ips[$itter]}
            echo "registring $node.$cluster.$dmz_hardwaredomain ($dmz_ip) for n-able.dmz"
            echo "bash .utility/dns-intranet/delete_subdomain.tsk $dmz_hardwaredomain $node.$cluster"
            bash .utility/dns-intranet/delete_subdomain.tsk $dmz_hardwaredomain $node.$cluster

            itter=$(( $itter + 1 ))
        done

        arguments=$empty
        if [[ $training != $false && $training != $empty ]]; then arguments=$arguments" --training"; fi
    if [[ $kuber_logon_required == $true ]]; then arguments=$arguments" --logon-needs-k"; fi
        if [[ $kuber_logon_required == $true ]]; then arguments=$arguments" --logon-needs-k"; fi
        echo "$me ($pid), $cluster: bash .utility/dns-internet/delete_all_subdomains.tsk $cluster_operational_tld $arguments"
        result=$(bash .utility/dns-internet/delete_all_subdomains.tsk $cluster_operational_tld $arguments)
        if [[ $result == *"successfully"* ]]; then
            register_result="dns registration succeeded"
            echo "$me ($pid), $cluster: dns $subdomain.$domain registry succeeded"
        else
            register_result="dns registry did not succeed"
            echo "$me ($pid), $cluster: $subdomain.$domain DNS REGISTRYING FAILED, result:$result"
        fi
    fi
}
function remove_cluster_state() {
    if [[ -d $cluster_state_dir ]]; then
        rm -rf $cluster_state_dir
    fi
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
max_arguments=$(( $# + 1 ))
while [[ $itter -lt $max_arguments ]]; do
    argument=$(eval echo "\${$itter}")
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--retain-state" ]]; then
            retain_state=$true
            argument_retain_state=--retain-state
        fi
    else
        if [[ $cluster == $empty ]]; then
            cluster=$argument
            clustername=$cluster
        fi
    fi
    itter=$(( $itter + 1 ))
done

initiate_state
load_cluster_and_default_settings

set_servernames

# scriptsactive=$(ps aux | grep $me.tsk | grep $cluster | wc -l)
# active_scripts=$(( $scriptsactive - 2 ))
# #echo "$me ($pid), $cluster: active-scripts are "$active_scripts
# if [[ $active_scripts -gt 0 ]]; then
#     active_scripts_error
# fi

cluster_state_dir="$state_dir/$cluster"

if [[ $virtualisation_type == "virsh" && $virsh_remote_rack == $true ]]; then
    #logon to rack to make sure fingerprint is saved locally
    result=$(ssh -o StrictHostKeyChecking=accept-new $virsh_rack_selected "x=1" 2>&1)
fi
#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"
#start cluster install

remove_ssh_cluster_admin

lb_delete_cluster

deregister_local_infrastructure

if [[ $resupply_state == $true ]]; then
    prepare_terraform
fi

destroy=$(remove_cluster)
echo "$me ($pid), $cluster: ### terraform destroy result ###"
echo "$me ($pid), $cluster: $destroy"
echo "$me ($pid), $cluster: ### end - terraform destroy result - end ###"
if [[ $destroy != *"success"* && $virsh_destroy == $false ]]; then
    echo "$me ($pid), $cluster: terraform destroy did not work properly, deleting using virsh api"
    virsh_delete_cluster
else
    if [[ $virsh_destroy == $false ]]; then
        echo "$me ($pid), $cluster: terraform has destroyed cluster $cluster"
    else
        virsh_delete_cluster
    fi
fi

remove_cluster_state

end_result="successfully executed $cluster infrastructure cleanup"
result_value=0
finish_root
