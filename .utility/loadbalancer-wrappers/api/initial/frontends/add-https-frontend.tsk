#!/bin/bash
#version:1.0
#
######################################
#arguments: loadbalancer
##########################################
#set general variables
source .generic-code/variables/start-variables

header="--header 'Content-Type: application/json'"
get="--request GET"
post="--request POST"
put="--request PUT"
delete="--request DELETE"
workers=$empty
force_new=$false
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function determine_cluster_workers() {
	itter=$worker_startcount
	get_next_worker=$true
	while [[ $get_next_worker == $true ]]; do
		worker=w$itter
		host=$(host $worker.$cluster.$dmz_hardwaredomain)
		echo "$worker, host result: $host"
		if [[ $host == *"not found"* || $host == *"timed out"* ]]; then
			get_next_worker=$false
		else
			workers=$workers" "$worker
		fi

		itter=$(( $itter + 1 ))
	done
}
#end functions
######################################
#intiating task variables
minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
	argument=$(eval echo "\${$itter}")
	if [[ $loadbalancer == $empty ]]; then
		loadbalancer=$argument
	fi
	itter=$(( $itter + 1 ))
done

load_cluster_default_settings $cluster

server=$loadbalancer.$dmz_hardwaredomain
user="--user dataplaneapi:$loadbalancer_dataplane_pwd"
baseurl=http://$server:5555/v2

#end initiating task variables
#######################################
report_start $*

#echo "curl --silent --location $get $user '$baseurl/info' $header"
result=$(curl --silent --location $get $user ''$baseurl'/info' --header 'Content-Type: application/json')
if [[ $result != *"api"* || $result != *"build_date"* ]]; then
	echo "$me ($pid): unable to query loadbalancer $server, aboarding script. received error result: $result"
	exit 99
fi

#get dataplane version
echo "curl --silent --location $get $user '$baseurl/services/haproxy/configuration/frontends' --header 'Content-Type: application/json'"
result=$(curl --silent --location $get $user ''$baseurl'/services/haproxy/configuration/frontends' --header 'Content-Type: application/json')
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): unable to query loadbalancer $server, aboarding script. received error result: $result"
	exit 99
fi
version=${result#*version\":} #get all after version":"
version=${version%%,*} #get all before first ,
echo "backend-version is $version"

#get new transaction id
echo "curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version"
result=$(curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version)
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): failed to start transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi
transaction_id=${result#*id\":\"} #get all after id":"
transaction_id=${transaction_id%%\"*} #get all before first \"
echo "transaction id is $transaction_id"
transaction="transaction_id=$transaction_id"
echo "change transaction-id is: $transaction_id"

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/frontends?$transaction' --data '{\"name\": \"https_in\",\"mode\": \"tcp\", \"tcplog\": true, \"httplog\": true }'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/frontends?'$transaction'' --data "{\"name\": \"https_in\",\"mode\": \"tcp\", \"tcplog\": true, \"httplog\": true }")
if [[ $result != *"mode"* ]]; then
	echo "$me ($pid): failed to create frontend https_in on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/binds?frontend=https_in&$transaction' --data '{\"address\": \"*\", \"name\": \"https_in\", \"port\": 443 }'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/binds?frontend=https_in&'$transaction'' --data "{\"name\": \"https_in\", \"address\": \"*\", \"port\": 443 }")
if [[ $result != *"address"* ]]; then
	echo "$me ($pid): failed to create bind on frontend https_in on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/http_request_rules?parent_name=https_in&parent_type=frontend&$transaction' --data '{\"index\": 0, \"type\": \"capture\", \"capture_sample\":\"req.hdr(Host)\", \"capture_len\": 50 }'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/http_request_rules?parent_name=https_in&parent_type=frontend&'$transaction'' --data "{\"index\": 0,\"type\": \"capture\", \"capture_sample\":\"req.hdr(Host)\", \"capture_len\": 50 }")
if [[ $result != *"capture_sample"* ]]; then
	echo "$me ($pid): failed to create http-request-rule on frontend https_in on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/tcp_request_rules?parent_name=https_in&parent_type=frontend&$transaction' --data '{\"index\": 0,\"type\": \"inspect-delay\", \"timeout\": 5000 }'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/tcp_request_rules?parent_name=https_in&parent_type=frontend&'$transaction'' --data "{\"index\": 0, \"type\": \"inspect-delay\", \"timeout\": 5000 }")
if [[ $result != *"inspect-delay"* ]]; then
	echo "$me ($pid): failed to create tcp-request-rule on frontend https_in on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/acls?parent_name=https_in&parent_type=frontend&$transaction' --data-raw '{\"acl_name\":\"tls\",\"criterion\":\"req.ssl_hello_type\", \"value\":\"1\", \"index\":0}'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/acls?parent_name=https_in&parent_type=frontend&'$transaction'' --data-raw '{"acl_name":"tls","criterion":"req.ssl_hello_type", "value":"1", "index":0}')
if [[ $result != *"ssl_hello_type"* ]]; then
	echo "$me ($pid): failed to add to https_in acl ssl_hello_type test to loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

echo "curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/tcp_request_rules?parent_name=https_in&parent_type=frontend&$transaction' --data '{\"index\": 0, \"type\": \"content\", \"action\": \"accept\", \"cond\": \"if\", \"cond_test\": \"tls\" }'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/tcp_request_rules?parent_name=https_in&parent_type=frontend&'$transaction'' --data "{\"index\": 0, \"type\": \"content\", \"action\": \"accept\", \"cond\": \"if\", \"cond_test\": \"tls\" }")
if [[ $result != *"cond_test"* ]]; then
	echo "$me ($pid): failed to add to https_in tcp-request-rule, tls for content, on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

#end transaction
echo "curl $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id"
result=$(curl --silent $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id)
if [[ $result != *"success"* ]]; then
	echo "$me ($pid): failed to commit adding frontends transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 96
fi

echo "$me ($pid): successfully added frontends to loadbalancer $server"
exit 0