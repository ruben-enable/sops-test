#!/bin/bash
#version:1.0
#
######################################
#arguments: loadbalancer cluster 
##########################################
#set general variables
source .generic-code/variables/start-variables

header="--header 'Content-Type: application/json'"
get="--request GET"
post="--request POST"
put="--request PUT"
delete="--request DELETE"
workers=$empty
force_new=$false
ingress=$false
arguments=$empty
add_nodes=$false
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function determine_cluster_workers() {
	workers=$empty

	itter=$worker_startcount
	get_next_worker=$true
	while [[ $get_next_worker == $true ]]; do
		worker=w$itter
		host=$(host $worker.$cluster.$dmz_hardwaredomain)
		echo "$me ($pid): $worker, host result: $host"
		if [[ $host == *"not found"* || $host == *"timed out"* ]]; then
			get_next_worker=$false
		else
			workers=$workers" "$worker
		fi

		itter=$(( $itter + 1 ))
	done
}
function load_settings() {
    load_cluster_default_settings $cluster
	if [[ $add_nodes == $false ]]; then
 	   list=($(cat config/$cluster/settings))
		for variable in ${list[@]}; do
			first_character=${variable:0:1}
			if [[ $first_character != "#" ]]; then
				variable=${variable//#/ }
				eval $variable
			fi
		done
	fi
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=2
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
	argument=$(eval echo "\${$itter}")
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--adding-single-nodes" ]]; then
            add_nodes=$true
        fi
    else
		if [[ $loadbalancer == $empty ]]; then
			loadbalancer=$argument
		elif [[ $cluster  == $empty ]]; then
			cluster=$argument
		fi
    fi
	itter=$(( $itter + 1 ))
done
load_settings

server=$loadbalancer.$dmz_hardwaredomain
user="--user dataplaneapi:$loadbalancer_dataplane_pwd"
baseurl=http://$server:5555/v2

#end initiating task variables
#######################################
report_start $*

determine_cluster_workers
echo "$me ($pid): cluster $cluster workers are: $workers"

echo "$me ($pid): curl --silent --location $get $user '$baseurl/info' $header"
result=$(curl --silent --location $get $user ''$baseurl'/info' --header 'Content-Type: application/json')
if [[ $result != *"api"* || $result != *"build_date"* ]]; then
	echo "$me ($pid): unable to query loadbalancer $server, aboarding script. received error result: $result"
	exit 88
fi

echo "$me ($pid): bash .utility/loadbalancer-wrappers/api/create/add-http-backend.tsk $loadbalancer $cluster"
result=$(bash .utility/loadbalancer-wrappers/api/create/add-http-backend.tsk $loadbalancer $cluster)
return=$?
if [[ $return -gt 0 ]]; then
	echo "$me ($pid): adding http backend failed, aboarding script. received error result: $result"
	exit 89
fi

echo "$me ($pid): bash .utility/loadbalancer-wrappers/api/create/add-https-backend.tsk $loadbalancer $cluster"
result=$(bash .utility/loadbalancer-wrappers/api/create/add-https-backend.tsk $loadbalancer $cluster)
return=$?
if [[ $return -gt 0 ]]; then
	echo "$me ($pid): adding https backend failed, aboarding script. received error result: $result"
	exit 90
fi

#determine if backend exists
echo "$me ($pid): curl --silent --location $get $user '$baseurl/services/haproxy/configuration/backends' $header"
result=$(curl --silent --location $get $user ''$baseurl'/services/haproxy/configuration/backends' --header 'Content-Type: application/json')
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): unable to query loadbalancer $server, aboarding script. received error result: $result"
	exit 91
fi
version=${result#*version\":} #get all after version":"
version=${version%%,*} #get all before first ,
echo "$me ($pid): backend-version is $version"

#start transactions
echo "$me ($pid): curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version"
result=$(curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version)
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): failed to start transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 92
fi
transaction_id=${result#*id\":\"} #get all after id":"
transaction_id=${transaction_id%%\"*} #get all before first \"
echo "$me ($pid): transaction id is $transaction_id"
transaction="transaction_id=$transaction_id"
echo "$me ($pid): change transaction-id is: $transaction_id"

if [[ $add_metallb == $false ]]; then
	for worker in $workers; do
		#create http backend redirects
		echo "$me ($pid): curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/servers?backend='http.$cluster'&'$transaction'' --data-raw '{\"name\": \"$worker.$cluster\", \"address\": \"$worker.$cluster.$dmz_hardwaredomain\", \"port\": 80, \"check\": \"enabled\"}'"
		result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/servers?backend='http.$cluster'&'$transaction'' --data-raw '{"name": "'$worker'.'$cluster'", "address": "'$worker'.'$cluster'.'$dmz_hardwaredomain'", "port": 80, "check": "enabled"}')
		if [[ $result != *"$cluster"* ]]; then
			echo "$me ($pid): failed to add server $worker.$cluster.$dmz_hardwaredomain to backend $cluster on loadbalancer $server, aboarding script. received error result: $result"
			exit 95
		fi
		#create https backend redirects
		echo "$me ($pid): curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/servers?backend='https.$cluster'&'$transaction'' --data-raw '{\"name\": \"$worker.$cluster\", \"address\": \"$worker.$cluster.$dmz_hardwaredomain\", \"port\": 443, \"check\": \"enabled\"}'"
		result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/servers?backend='https.$cluster'&'$transaction'' --data-raw '{"name": "'$worker'.'$cluster'", "address": "'$worker'.'$cluster'.'$dmz_hardwaredomain'", "port": 443, "check": "enabled"}')
		if [[ $result != *"$cluster"* ]]; then
			echo "$me ($pid): failed to add server $worker.$cluster.$dmz_hardwaredomain to backend $cluster on loadbalancer $server, aboarding script. received error result: $result"
			exit 96
		fi
	done
else
	#create http backend redirects
	echo "$me ($pid): curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/servers?backend='http.$cluster'&'$transaction'' --data-raw '{\"name\": \"ingress.$cluster\", \"address\": \"ingress.$cluster.$dmz_hardwaredomain\", \"port\": 80, \"check\": \"enabled\"}'"
	result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/servers?backend='http.$cluster'&'$transaction'' --data-raw '{"name": "'ingress'.'$cluster'", "address": "'ingress'.'$cluster'.'$dmz_hardwaredomain'", "port": 80, "check": "enabled"}')
	if [[ $result != *"$cluster"* ]]; then
		echo "$me ($pid): $me ($pid): failed to add server ingress.$cluster.$dmz_hardwaredomain to backend $cluster on loadbalancer $server, aboarding script. received error result: $result"
		exit 97
	fi
	#create https backend redirects
	echo "$me ($pid): curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/servers?backend='https.$cluster'&'$transaction'' --data-raw '{\"name\": \"ingress.$cluster\", \"address\": \"ingress.$cluster.$dmz_hardwaredomain\", \"port\": 443, \"check\": \"enabled\"}'"
	result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/servers?backend='https.$cluster'&'$transaction'' --data-raw '{"name": "'ingress'.'$cluster'", "address": "'ingress'.'$cluster'.'$dmz_hardwaredomain'", "port": 443, "check": "enabled"}')
	if [[ $result != *"$cluster"* ]]; then
		echo "$me ($pid): failed to add server ingress.$cluster.$dmz_hardwaredomain to backend $cluster on loadbalancer $server, aboarding script. received error result: $result"
		exit 98
	fi
fi

#end transaction
echo "$me ($pid): curl $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id"
result=$(curl --silent $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id)
if [[ $result != *"success"* ]]; then
	echo "$me ($pid): failed to commit transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 99
fi

echo "$me ($pid): successfully added cluster $cluster backend to loadbalancer $server"
exit 0