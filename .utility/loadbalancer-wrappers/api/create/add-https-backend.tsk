#!/bin/bash
#version:1.0
#
######################################
#arguments: loadbalancer cluster
##########################################
#set general variables
source .generic-code/variables/start-variables

header="--header 'Content-Type: application/json'"
get="--request GET"
post="--request POST"
put="--request PUT"
delete="--request DELETE"
workers=$empty
force_new=$false
ingress=$false
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function determine_cluster_workers() {
	itter=$worker_startcount
	get_next_worker=$true
	while [[ $get_next_worker == $true ]]; do
		worker=w$itter
		host=$(host $worker.$cluster.$dmz_hardwaredomain)
		echo "$me ($pid): $worker, host result: $host"
		if [[ $host == *"not found"* || $host == *"timed out"* ]]; then
			get_next_worker=$false
		else
			workers=$workers" "$worker
		fi

		itter=$(( $itter + 1 ))
	done
}
#end functions
######################################
#intiating task variables
minimal_number_of_arguments=2
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
	argument=$(eval echo "\${$itter}")
	if [[ $loadbalancer == $empty ]]; then
		loadbalancer=$argument
	elif [[ $cluster  == $empty ]]; then
		cluster=$argument
	fi
	itter=$(( $itter + 1 ))
done

load_cluster_default_settings $cluster

server=$loadbalancer.$dmz_hardwaredomain
user="--user dataplaneapi:$loadbalancer_dataplane_pwd"
baseurl=http://$server:5555/v2

#end initiating task variables
#######################################
report_start $*

#determine if backend exists
echo "$me ($pid): curl --silent --location $get $user '$baseurl/services/haproxy/configuration/backends' $header"
result=$(curl --silent --location $get $user ''$baseurl'/services/haproxy/configuration/backends' --header 'Content-Type: application/json')
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): unable to query loadbalancer $server, aboarding script. received error result: $result"
	exit 99
fi
version=${result#*version\":} #get all after version":"
version=${version%%,*} #get all before first ,
echo "$me ($pid): backend-version is $version"

#start transactions
echo "$me ($pid): curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version"
result=$(curl --silent $post $header $user $baseurl/services/haproxy/transactions?version=$version)
if [[ $result != *"_version"* ]]; then
	echo "$me ($pid): failed to start transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi
transaction_id=${result#*id\":\"} #get all after id":"
transaction_id=${transaction_id%%\"*} #get all before first \"
echo "$me ($pid): transaction id is $transaction_id"
transaction="transaction_id=$transaction_id"
echo "$me ($pid): change transaction-id is: $transaction_id"

#create backend https
echo "$me ($pid): curl --silent $post --header 'Content-Type: application/json' $user '$baseurl/services/haproxy/configuration/backends?$transaction' --data-raw '{\"name\":\"https.$cluster\", \"mode\":\"tcp\", \"balance\": {\"algorithm\":\"roundrobin\"}}'"
result=$(curl --silent $post --header 'Content-Type: application/json' $user ''$baseurl'/services/haproxy/configuration/backends?'$transaction'' --data-raw '{"name":"https.'$cluster'", "mode":"tcp", "balance": {"algorithm":"roundrobin"}}')
if [[ $result != *"$cluster"* ]]; then
	echo "$me ($pid): failed to create https backend for $cluster on loadbalancer $server, aboarding script. received error result: $result"
	exit 98
fi

#end transaction
echo "$me ($pid): curl $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id"
result=$(curl --silent $put --header 'Content-Type: application/json' $user $baseurl/services/haproxy/transactions/$transaction_id)
if [[ $result != *"success"* ]]; then
	echo "$me ($pid): failed to commit transaction on loadbalancer $server, aboarding script. received error result: $result"
	exit 96
fi

echo "$me ($pid): successfully added cluster $cluster https backend to loadbalancer $server"
exit 0