#!/usr/bin/env python

"""
Python wrapper for the dnsmadeeasy RESTful API V2.0.
"""
from __future__ import print_function

import httplib2
import json
from time import strftime, gmtime
import hashlib
import hmac
import sys
import os

class DME2:
    def __init__(self):
        self.api = 'f072d657-7f11-44a0-9ba0-b766f128265d'
        self.secret = '22f077d9-c257-4fdd-8275-22108c57aefd'
        self.baseurl = 'https://api.dnsmadeeasy.com/V2.0/'

    def _headers(self):
        currTime = self._get_date()
        hashstring = self._create_hash(currTime)
        headers = {'x-dnsme-apiKey': self.api,
                   'x-dnsme-hmac': hashstring,
                   'x-dnsme-requestDate': currTime,
                   'content-type': 'application/json'}
        return headers

    def _get_date(self):
        return strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime())

    def _create_hash(self, rightnow):
        return hmac.new(self.secret.encode(), rightnow.encode(), hashlib.sha1).hexdigest()

    def _rest_connect(self, resource, method, data=''):
        http = httplib2.Http()
        response, content = http.request(self.baseurl + resource, method, body=data, headers=self._headers())
        if (response['status'] == "200" or  response['status'] == "201" ):
            if content:
                return json.loads(content.decode('utf-8'))
            else:
                return response
        else:
            print(content)
            raise Exception("Error talking to dnsmadeeasy: " + response['status'])

    ##########################################################################
    #  set trainings account
    ##########################################################################
    def initiate_training(self):
        self.api = '7e80f2e2-16c9-4d6d-a210-0219c0e25fd9'
        self.secret = '30f414e5-fcca-4f09-9cd3-00861b6de807'

    ##########################################################################
    #  /dns/managed - operate on multiple domains for your account
    ##########################################################################
    def get_domains(self):
        """
        Returns list of all domains for your account
        """
        return self._rest_connect('dns/managed', 'GET')['data']

    def test(self):
        """
        Returns list of all domains for your account
        """
        result = "test()"
        if training:
            result = "test(True)"
        return result

    def get_domain_id(self, domainname):
        domains = self.get_domains()
        for domain in domains:
            if domainname == domain['name']:
                return domain['id']
        return 0

    def delete_domains(self, domainIds):
        """
        Deletes all domains for your account specified as a list of their IDs.
        """
        return self._rest_connect('dns/managed', 'DELETE', json.dumps(domainIds))

    def update_domains(self, domainIds, data):
        """
        Updates multiple domains specified by their identifiers.
        """
        domainsData = {'ids': domainIds}
        domainsData.update(data)
        return self._rest_connect('dns/managed', 'PUT', json.dumps(domainsData))

    def add_domains(self, domains):
        """
        Creates multiple domains based on a list of domain names.
        Returns list of domain IDs of created domains.
        """
        data = {'names': domains}
        return self._rest_connect('dns/managed', 'POST', json.dumps(data))

    ##########################################################################
    #  /dns/managed/{domainId} - operate on a single domain
    ##########################################################################

    def get_domain(self, domainId):
        """
        Returns the domain specified by its ID.
        """
        return self._rest_connect('dns/managed/' + str(domainId), 'GET')

    def delete_domain(self, domainId):
        """
        Deletes the specified domain by its ID.
        """
        return self._rest_connect('dns/managed/' + str(domainId), 'DELETE')

    def update_domain(self, domainId, data):
        """
        Updates a domain based on the domain identifier with new data.
        """
        return self._rest_connect('dns/managed/' + str(domainId), 'PUT', json.dumps(data))

    # TODO: Test EVERYTHING BELOW HERE

    ##########################################################################
    #    /dns/managed/{domainId}/records - operate on multiple records for 
    #       one domain
    ##########################################################################

    def get_records_from_domain(self, domainId):
        """
        Returns all records for the specified domain.
        """
        return self._rest_connect('dns/managed/' + str(domainId) + '/records', 'GET')['data']

    def get_domain_subdomains(self, domainId):
        domainrecords = self.get_records_from_domain(domainId)
        result = "["
        for subdomain in domainrecords:
            if result != "[":
                result += ","
            result += "{'subdomain':'%s','ip4':'%s','ttl':'%s'}" % (subdomain['name'],subdomain['value'],subdomain['ttl'])
        result += "]"
        return result

    def get_domain_subdomain(self, domainId, subdomainIn):
        domainrecords = self.get_records_from_domain(domainId)
        result = "["
        for subdomain in domainrecords:
            if subdomain['name'] == subdomainIn:
                if result != "[":
                    result += ","
                result += "{'ip4':'%s','ttl':'%s'}" % (subdomain['value'],subdomain['ttl'])
        result += "]"
        return result

    def get_domain_subdomain_ip(self, domainId, subdomainIn):
        domainrecords = self.get_records_from_domain(domainId)
        result = []
        for subdomain in domainrecords:
            if subdomain['name'] == subdomainIn:
                result.append(subdomain['ip'])
        return result

    def get_domain_subdomain_id(self, domainId, subdomainIn):
        domainrecords = self.get_records_from_domain(domainId)
        result = []
        for subdomain in domainrecords:
            if subdomain['name'] == subdomainIn:
                result.append(subdomain['id'])
        return result

    def delete_records_from_domain(self, domainId, recordIds):
        """
        Deletes the specified records using a list of record identifiers.
        Warning: This is irreversible!
        """
        data = {'ids': recordIds}
        return self._rest_connect('dns/managed/' + str(domainId) + '/records', 'DELETE', json.dump(data))

    def add_record_to_domain(self, domainId, data):
        return self._rest_connect('dns/managed/' + str(domainId) + '/records', 'POST', data)

    def add_subdomain(self,domainId, subdomain, ip):
        record = {
            "name": subdomain,
            "type": "A",
            "value": ip,
            "ttl": "60"
        }
        self.add_record_to_domain(domainId,json.dumps(record))

    def update_record_by_id(self, domainId, recordId, data):
        return self._rest_connect('dns/managed/' + str(domainId) + '/records/' + str(recordId), 'PUT', data)


    def delete_record(self, domainId, recordId):
      return self._rest_connect('dns/managed/' + str(domainId) + '/records/' + str(recordId), 'DELETE')

    def delete_subdomain_from_domain(self, domainId, subdomain):
        idList = self.get_domain_subdomain_id(domainId,subdomain)
        for subId in idList:
            self.delete_record(domainId,subId)

    def get_soa_records(self):
        """
        Displays all custom SOA records defined for an account.
        """
        return self._rest_connect('dns/soa', 'GET')

    def assign_soa_record(self, domainIds, soaId):
        """
        Assigns the domains with the custom SOA record.
        """
        data = {'ids': [str(domainId) for domainId in domainIds], 'soaId': str(soaId), }
        return self._rest_connect('dns/managed', 'PUT', json.dumps(data))

    def add_domains_and_assign_soa(self, domains, soaId):
        """
        Creates the domains and assign the custom SOA to them.
        """
        data = {'soaId': str(soaId), 'names': domains}
        return self._rest_connect('dns/managed', 'POST', json.dumps(data))

    def get_vanity_ns(self):
        """
        Returns a list of all vanity name server groups public and private
         defined within the account.
        """
        return self._rest_connect('dns/vanity', 'GET')['data']

    def get_templates(self):
        """
        Returns all public and private templates within the account.
        """
        return self._rest_connect('dns/template', 'GET')

    def get_template(self, templateId):
        """
        Returns the records within a template based on the template ID
        including their associated record IDs.
        """
        return self._rest_connect('dns/template/' + str(templateId) + '/records?type=A', 'GET')

    def delete_template_from_record(self, templateId, recordId):
        """
        Deletes the record from the template.
        """
        return self._rest_connect('dns/template/' + str(templateId) + '/records?ids=' + str(recordId), 'DELETE')

    def get_transfer_acls(self):
        """
        Returns the list of all AXFR transfer ACL's.
        """
        return self._rest_connect('dns/transferAcl', 'GET')['data']

    def get_security_folders(self):
        """
        Returns a list of folders defined in the account.
        """
        return self._rest_connect('security/folder', 'GET')

    def get_query_usage_all(self):
        """
        Returns full report of query traffic for all months and domains.
        """
        return self._rest_connect('usageApi/queriesApi', 'GET')

    def get_query_usage_by_month(self, year, month):
        """
        Returns query usage for month for all domains.
        """
        return self._rest_connect('usageApi/queriesApi/%s/%s' % (str(year), str(month)))

    def get_query_usage_by_month_and_domain(self, year, month, domainId):
        """
        Returns query usage for month for the domain.
        """
        return self._rest_connect('usggeApi/queriesApi/' +
                                  str(year) + '/' + str(month) +
                                  '/managed/' + str(domainId), 'GET')

    # TODO: rename?
    def set_dns_failover(self, recordId, data):
        """
        Configures DNS Failover for the record.
        """
        return self._rest_connect('monitor/' + str(recordId), 'PUT', json.dump(data))

    # TODO: /dns/secondary
    def secondary_dns_add_domains(self, domains, ipSetId):
        """
        Creates domains under secondary DNS management with the assigned
        IP Set.
        """
        data = {'names': domains, 'ipSetId': str(ipSetId)}
        return self._rest_connect('dns/secondary', 'POST', json.dump(data))

    # TODO: /dns/ipSet
    def secondary_dns_assign_ip_set(self, domainIds, ipSetId):
        """
        Assigns IP Set to the domains under secondary DNS management.
        """
        data = {'ids': [str(domainId) for domainId in domainIds], 'ipSetid': str(ipSetId)}
        return self._rest_connect('dns/secondary', 'PUT', json.dump(data))

    def secondary_dns_delete_domain(self, domainId):
        """
        Delete domain under secondary DNS management.
        """
        return self._rest_connect('dns/secondary' + str(domainId), 'DELETE')

    def get_ip_sets(self):
        """
        Returns a list of secondary IP Sets.
        """
        return self._rest_connect('dns/secondary/ipSet', 'GET')['data']

    def add_ip_set(self, name, ips):
        """
        Creates a secondary DNS IP Set.
        """
        data = {'name': name, 'ips': ips}
        return self._rest_connect('dns/secondary/ipSet', 'POST', json.dump(data))

    def update_ip_set_by_domain(self, domainId, ipSetId, folderId):
        """
        Assigns an IP Set to a secondary DNS domain.
        """
        data = {'ipSetId': int(ipSetId), 'folderId': int(folderId)}
        return self._rest_connect('dns/secondary/' + str(domainId), 'PUT', data)

def usage():
    sys.stderr.write("""Usage: %s [function executed] <function arguments>...
Call can be one of:
    get_domains                                                    Returns available domains
    delete_domains                [domain]                         Delete one or more domains
    add_domains                   [domainnames]                    Add one or more domains
    get_domain                    [domain]                         Return data from domain
    get_domain_subdomains         [domain]                         Return subdomains from domain
    get_domain_subdomain          [domain] [subdomain]             Return specific subdomain from domain
    get_domain_subdomain_ip       [domain] [subdomain]             Return specific subdomain-ip from domain
    get_records_from_domain       [domain]                         Return domain records
    delete_subdomain_from_domain  [domain] [subdomain]             Delete all subdomain records from domain
    add_subdomain                 [domain] [subdomain] [ip4]       Add a subdomain-record to domain
""" % os.path.basename(sys.argv[0]))
    sys.exit(1)

def no_result():
    sys.stderr.write("""No result: not in dns or invalid input
    Call should be one of:
    get_domains                                                    Returns available domains
    delete_domains                [domain]                         Delete one or more domains
    add_domains                   [domainnames]                    Add one or more domains
    get_domain                    [domain]                         Return data from domain
    get_domain_subdomains         [domain]                         Return subdomains from domain
    get_domain_subdomain          [domain] [subdomain]             Return specific subdomain from domain
    get_domain_subdomain_ip       [domain] [subdomain]             Return specific subdomain-ip from domain
    get_records_from_domain       [domain]                         Return domain records
    delete_subdomain_from_domain  [domain] [subdomain]             Delete all subdomain records from domain
    add_subdomain                 [domain] [subdomain] [ip4]       Add a subdomain-record to domain
""")
    sys.exit(10)

if __name__ == "__main__":
    functioncall_items = 1
    if sys.argv == "python":
        functioncall_items = 2

    if len(sys.argv) <= functioncall_items:
        usage()
    else:
     dme = DME2()
     command = sys.argv[functioncall_items]
     if command == "get_domains":
        if len(sys.argv) == 3:
            dme.initiate_training()
        print("domains returned %s" % (dme.get_domains()))
     elif command == "test" :
         if len(sys.argv) == 3:
            dme.initiate_training()
         print("called test as " + dme.test(training))
     elif command == "delete_domains" :
        print("for security reasons: not implemented")

     elif command == "update_domains" :
        print("for security reasons: not implemented")

     elif command == "add_domains" :
        print("for security reasons: not implemented")

     elif command == "get_domain" :
        if len(sys.argv) <= functioncall_items + 1:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: get_domain [domainname]

example: dme2.py getdomain example.com
""")
            sys.exit(1)
        if len(sys.argv) == 4:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            print(dme.get_domain(domainid))
        else:
            no_result()

     elif command == "update_domain" :
        print("for security reasons: not implemented")
        sys.exit(0)

     elif command == "get_records_from_domain" :
        if len(sys.argv) <= functioncall_items + 1:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: get_records_from_domain [domainname]

example: dme2.py get_records_from_domain example.com
""")
            sys.exit(1)
        if len(sys.argv) == 4:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            print(dme.get_records_from_domain(domainid))
        else:
            no_result()

     elif command == "get_domain_subdomains" :
        if len(sys.argv) <= functioncall_items + 1:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: get_domain_subdomains [domainname]

example: dme2.py get_domain_subdomains example.com
""")
            sys.exit(1)
        if len(sys.argv) == 4:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            print(dme.get_domain_subdomains(domainid))
        else:
            no_result()

     elif command == "get_domain_subdomain" :
        if len(sys.argv) <= functioncall_items + 1:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: get_domain_subdomain [domainname]

example: dme2.py get_domain_subdomain example.com show
""")
            sys.exit(1)
        if len(sys.argv) == 4:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        subdomain = sys.argv[functioncall_items+2]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            print(dme.get_domain_subdomain(domainid,subdomain))
        else:
            no_result()

     elif command == "get_domain_subdomain_ip" :
        if len(sys.argv) <= functioncall_items + 2:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: get_domain_subdomain_ip [domainname] [subdomain]

example: dme2.py get_domain_subdomain_ip example.com show
""")
            sys.exit(1)
        if len(sys.argv) == 5:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        subdomain = sys.argv[functioncall_items+2]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            print(dme.get_domain_subdomain_ip(domainid,subdomain))
        else:
            no_result()

     elif command == "delete_subdomain_from_domain" :
        if len(sys.argv) <= functioncall_items + 2:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: delete_subdomain_from_domain [domainname] [subdomain]

example: dme2.py delete_subdomain_from_domain example.com show
""")
            sys.exit(1)
        if len(sys.argv) == 5:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        subdomain = sys.argv[functioncall_items+2]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            dme.delete_subdomain_from_domain(domainid,subdomain)
            print("subdomain %s on domain %s deleted" % (subdomain,domainname))
        else:
            no_result()

     elif command == "add_subdomain" :
        if len(sys.argv) <= functioncall_items + 3:
            sys.stderr.write("""Missing domainname argument: invalid argument call!
Usage: add_subdomain [domainname] [subdomain]

example: dme2.py add_subdomain example.com show
""")
            sys.exit(1)
        if len(sys.argv) == 6:
            dme.initiate_training()
        domainname = sys.argv[functioncall_items+1]
        subdomain = sys.argv[functioncall_items+2]
        ip4 = sys.argv[functioncall_items+3]
        domainid = dme.get_domain_id(domainname)
        if domainid > 0:
            dme.add_subdomain(domainid, subdomain, ip4)
            print("subdomain %s with ip %s added to domain %s" % (subdomain,ip4,domainname))
        else:
            no_result()

     elif command == "update_record_by_id" :
        print("for security reasons: not implemented")
        sys.exit(0)

     elif command == "delete_record" :
        print("for security reasons: not implemented")
        sys.exit(0)

     elif command == "get_soa_records" :
        print("for security reasons: not implemented")
        sys.exit(0)