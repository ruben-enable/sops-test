#!/bin/bash
#version:1.0
#
######################################
#arguments: ssh-profile recursive(optional: default=0)
#where:
#user is the external user used to logon as
#url is the server endpoint to connect to
#
#call: first-logon.tsk user ssh_server
#message may not have empty spaces, spaces have to be substituted for %
##########################################
#set general variables
source .generic-code/variables/start-variables
progress_file="$state_dir/.progress"
end_result=$empty
result_value=0
ip_logon=$false
private_key=$empty
state_app_dir=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function add_color_codes() {
	#bash_colors
	RED="\033[0;31m"
	GREEN="\033[0;32m"
	BLUE="\033[0;34m"
	ORANGE="\033[0;33m"
	PURPLE="\033[0;35m"
	CYAN="\033[0;36m"
	YELLOW="\033[1;33m"
	LIGHT_GRAY="\033[0;37m"
	DARK_GRAY="\033[1;30m"
	LIGHT_RED="\033[1;31m"
	LIGHT_GREEN="\033[1;32m"
	LIGHT_BLUE="\033[1;34m"
	LIGHT_PURPLE="\033[1;35m"
	LIGHT_CYAN="\033[1;36m"
	WHITE="\033[1;37m"
	BLINK="\e[5m"
	BOLD="\e[1m"
	UNDERLINE="\e[4m"
	REVERSE="\e[7m"
	ITALIC="\e[3m"
	NC="\033[0m"
	EXCLAMATION="\xE2\x9D\x97"
	MARK="\xE2\x9D\x8C"
	CHECK_MARK="\xE2\x9C\x85"
	MEDIUM_STAR="\xE2\xAD\x90"
}
add_color_codes
function initiate_state() {
    echo "$me ($pid): initiate state"
    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function known_host_issue() {
	if [[ -f ~/.ssh/known_hosts ]]; then rm ~/.ssh/known_hosts; fi
}
#end functions
######################################
#intiating task variables
recursive=0

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
else
	ssh_profile=$1
fi
if [[ $ssh_server != *"-"* ]]; then
	ip_logon=$true
	Expect="expect $scriptpath/$ip_logon_script"
	private_key=$3
fi

if [[ "$#" -gt 2 ]];then 
    recursive=$3
fi

#end initiating task variables
#######################################
#echo "pid $app $pid executing: $me $*"

if [[ $recursive -eq 4 ]]; then
	echo "reached recursive limit"
	exit 99
fi

arguments=$ssh_profile

echo "expect $scriptpath/firstlogon.exp $arguments"
result=$(expect $scriptpath/firstlogon.exp $arguments)
return=$?
echo "$me ($pid) for $ssh_server: $expect_script returned with, return is $return, result is $result"
if [[ $return -ne 0 ]];then #error or server fingerprint changed...
	empty_known_hosts=false
	case "$return" in
		1)	result="connection refushed by remote server $ssh_server: $ssh_server permissions failure?"
			empty_known_hosts=true
			;;
		8)	result="unable to resolve $ssh_server hostname: DNS configuration error!"
			;;
		9)	result="different fingerprint already present in known_host: unable to comply"
			empty_known_hosts=true
			;;
		11)	result="timeout: response not recognized?"
			;;
		13)	echo "$me ($pid) for $ssh_server: successfully logged into Ubuntu Os"
			exit 0
			;;
		19)	result="different fingerprint already present in known_host: unable to comply"
			empty_known_hosts=true
			;;
		20)	result="failed: explicit permission denied to $ssh_server - human intervention needed?" 
			;;
		21)	result="entry issue: passphrase or password needed. failed due to a permissions or configuration error"
			;;
		31)	result="adding new public key to authorized keys failed"
			;;
		*)	result="unknown issue: script came back with return-error $return"
			;;
	esac
	if [[ $empty_known_hosts == $false ]]; then
		echo "$me ($pid - attempt $recursive) for $ssh_server failed: error $?, $result"
		exit $?
	else
		known_host_issue
		sleep 2
		recursive=$(( $recursive + 1 ))
		result=$($scriptpath/$me.tsk $1 $2 $recursive)
		echo "recursion $recursive: error $?, $result"
		exit $99
	fi
fi

echo "$me ($pid) for $ssh_server: successfully logged into server $ssh_server"
exit 0
