#!/bin/bash
#version:1.0
######################################
#arguments: --routerip=... clusterdomains logon1 logon2 logon2 ... --training
#
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
app=voting-application
task=install-voting-application
install_voting_application=$scriptpath/$task.tsk

kuber_precursor=$empty
state_app_dir=$empty
progress_dir=$empty
logons=$empty
logon_count=0
success_count=0
failure_count=0
failure_list=$empty
variable_by_string=$empty
initiated_scripts=0
pids=$empty
cluster=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $app: initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function cleanup_success_before_start() {
    before_start_success=$(get_success_count)
    create_list_count before_start_success
    if [[ $before_start_success_count -gt 0 ]]; then
        for success in $successes; do
            sed -i "/$success/,d" $progress_file
        done
    fi
}
function get_success_count() {
    pids=$empty
    successes=$(cat $progress_file | grep "successfully" | grep "client-ip_ingress_example")

    for success in $successes; do
        success=${success//_/ }
        for argument in $success; do 
            pid=$empty
            if [[ $argument == *"):"* ]]; then
                pid=${argument//\):/}
                pid=${pid//\(/}
            fi
            if [[ $pid == $empty && $argument == *"),"* ]]; then
                pid=${argument//\),/}
                pid=${pid//\(/}
            fi
            if [[ $pid != $empty ]]; then
                pids=$pids" "$pid
            fi
        done
    done
    pids=$(make_unique $pids)
    echo $pids
}
function get_successes() {
    criterium=$1
    pids=$empty
    successes=$(cat $progress_file | grep "successfully" | grep $app)

    for success in $successes; do
        success=${success//_/ }
        for argument in $success; do 
            if [[ $argument == *"),"* ]]; then
                pid=${argument//\),/}
                pid=${pid//\(/}
                pids=$pids" "$pid
            fi
        done
    done
    pids=$(make_unique $pids)
    echo $pids
}
function get_fails() {
    pids=$empty
    failures=$(cat $progress_file | grep fail)

    for fail in $failures; do
        fail=${fail//_/ }
        for argument in $fail; do 
            if [[ $argument == *"),"* ]]; then
                pid=${argument//\),/}
                pid=${pid//\(/}
                pids=$pids" "$pid
            fi
        done
    done
    pids=$(make_unique $pids)
    echo $pids
}
function evaluate_progress() {
    #initialize variables
    finished=false
    result_count=0
    success_count=0
    failure_count=0
    failure_list=$empty
    result=$empty

    timer_start=$(get_now)
    shout=0
    reporttime=60
    now=$timer_start
    end=$(( $now + 300))

    if [[ ! -d $progress_dir ]]; then mkdir $progress_dir; fi
    if [[ ! -f $progress_file ]]; then touch $progress_file; fi

    #check for progress
    while [[ $now -lt $end && $finished == $false ]]; do
        if [[ $result_count -eq $clusters_count || $result_count -gt $clusters_count ]]; then 
            finished=true
            timein=$(delta_time)" "seconds
            echo "$me ($pid), $app, installing $app: $timein seconds ending evaluation, last status: $result_count/$clusters_count finished clusters (successes $successes_count - fails $fails_count)"
        else
            sleep 10
            successes=$(get_successes $app)
            create_list_count successes
            #echo "successes: $successes, successes count is $successes_count"
            fails=$(get_fails)
            create_list_count fails
            #echo "fails: $fails, fails count is $fails_count"
        fi
        
        result_count=$(( $successes_count + $fails_count ))
        timein=$(delta_time)" "seconds
        shouttime=$(( $(get_now) - $shout ))
        if [[ $shouttime -gt $reporttime ]]; then
            shout=$(get_now)
            echo "$me ($pid), $app, installing $app: $timein in $result_count/$clusters_count finished clusters (successes $successes_count - fails $fails_count)"
        fi      
    done
}
function analyze_results() {
    results=$(cat $progress_file)
    success_count=0
    failure_count=0
    failure_list=$empty
    for result in $results; do
        if [[ $result == *"successfully"* ]]; then
            success_count=$(( $success_count + 1 ))
        else
            failure_count=$(( $failure_count + 1 ))
            failure_list=$failure_list" "$result
        fi
    done
    failure_list=($failure_list)
}
function extract_cluster_from_logon() {
	clusterlogon=$1

	clusterlogon=(${clusterlogon//-/ })
	clusterlogon=${clusterlogon[1]}
    echo $clusterlogon
}
#end functions
######################################
#intiating task variables

if [[ $# -lt 3 ]]; then
    invalid_number_of_arguments
fi

training=$false
itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
    argument=$(echo $(eval echo "\${$itter}")) #read incomming argument to string
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
    else
        if [[ $clusterdomains == $empty ]]; then
            clusterdomains=$argument
        else
            logons=$logons" "$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
create_list_count logons
clusterdomains=${clusterdomains//#/ }
clusterdomains=($clusterdomains)
create_list_count clusterdomains

#create 1000 empty clusterlists as: cluster1,cluster2,...
itter=0
while [[ $itter -lt 1001 ]]; do
    itter=$(( $itter + 1 ))
    eval "cluster$itter=-"
done

#set clusterlists: logon has ..-c2 or ..-c3
for logon in $logons; do
    clusternumber=(${logon//-/ })
    clusternumber=${clusternumber[1]}
    clusternumber=${clusternumber//k/} #remove k

    if [[ $(eval echo "\${cluster$clusternumber}") != "-" ]]; then #check if cluster is already initialized
        newcluster=$(eval echo "\${cluster$clusternumber}")"#"$logon
    else
        newcluster=$logon
    fi
    eval "cluster$clusternumber=\"$newcluster\"" #reintialize clusterlist
done

#determine clusterarray
itter=1
found=0
while [[ $itter -lt 1001 ]]; do #add all initialized clusterlists to cluster array
    tlist=$(eval echo "\${cluster$itter"})
    if [[ $tlist != "-" && $tlist != "" ]]; then
        clusters[$found]=$tlist
        found=$(( $found + 1 ))
    fi
    itter=$(( $itter + 1 ))
done
create_list_count clusters

#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"

itter=0
for clusternodes in ${clusters[@]}; do
    clusternodes=${clusternodes//#/ }
    echo "$me ($pid), $app: processing cluster-nodes $clusternodes"
    create_list_count clusternodes
    cluster=($clusternodes)
    cluster=$(extract_cluster_from_logon $cluster)

    #(re)-initialize variables
    join_statement=$empty
    controlplane_nodes_count=0
    workernodes_count=0
    controlplane_nodes=$empty
    workernodes=$empty

    for item in $clusternodes; do
        nodename=(${item//./ })
        if [[ $nodename == *m* ]]; then #controlplane_node found
            controlplane_nodes=$controlplane_nodes" "${item// /}
        else
            workernodes=$workernodes" "${item// /}
        fi
    done

    controlplane_nodes_count=($controlplane_nodes)
    controlplane_nodes_count=${#controlplane_nodes[@]}
    workernodes_count=($workernodes)
    workernodes_count=${#workernodes_count[@]}

    controlplane_nodes=($controlplane_nodes)
    controlplane_logon=${controlplane_nodes[0]}
    domain=${clusterdomains[$itter]}

    initiate_state
    arguments=$empty
    if [[ $reinstall == $true ]]; then arguments=$arguments" --reinstall"; fi
    if [[ $training != $false && $training != $empty ]]; then arguments=$arguments" --training"; fi
    if [[ $kuber_logon_required == $true ]]; then arguments=$arguments" --logon-needs-k"; fi
    if [[ $routerip != $empty ]]; then arguments=$arguments" --routerip=$routerip"; fi
    #echo "$me ($pid), $app, $cluster: bash $install_voting_application $domain $controlplane_logon $workernodes $arguments"
    bash $install_voting_application $domain $controlplane_logon $workernodes $arguments & #run out of process

    itter=$(( $itter + 1 ))
done
evaluate_progress
analyze_results

if [[ $all_succeeded == $false ]]; then
    echo -e "${RED}rebooting logon(s) $logon failed: unable to comply, aboarding installation script${NC}"
    exit 99
fi

echo -e "${CHECK_MARK} ${GREEN}$me ($pid): successfully finished installing $app${NC}"
exit 0