#!/bin/bash
#version:1.0
#
######################################
#arguments: logon --delete_admission  --version=...(optional) --metallb-installed(optional)
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
before_start_success=0;successes=$empty
task=install_helm
app=cert-manager
end_result=$empty
result_value=0

ingress_version=$empty
cert_version=$empty
cert_crd_version=$empty
delete_admission=$true
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
metallb_installed=$false
add_crds_by_yaml=$false
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function secure_jetstack_repo_presence() {
	repo_present=$(ssh $cluster_controlplane "helm repo list | grep jetstack.io" 2>&1)
	if [[ $repo_present == $empty || $repo_present == *"Error"* ]]; then
		echo "$me ($pid), $cluster: missing jetstack repo, adding: helm repo add jetstack https://charts.jetstack.io"
		result=$(ssh $cluster_controlplane "helm repo add jetstack https://charts.jetstack.io" 2>&1)
		echo "$result"

		echo "$me ($pid), $cluster: updating helm repos"
		result=$(ssh $cluster_controlplane "helm repo update" 2>&1)
		echo "$result"
	fi
}
function validate_certmanager_crd_presence() {
	crds_present=$false

	result=$(ssh $cluster_controlplane "kubectl get crds | grep cert-manager" 2>&1)
	#echo "$me ($pid), $cluster: vccp: $result"
	if [[ $result != $empty && $result != *"No resources found"* ]]; then
		crds_present=$true
	fi

	echo $crds_present
}
function validate_plugin_existence() {
	plugin_is_present=$false

	result=$(ssh $cluster_controlplane "kubectl cert-manager" 2>&1)
	if [[ $result != *"unknown command"* ]]; then
		if [[ $result != *"Approve a CertificateRequest"* ]]; then
			plugin_is_present=$true
		fi
	fi
	echo $plugin_is_present
}
function install_certmanager_kubectl_plugin() {
	echo "$me ($pid), $cluster: installing kubectl cert-manager plugin on server $cluster_controlplane"
	result=$(ssh $cluster_controlplane "curl --silent -L -o kubectl-cert-manager.tar.gz https://github.com/jetstack/cert-manager/releases/latest/download/kubectl-cert_manager-linux-amd64.tar.gz" 2>&1)
	if [[ $result != $empty ]]; then
				end_result="downloading kubectl cert-manager plugin failed. error result: $result"
				result_value=21
				finish
	fi
	result=$(ssh $cluster_controlplane "tar xzf kubectl-cert-manager.tar.gz" 2>&1)
	if [[ $result != $empty ]]; then
				end_result="untarring kubectl-cert-manager (plugin) tar failed. error result: $result"
				result_value=22
				finish
	fi
	result=$(ssh $cluster_controlplane "sudo mv kubectl-cert_manager /usr/local/bin" 2>&1)
	if [[ $result != $empty ]]; then
				end_result="moving the kubectl cert-manager plugin (binary) to /usr/local/bin failed. error result: $result"
				result_value=23
				finish
	fi
	echo "$me ($pid), $cluster: kubectl cert-manager plugin installed on server $cluster_controlplane"
}
function install_certmanager() {
	echo "$me ($pid), $cluster: installing cert-manager"

	plugin_present=$(validate_plugin_existence)
	if [[ $plugin_present == $false ]]; then
		install_certmanager_kubectl_plugin
	fi

	secure_jetstack_repo_presence

	add_crds=$empty
	if [[ $add_crds_by_yaml == $true ]]; then
		echo "$me ($pid), $cluster: ssh $cluster_controlplane kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/$cert_crd_version/cert-manager.crds.yaml"
		result=$(ssh $cluster_controlplane "kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/$cert_crd_version/cert-manager.crds.yaml" 2>&1)
		echo "$result"
	else
		crds_present=$(validate_certmanager_crd_presence)
		echo "$me ($pid), $cluster: validate_certmanager_crd_presence check result is: $crds_present"
		if [[ $crds_present == $false ]]; then 
			add_crds=" --set installCRDs=true"
		fi
	fi
	echo "$me ($pid), $cluster: add_crds is $add_crds"

	version=$empty
	if [[ $certmanager_helm_version != $empty ]]; then helm_package_version="--version $certmanager_helm_version"; fi
	echo "$me ($pid), $cluster: ssh $cluster_controlplane helm install n-able-cm jetstack/cert-manager --namespace kube-system $helm_package_version--set ingressShim.defaultIssuerName=letsencrypt-prod,ingressShim.defaultIssuerKind=ClusterIssuer,ingressShim.defaultIssuerGroup=cert-manager.io $add_crds"
	result=$(ssh $cluster_controlplane helm install n-able-cm jetstack/cert-manager --namespace kube-system $helm_package_version--set ingressShim.defaultIssuerName=letsencrypt-prod,ingressShim.defaultIssuerKind=ClusterIssuer,ingressShim.defaultIssuerGroup=cert-manager.io $add_crds 2>&1)

	if [[ $result != *"deployed successfully"* ]]; then
		if [[ $result == *"Error"* && $result == *"name that is still in use"* ]]; then
			if [[ $recursive -lt 3 ]]; then
				remove_old_certmanager_install
				recursive=$(( $recursive + 1 ))
				install_certmanager
			else
				end_result="failed to remove old n-able-cm install from cluster, unable to installing cert-manager, aboarding task"
				result_value=61
				finish
			fi
		elif [[ $result == *"Error: customresourcedefinitions"* ]]; then
			if [[ $recursive -lt 3 ]]; then
				solve_residue_resource_issue
				recursive=$(( $recursive + 1 ))
				install_certmanager
			else
				end_result="failed to remove residue customresourcedefinitions from cluster, unable to installing cert-manager, aboarding task"
				result_value=70
				finish
			fi
		else
			if [[ $result == *"warning"* ]]; then
				echo -e "${EXCLAMATION}${ORANGE}$me ($pid), $cluster: helm came back with a warning:${NC} $result"
			else  
				end_result="installing cert-manager failed: $result"
				result_value=89
				finish
			fi
		fi
	fi

}
function solve_residue_resource_issue() {
	echo "$me ($pid), $cluster: solving cert-manager residue issue"
	result=$(scp $scriptpath/delete_residue_certmanager_customresourcedefinitions.tsk $cluster_controlplane:issue.tsk 2>&1)
	if [[ $result != $empty ]]; then
		end_result="copying issue task to controlplane-node failed: $result"
		result_value=71
		finish
	fi

	result=$(ssh $cluster_controlplane "bash issue.tsk")
	result=$(ssh $cluster_controlplane "rm issue.tsk")
}
function remove_old_certmanager_install() {
	echo "$me ($pid), $cluster: removing old cert-manager installation"
	echo "$me ($pid), $cluster: ssh $cluster_controlplane helm --namespace kube-system uninstall n-able-cm"
	result=$(ssh $cluster_controlplane helm  --namespace kube-system uninstall n-able-cm)
	#solve_residue_resource_issue
	echo "$me ($pid), $cluster: waiting 30 seconds"
	sleep 30
}
function wait_for_certmanager_to_run() {
  echo "$me ($pid), $cluster: waiting for all cert-manager pods to be (up and) running..."
  running=$false;ready=$false;number_of_pods=0
  status_webhook=$empty;status_cainjector=$empty;status_controler=$empty
  
  start=$(get_now)
  while [[ $running == $false || $ready == $false ]]; do
    sleep 10

	#get data
	pods=$empty
    results=$(ssh $cluster_controlplane "kubectl -n kube-system get pods --no-headers"  2>&1)
	results=(${results// /###})
	for line in ${results[@]}; do 
		if [[ $line == *"cert-manager"* ]]; then
			pods=$pods" "$line
		fi
	done
	number_of_pods=($pods)
	number_of_pods=${#number_of_pods[@]}

	#process data
	running_pods=0
	ready_pods=0
	for pod in $pods; do
		pod=(${pod//###/ })
		podname=${pod[0]}
		podstatus=${pod[2]}
		podready=${pod[1]}
		podready=(${podready//\// })
		pod_containers=${podready[1]}
		containers_ready=${podready[0]}

		if [[ $podname == *"cainjector"* ]]; then
			status_cainjector="($containers_ready/$pod_containers,$podstatus)"
			if [[ $podstatus == "Running" ]]; then
				running_pods=$(( $running_pods + 1 ))
			fi
			if [[ $containers_ready == $pod_containers ]]; then
				ready_pods=$(( $ready_pods + 1 ))
			fi
		elif [[ $podname == *"webhook"* ]]; then
			status_webhook="($containers_ready/$pod_containers,$podstatus)"
			if [[ $podstatus == "Running" ]]; then
				running_pods=$(( $running_pods + 1 ))
			fi
			if [[ $containers_ready == $pod_containers ]]; then
				ready_pods=$(( $ready_pods + 1 ))
			fi
		else
			status_controler="($containers_ready/$pod_containers,$podstatus)"
			if [[ $podstatus == "Running" ]]; then
				running_pods=$(( $running_pods + 1 ))
			fi
			if [[ $containers_ready == $pod_containers ]]; then
				ready_pods=$(( $ready_pods + 1 ))
			fi
		fi
	done

	#evaluate data
	if [[ $running_pods -eq $number_of_pods ]]; then
		running=$true
	fi
	if [[ $ready_pods -eq $number_of_pods ]]; then
		ready=$true
	fi

	#report progression
	now=$(get_now)
    timein=$(( $now - $timer_start ))
    echo "$me ($pid), $cluster: waiting for cert-manager to be ready, $timein seconds in, running pods: $running_pods/$number_of_pods, ready pods: $ready_pods/$number_of_pods. status controler $status_controler, status cainjector $status_cainjector, status webhook $status_webhook"
  done
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

number_arg=$(( $# + 1 ))
argindex=1
while [[ $argindex -lt $number_arg ]]; do
    argument=$(eval echo $(eval echo "$\{$argindex\}")) #read incomming argument to logons string
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
        if [[ $argument == "--version" ]]; then
			certmanager_helm_version=(${argument//=/ })
            certmanager_helm_version=${certmanager_helm_version[1]}
        fi
        if [[ $argument == "--metallb-installed" ]]; then
			metallb_installed=$true
        fi
        if [[ $argument == "--add_crds_by_yaml" ]]; then
			add_crds_by_yaml=$true
        fi
	else
		if [[ $cluster_controlplane == $empty ]]; then
			cluster_controlplane=$argument
		fi
    fi
    argindex=$(( $argindex + 1 ))
done
cluster=($cluster_controlplane)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
initiate_state

#end initiating task variables
#######################################
report_start_to_file

recursive=0
install_certmanager
echo "$me ($pid), $cluster: waiting 20 seconds before continuing"
sleep 20

echo "$me ($pid), $cluster: adding clusterissuers"
#add issuer to server
place_file_on_controlplaneserver $scriptpath/certmanager-clusterissuers.yaml
apply_yaml_to_cluster certmanager-clusterissuers.yaml --no-namespace

wait_for_certmanager_to_run

end_result="successfully installed  $app controler and cert-manager on cluster $cluster (using controlplane $cluster_controlplane)"
result_value=0
finish