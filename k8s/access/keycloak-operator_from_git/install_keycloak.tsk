#!/bin/bash
#version:1.0
#
######################################
#arguments: domainname controlplane_nodes-logonlist workernodes-urllist
#optional switches: --reinstall --training --prd-install
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
before_start_success=0;successes=$empty
task=install_helm
app=keycloak
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
realm=realm-$(head -c 99000 /dev/urandom| tr -dc _A-Z-a-z-0-9 | cut -c1-6).json
rbac=rbac.yaml
realm_source=realm.json
subdomain=keycloak
releasename=n-able-identifier
namespace=keycloak
secret=realm-secret
helm_values_file=values.yaml

scriptfilename=initiate_operator_script.sh
domain=$empty
variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
reinstall=false
reinstall=false
reciproce=0
reinstall=$false
training=$false
prd_install=$false
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
old_install_present=$false
namespace_exists=$false
reinstall_database=$false
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function remove_old_keycloak_install() {
	echo "$me ($pid), $cluster: removing old keycloak installation & waiting for results"
	result=$(ssh $cluster_controlplane kubectl delete namespace $namespace --wait)
}
function add_certificates() {
	cp $scriptpath/ing.yaml $state_app_dir/$subdomain-$domain-ing.yaml
	sed -i 's/#subdomain#/'$subdomain'/g; s/#domain#/'$domain'/g; s/#namespace#/'$namespace'/g;' $state_app_dir/$subdomain-$domain-ing.yaml

	echo "$me ($pid), $cluster: adding keycloak ingress ($subdomain-$domain-ing.yaml)"
	result=$(scp $state_app_dir/$subdomain-$domain-ing.yaml $cluster_controlplane:$subdomain-$domain-ing.yaml 2>&1)
	if [[ $result != "" ]]; then
		end_result="failed to scp $subdomain-$domain-ing.yaml to controlplane: $result"
		result_value=91
		finish
	fi
	result=$(ssh $cluster_controlplane "kubectl apply -f $subdomain-$domain-ing.yaml")
	if [[ $result != *"ingress"* && $result != *"created"* ]]; then
		if [[ $result != *"ingress"* && $result != *"unchanged"* ]]; then
			end_result="failed adding $subdomain-$domain-ing to controlplane: $result"
			result_value=92
			finish
		fi
	fi
	result=$(ssh $cluster_controlplane "rm $subdomain-$domain-ing.yaml")
}
function prepare_secret() {
	secret_exists=false

	#check if secret is already present
	result=$(ssh $cluster_controlplane "kubectl get secret -n $namespace -o go-template --template '{{range .items}}{{.metadata.name}}{{end}}'")
	if [[ $result == *"$secret"* ]]; then
		secret_exists=true
	fi

	if [[ $secret_exists == $true ]]; then
		delete_keycloak_secret
	fi

	prepare_and_scp_realm

	add_keycloak_secret

	#cleanup realm.json
	result=$(ssh $cluster_controlplane "rm realm.json")
}
function add_keycloak_secret() {
	echo "$me ($pid), $cluster: adding secret: kubectl -n $namespace create secret generic $secret --from-file=realm.json"
	result=$(ssh $cluster_controlplane "kubectl -n $namespace create secret generic $secret --from-file=realm.json")
	if [[ $result != *"secret"* && $result != *"created"* ]]; then
		if [[ $result != *"secret"* && $result != *"unchanged"* ]]; then
			end_result="failed adding keycloak secret to controlplane: $result"
			result_value=21
			finish
		fi
	fi
}
function delete_keycloak_secret() {
	echo "$me ($pid), $cluster: deleting existing secret: kubectl -n $namespace delete secret $secret"
	result=$(ssh $cluster_controlplane "kubectl -n $namespace delete secret $secret")
	if [[ $result != *"secret"* && $result != *"deleted"* ]]; then
			end_result="failed deleting keycloak 'configmap' secret: $result"
			result_value=20
			finish
	fi
}
function prepare_and_scp_realm() {

	cp $scriptpath/$realm_source $realm
	sed -i 's/#domain#/'$domain'/g;' $realm

	echo "$me ($pid), $cluster: adding $realm_source to cluster-controlplane ($cluster_controlplane)"
	result=$(scp $realm $cluster_controlplane:realm.json 2>&1)
	if [[ $result != "" ]]; then
		end_result="failed to scp $realm to controlplane (realm.json): $result"
		result_value=81
		finish
	fi

	rm $realm
}
function enable_apiserver_oidc() {
	cp $scriptpath/keycloak_auth_to_apiserver.tsk $state_app_dir/$domain-keycloak_auth_to_apiserver.tsk
	sed -i 's/##subdomain##/'$subdomain'/g; s/##domain##/'$domain'/g; s/##namespace##/'$namespace'/g;' $state_app_dir/$domain-keycloak_auth_to_apiserver.tsk

	echo "$me ($pid), $cluster: extending api-server with keycloak oidc authentication"
	result=$(scp $state_app_dir/$domain-keycloak_auth_to_apiserver.tsk $cluster_controlplane:extend_api-server.tsk 2>&1)
	if [[ $result != "" ]]; then
		end_result="failed to scp keycloak_auth_to_apiserver.tsk to controlplane: $result"
		result_value=91
		finish
	fi
	result=$(ssh $cluster_controlplane "bash extend_api-server.tsk")
	result=$(ssh $cluster_controlplane "rm extend_api-server.tsk")
	sleep 20
}
function add_realm_rbac_to_k8s() {
	rbac_file=$(basename $rbac)
	echo "$me ($pid), $cluster: adding $rbac_file to cluster-controlplane: scp $rbac $cluster_controlplane:rbac.yaml"
	result=$(scp $rbac $cluster_controlplane:rbac.yaml 2>&1)
	if [[ $result != "" ]]; then
		end_result="failed to scp $rbac to controlplane: $result"
		result_value=31
		finish
	fi

	echo "$me ($pid), $cluster: adding rbac: kubectl apply -f rbac.yaml"
	result=$(ssh $cluster_controlplane "kubectl apply -f rbac.yaml")
	if [[ $result != *"role.rbac.authorization.k8s.io"* && $result != *"created"* ]]; then
		if [[ $result != *"role.rbac.authorization.k8s.io"* && $result != *"unchanged"* ]]; then
			end_result="failed adding keycloak groups rbac to controlplane: $result"
			result_value=32
			finish
		fi
	fi
	result=$(ssh $cluster_controlplane rm rbac.yaml)
}
function validate_existence_rbac_namespaces() {
	validate_existence_namespace development
	validate_existence_namespace build
	validate_existence_namespace test
	validate_existence_namespace production
}
function wait_till_keycloak_runs() {
  echo "waiting for keycloak frontend pod to be (up and) running..."
  running=$false;ready=$false
  
  start=$(get_now)
  while [[ $running == $false || $ready == $false ]]; do
    sleep 20

    result=$(ssh $cluster_controlplane "kubectl -n $namespace get pods | grep $releasename-ke"  2>&1) 
    result=($result)
    keycloak_ready=${result[1]}
    keycloak_containers_ready=(${keycloak_ready//\// })
    keycloak_containers=${keycloak_containers_ready[1]}
    keycloak_running=${result[2]}
    now=$(get_now)
    timein=$(( $now - $timer_start ))
    echo "$timein seconds in: keycloak status running: $keycloak_running, keycloak containers: $keycloak_containers, keycloak containers ready: $keycloak_containers_ready"
    if [[ $keycloak_running == *"Running"* ]]; then
      running=$true
    fi
    if [[ $keycloak_containers_ready -eq 1 ]]; then
      ready=$true
    fi
  done

  echo "success"
}
function load_app_settings() {
	valuesfile=values.yaml
	if [[ $prd_install == $true ]]; then
		valuesfile=prd_values.yaml
	fi

    list=($(cat $scriptpath/$valuesfile))
    for variable in ${list[@]}; do
        first_character=${variable:0:1}
        if [[ $first_character != "#" ]]; then
            variable=${variable//#/ }
			#echo "$me ($pid), $cluster: load_settings default.cfg - $variable"
            eval $variable
        fi
    done
}
function add_kuber_precursor() {
	echo "in add_kuber_precursor"
	tnodeslist=$empty
	for worker in $worker_nodeslist; do
		tnodeslist=$tnodeslist" "$kuber_precursor$worker
	done
	worker_nodeslist="$tnodeslist"

	tnodeslist=$empty
	for cp in $controlplane_nodeslist; do
		tnodeslist=$tnodeslist" "$kuber_precursor$cp
	done
	controlplane_nodeslist="$tnodeslist"
}
function remove_old_install() {
	old_install_present=$false

	namespace_exists=$(check_existence_namespace)
	if [[ $namespace_exists == $true ]]; then
		old_install_present=$true
	fi

	if [[ $old_install_present == $true ]]; then
		remove_old_keycloak_install
	fi
}
function get_secret_data() {
	secret_name=$1
	if [[ $# -gt 1 ]]; then
		secret_namespace=$2
	else
		secret_namespace=default
	fi

	echo $(ssh $cluster_controlplane "kubectl -n $secret_namespace get secret $secret_name -o json | jq '.data | map_values(@base64d)'" 2>&1)
	#echo $result
}
function retrieve_postqress_connection_settings() {
	database_name=hippo-ha
	database_secret=$database_name-pguser-$database_name
	data=$(get_secret_data $database_secret $postgres_namespace)
	data=$(echo $data | jq -r '. | to_entries | .[] | .key + "=" + (.value | @sh)')
	
	for item in $data; do
		#separate key and value
		key=${item%%=*} #get all before first =
		key=${key//-/_}
		value=${item#*=} #get all after =
		#move all key - to _
		eval $key=$value
	done
}
function wait_for_keycloak_namespace_pods_running() {
	echo "$me ($pid), $cluster: waiting for all pods in $namespace to be (up and) running..."
	running=$false;ready=$false;number_of_pods=0

	start=$(get_now)
	while [[ $running == $false || $ready == $false ]]; do
		sleep 10

		#get data
		results=$(ssh $cluster_controlplane "kubectl -n $namespace get pods --no-headers"  2>&1)
		results=(${results// /###} )
		number_of_pods=${#results[@]}

		#process data
		running_pods=0
		ready_pods=0
		for pod in ${results[@]}; do
			pod=(${pod//###/ })
			podname=${pod[0]}
			podstatus=${pod[2]}
			podready=${pod[1]}
			podready=(${podready//\// })
			pod_containers=${podready[1]}
			containers_ready=${podready[0]}

			if [[ $podstatus == "Running" || $podstatus == "Completed" ]]; then
				running_pods=$(( $running_pods + 1 ))
			fi
			if [[ $containers_ready == $pod_containers ]]; then
				ready_pods=$(( $ready_pods + 1 ))
			fi
		done

		#evaluate data
		if [[ $running_pods -eq $number_of_pods ]]; then
			running=$true
		fi
		if [[ $ready_pods -eq $number_of_pods ]]; then
			ready=$true
		fi

		#report progression
		now=$(get_now)
		timein=$(( $now - $timer_start ))
		echo "$me ($pid), $cluster: waiting for namespace $namespace pods to be ready, $timein seconds in, running pods: $running_pods/$number_of_pods, ready pods: $ready_pods/$number_of_pods"
	done
}
#end functions
######################################
#intiating task variables
argindex=1

minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
    if [[ $argument == *"--"* ]]; then
		if [[ $argument == "--training" ]]; then
			training=$true
		fi
		if [[ $argument == "--reinstall" ]]; then
			reinstall=$true
		fi
		if [[ $argument == "--reinstall-database" ]]; then
			reinstall_database=$true
		fi
		if [[ $argument == "--prd-install" ]]; then
			prd_install=$true
			subdomain=$prd_keycloak_subdomain
		fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
	else
		if [[ $domain == $empty ]]; then
	        domain=$argument
		else
			if [[ $argument == *"w"* ]]; then
				worker_nodeslist=$worker_nodeslist" "$argument
			else
				controlplane_nodeslist=$controlplane_nodeslist" "$argument
			fi
		fi
	fi
    argindex=$(( $argindex + 1 ))
done
if [[ $kuber_logon_required == $true ]]; then add_kuber_precursor; fi
create_list_count controlplane_nodeslist
create_list_count worker_nodeslist
cluster_controlplane=($controlplane_nodeslist) #get first item of list
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}

initiate_state
realm=$state_app_dir/$realm
rbac=$state_app_dir/$rbac
scripfile=$state_app_dir/$scriptfilename

#if [[ $routerip != $empty ]]; then load_settings; fi
load_settings_k8s
if [[ $prd_install == $true ]]; then
	namespace=iam
	domain=$production_domain
	rbac=$scriptpath/prd_rbac.yaml
	realm_source=prd_$realm_source
	helm_values_file=prd_$helm_values_file
fi
#end initiating task variables
#######################################
report_start_to_file

echo "$me ($pid), $cluster: register keycloak domain $subdomain.$domain (endpoints: $worker_nodeslist)"
register_fqdn $subdomain $domain

echo "$me ($pid), $cluster: keycloak operator sources github.com/keycloak/keycloak-operator to cluster master $cluster_controlplane"
ssh $cluster_controlplane "git clone https://github.com/keycloak/keycloak-operator.git"

#echo "$me ($pid), $cluster: path is $PWD"
if [[ $central_postgres == $true ]]; then
    arguments=$empty
    if [[ $reinstall_database == $true ]]; then arguments=$arguments"--reinstall --reinstall-database"; fi
	echo "$me ($pid), $cluster: bash k8s/add_postgres.tsk $cluster $arguments"
	bash k8s/add_postgres.tsk $cluster $arguments
fi

remove_old_install
validate_existence_namespace $namespace

cat > $scripfile <<EOF
#!/bin/bash
make cluster/prepare
kubectl apply -f deploy/operator.yaml -n $namespace
EOF

echo "$me ($pid), $cluster: adding keycloak operator install script $scriptfilename to cluster master $cluster_controlplane"
result=$(scp $scripfile $cluster_controlplane:keycloak-operator/$scriptfilename 2>&1)
if [[ $result != "" ]]; then
	end_result="failed to scp $scripfile to controlplane: $result"
	result_value=91
	finish
fi

success=$false
echo "$me ($pid), $cluster: executing 'bash keycloak-operator/$scriptfilename' on cluster master $cluster_controlplane"
result=$(ssh $cluster_controlplane "cd keycloak-operator; bash $scriptfilename" 2>&1)
if [[ $result == *"deployment.apps/keycloak-operator created"* || $result == *"deployment.apps/keycloak-operator unchanged"* ]]; then
	if [[ $result == *"keycloakrealms.keycloak.org unchanged"* || $result == *"keycloakrealms.keycloak.org created"* ]]; then
		success=$true
	fi
fi
if [[ $success = $false ]]; then
	end_result="installing operator failed: $result"
	result_value=92
	finish
fi
wait_for_keycloak_namespace_pods_running
echo "$me ($pid), $cluster: operator successfully added!"

retrieve_postqress_connection_settings

#delete old secret
result=$(ssh $cluster_controlplane "kubectl -n $namespace get secrets | grep keycloak-db-secret | wc -l")
if [[ $result -gt 0 ]]; then #secret already present
	echo "$me ($pid), $cluster: removing existing 'keycloak-db-secret' secret"
	result=$(ssh $cluster_controlplane "kubectl -n $namespace delete secret keycloak-db-secret" 2>&1)
fi

#add secret
create_secret_statement="kubectl -n $namespace create secret generic keycloak-db-secret"
create_secret_statement=$create_secret_statement" --from-literal=POSTGRES_DATABASE=$dbname"
create_secret_statement=$create_secret_statement" --from-literal=POSTGRES_EXTERNAL_ADDRESS=$host.cluster.local"
create_secret_statement=$create_secret_statement" --from-literal=POSTGRES_EXTERNAL_PORT=$port"
create_secret_statement=$create_secret_statement" --from-literal=POSTGRES_PASSWORD=\"$password\""
create_secret_statement=$create_secret_statement" --from-literal=POSTGRES_USERNAME=$user"
#echo "$me ($pid), $cluster: $create_secret_statement"
result=$(ssh $cluster_controlplane "$create_secret_statement" 2>&1)
echo "$me ($pid), $cluster: $create_secret_statement, result: $result"
result=$(ssh $cluster_controlplane "kubectl -n $namespace patch secret keycloak-db-secret -p '{\"metadata\":{\"labels\":{\"app\": \"sso\"}}}'" 2>&1)
echo "$me ($pid), $cluster: kubectl -n $namespace patch secret keycloak-db-secret -p '{\"metadata\":{\"labels\":{\"app\": \"sso\"}}}', result: $result"

#delete old admin secret
result=$(ssh $cluster_controlplane "kubectl -n $namespace get secrets | grep credential-example-keycloak | wc -l")
if [[ $result -gt 0 ]]; then #secret already present
	echo "$me ($pid), $cluster: removing existing 'credential-example-keycloak' secret"
	result=$(ssh $cluster_controlplane "kubectl -n $namespace delete secret credential-example-keycloak" 2>&1)
fi

cat > $state_app_dir/keycloak.yaml <<EOF
apiVersion: keycloak.org/v1alpha1
kind: Keycloak
metadata:
  name: example-keycloak
  labels:
    app: sso
spec:
  instances: 3
  # User needs to provision the external database
  externalDatabase:
    enabled: True
  externalAccess:
    enabled: true
    host: $subdomain.$domain
EOF

echo "$me ($pid), $cluster: adding keycloak.yaml to cluster master $cluster_controlplane"
result=$(scp $state_app_dir/keycloak.yaml $cluster_controlplane:keycloak-operator/keycloak.yaml 2>&1)
if [[ $result != "" ]]; then
	end_result="failed to scp $state_app_dir/keycloak.yaml to controlplane: $result"
	result_value=93
	finish
fi

#add keycloak
result=$(ssh $cluster_controlplane "kubectl -n $namespace apply -f keycloak-operator/keycloak.yaml" 2>&1)
echo "$me ($pid), $cluster: kubectl -n $namespace apply -f keycloak-operator/keycloak.yaml, result: $result"
wait_for_keycloak_namespace_pods_running

#add admin secret
# create_admin_secret_statement="kubectl -n $namespace create secret generic credential-example-keycloak"
# create_admin_secret_statement=$create_admin_secret_statement" --from-literal=ADMIN_PASSWORD=\"$keycloak_password\""
# create_admin_secret_statement=$create_admin_secret_statement" --from-literal=ADMIN_USERNAME=$keycloak_username"
# #echo "$me ($pid), $cluster: $create_admin_secret_statement"
# result=$(ssh $cluster_controlplane "$create_admin_secret_statement" 2>&1)
# echo "$me ($pid), $cluster: $create_admin_secret_statement, result: $result"
# result=$(ssh $cluster_controlplane "kubectl -n $namespace patch secret credential-example-keycloak -p '{\"metadata\":{\"labels\":{\"app\": \"keycloak\"}}}'" 2>&1)
# echo "$me ($pid), $cluster: kubectl -n $namespace patch secret credential-example-keycloak -p '{\"metadata\":{\"labels\":{\"app\": \"keycloak\"}}}', result: $result"

add_certificates

echo "$me ($pid), $cluster: removing keycloak operator sources directory on cluster-node $cluster_controlplane"
result=$(ssh $cluster_controlplane "rm -rf ~/keycloak-operator" 2>&1)

end_result="successfully installed  $app on cluster $cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish