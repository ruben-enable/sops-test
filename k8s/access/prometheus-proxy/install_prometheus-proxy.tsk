#!/bin/bash
#version:1.0
#
######################################
#arguments: domainname sso_realm sso_secret controlplane_nodes-logonlist workernodes-urllist
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
task=install_helm
app=prometheus-proxy
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
subdomain=prometheus
releasename=n-able-monitoring
namespace=monitoring

keycloak_subdomain=keycloak

variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
training=$false
reinstall=$false
sso_secret=$empty
sso_realm=$empty
sso_client_id=kubernetes
prd_install=$false
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
#end functions
######################################
#intiating task variables
minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

argindex=1
args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
	#echo "argument is $argument"
    if [[ $argument == *"--"* ]]; then
		if [[ $argument == "--training" ]]; then
			training=$true
		fi
		if [[ $argument == "--reinstall" ]]; then
			reinstall=$true
		fi
		if [[ $argument == "--prd-install" ]]; then
			prd_install=$true
			keycloak_subdomain=$prd_keycloak_subdomain
		fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
	else
		if [[ $domain == $empty ]]; then
	        domain=$argument
		elif [[ $sso_realm == $empty ]]; then
			sso_realm=$argument
		elif [[ $sso_secret == $empty ]]; then
			sso_secret=$argument
		else
			if [[ $argument == *"w"* ]]; then
				worker_nodeslist=$worker_nodeslist" "$argument
			else
				controlplane_nodeslist=$controlplane_nodeslist" "$argument
			fi
		fi
	fi
    argindex=$(( $argindex + 1 ))
done
create_list_count controlplane_nodeslist
create_list_count worker_nodeslist
cluster_controlplane=($controlplane_nodeslist) #get first item of list
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
initiate_state

load_settings_k8s

#end initiating task variables
#######################################
report_start_to_file

echo "$me ($pid), $cluster: register $subdomain-$domain-proxy domain $subdomain.$domain (endpoints: $worker_nodeslist)"
register_fqdn $subdomain $domain

cp $scriptpath/manifest.yaml $scriptpath/$subdomain-$domain-manifest.yaml
sed -i 's/#subdomain#/'$subdomain'/g; s/#domain#/'$domain'/g; s/#namespace#/'$namespace'/g; s/#realm#/'$sso_realm'/g;' $scriptpath/$subdomain-$domain-manifest.yaml
sed -i 's/#keycloak-subdomain#/'$keycloak_subdomain'/g; s/#client-secret#/'$sso_secret'/g; s/#client-id#/'$sso_client_id'/g;' $scriptpath/$subdomain-$domain-manifest.yaml
sed -i 's/#release#/'$releasename'/g;' $scriptpath/$subdomain-$domain-manifest.yaml

echo "$me ($pid), $cluster: adding $subdomain-$domain-proxy service and deployment"
result=$(scp $scriptpath/$subdomain-$domain-manifest.yaml $cluster_controlplane:$subdomain-$domain-proxy.yaml 2>&1)
if [[ $result != $empty ]]; then
	end_result="failed to scp $subdomain-$domain-manifest.yaml to controlplane: $result"
	result_value=91
	finish
fi
result=$(ssh $cluster_controlplane "kubectl apply -f $subdomain-$domain-proxy.yaml")
if [[ $result != *"deployment"* && $result != *"created"* ]]; then
    if [[ $result != *"deployment"* && $result != *"unchanged"* ]]; then
		end_result="failed adding $subdomain-$domain-manifest.yaml to controlplane: $result"
		result_value=92
		finish
	fi
fi
result=$(ssh $cluster_controlplane "rm $subdomain-$domain-proxy.yaml")
rm $scriptpath/$subdomain-$domain-manifest.yaml

cp $scriptpath/ing.yaml $scriptpath/$subdomain-$domain-ing.yaml
sed -i 's/#subdomain#/'$subdomain'/g; s/#domain#/'$domain'/g; s/#namespace#/'$namespace'/g;' $scriptpath/$subdomain-$domain-ing.yaml
sed -i 's/#release#/'$subdomain'-proxy/g;' $scriptpath/$subdomain-$domain-ing.yaml

echo "$me ($pid), $cluster: adding $subdomain-$domain-proxy ingress"
result=$(scp $scriptpath/$subdomain-$domain-ing.yaml $cluster_controlplane:$subdomain-$domain-ing.yaml 2>&1)
if [[ $result != $empty ]]; then
	end_result="failed to scp $subdomain-$domain-ing.yaml to controlplane: $result"
	result_value=91
	finish
fi
result=$(ssh $cluster_controlplane "kubectl apply -f $subdomain-$domain-ing.yaml")
if [[ $result != *"ingress"* && $result != *"created"* ]]; then
    if [[ $result != *"ingress"* && $result != *"unchanged"* ]]; then
		end_result="failed adding $subdomain-$domain-ing to controlplane: $result"
		result_value=92
		finish
	fi
fi
result=$(ssh $cluster_controlplane "rm $subdomain-$domain-ing.yaml")
rm $scriptpath/$subdomain-$domain-ing.yaml

end_result="successfully installed $subdomain-$domain-proxy ($app) on cluster $cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish