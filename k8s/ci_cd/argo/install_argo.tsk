#!/bin/bash
#version:1.0
#
######################################
#arguments: domainname controlplane_nodes-logonlist workernodes-urllist
#optional switches: --reinstall
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
before_start_success=0;successes=$empty
task=install_helm
app=argo
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
subdomain=argo
servicename=argocd-server
wf_servicename=argo-server
serviceport=80
wf_serviceport=2746

variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
reinstall=false
reinstall=false
argo_cli_version=-
retain_tls=false
training=$false
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
kuber_logon_required=$false
state_argocd_dir=$empty
state_workflow_dir=$empty
state_event_dir=$empty
state_rollout_dir=$empty
recursive_itter=0
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
	state_argocd_dir=$state_app_dir/argocd
	state_workflow_dir=$state_app_dir/workflow
	state_event_dir=$state_app_dir/events
	state_rollout_dir=$state_app_dir/rollout
}
function add_argocd() {
	state_argocd_dir=$state_app_dir/argocd
	validate_dir_existence $state_argocd_dir

	validate_existence_namespace $argocd_namespace

	get_internet_file https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml $state_argocd_dir argocd_install.yaml
	echo "adding insecure argument to argocd server deployment, so argo website can work with ingress (tls termination at argocd creates routing error)"
	sed -i "s@- /shared/app@- /shared/app\n        - --insecure@g;" $state_argocd_dir/argocd_install.yaml

	place_file_on_controlplaneserver $state_argocd_dir/argocd_install.yaml

	apply_yaml_to_cluster argocd_install.yaml $argocd_namespace

	argo_cli_version=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')
	echo "$me ($pid), $cluster: latest argocli version found - $argo_cli_version"

	#add argo cli to cluster controlplane
	echo "$me ($pid), $cluster: download argo cli - https://github.com/argoproj/argo-cd/releases/download/$argo_cli_version/argocd-linux-amd64"
	result=$(ssh $cluster_controlplane "sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$argo_cli_version/argocd-linux-amd64" 2>&1)
	if [[ $result != $empty ]]; then
		end_result="iaw1 - adding argo cli failed, result: $result"
		result_value=28
		finish
	fi
	echo $result
	echo "$me ($pid), $cluster: adding cli to cluster controlplane ($cluster_controlplane)"
	result=$(ssh $cluster_controlplane "sudo chmod +x /usr/local/bin/argocd" 2>&1)
	if [[ $result != $empty ]]; then
		end_result="iaw2 - adding argo cli failed, while setting cli rights: $result"
		result_value=29
		finish
	fi
	echo $result

	old_tls_files_present=$(ssh $cluster_controlplane "if [[ -f $argocd_subdomain.$domain-tls.yaml ]]; then echo 'true'; else echo 'false';fi")
	echo "$me ($pid), $cluster: old-tls-present is $old_tls_files_present, retain-tls is $retain_tls"
	if [[ $old_tls_files_present == $true ]]; then
		if [[ $retain_tls == $true ]]; then
			echo "$me ($pid), $cluster: reinstalling old tls certificates"
			result=$(ssh $cluster_controlplane kubectl -n $argocd_namespace apply -f $argocd_subdomain.$domain-tls.yaml)
		else
			echo "$me ($pid), $cluster: not retaining tls, retain tls is set to false"
		fi
		result=$(ssh $cluster_controlplane rm $argocd_subdomain.$domain-tls.yaml)
	fi

	if [[ $argo_all_sso_enabled == $true || $argo_cd_sso_enabled == $true ]]; then
		enable_argocd_sso
	fi
	
}
function enable_argocd_sso() {
	echo "$me ($pid) - cluster $cluster: enabling argocd sso"

	secure_dir_existence $state_argocd_dir/sso
	cp $scriptpath/keycloak-secret.json $state_argocd_dir/sso/keycloak-secret.json
	place_file_on_controlplaneserver $state_argocd_dir/sso/keycloak-secret.json
	result=$(ssh $cluster_controlplane "kubectl -n $argocd_namespace patch secret argocd-secret --patch-file keycloak-secret.json"  2>&1)
	if [[ $result != *"patched"* ]]; then
		end_result="iaw3 - setting up sso, patching argocd secret keycloak-secret failed: $result"
		result_value=26
		finish
	fi
	
	cp $scriptpath/argocd-rbac-cm-patch.yaml $state_argocd_dir/sso/argocd-rbac-cm-patch.yaml
	place_file_on_controlplaneserver $state_argocd_dir/sso/argocd-rbac-cm-patch.yaml
	result=$(ssh $cluster_controlplane "kubectl -n $argocd_namespace patch cm argocd-rbac-cm --patch-file argocd-rbac-cm-patch.yaml"  2>&1)
	if [[ $result != *"patched"* ]]; then
		end_result="iaw4 - setting up sso, patching argocd configmap argocd-rbac-cm failed: $result"
		result_value=28
		finish
	fi

	cp $scriptpath/argocd-cm-sso-patch.yaml $state_argocd_dir/sso/argocd-cm-sso-patch.yaml
    sed -i 's/#subdomain#/'$subdomain'/g; s/#domain#/'$domain'/g;' $state_argocd_dir/sso/argocd-cm-sso-patch.yaml
    sed -i 's/#keycloak-subdomain#/'$argo_keycloak_subdomain'/g; s/#keycloak-domain#/'$argo_keycloak_domain'/g;' $state_argocd_dir/sso/argocd-cm-sso-patch.yaml
    sed -i 's/#keycloak-realm#/'$argo_keycloak_realm'/g; s/#keycloak-clientid#/'$argo_keycloak_clientid'/g;' $state_argocd_dir/sso/argocd-cm-sso-patch.yaml
	place_file_on_controlplaneserver  $state_argocd_dir/sso/argocd-cm-sso-patch.yaml
	result=$(ssh $cluster_controlplane "kubectl -n $argocd_namespace patch cm argocd-cm --patch-file argocd-cm-sso-patch.yaml"  2>&1)
	if [[ $result != *"patched"* && $result != *"unchanged"*  && $result != *"created"* ]]; then
		end_result="iaw5 - setting up sso, patching argocd configmap argocd-cm failed: $result"
		result_value=30
		finish
	fi

	result=$(ssh $cluster_controlplane "rm keycloak-secret.json argocd-rbac-cm-patch.yaml argocd-cm-sso-patch.yaml")
}
function remove_argo() {

	echo "$me ($pid), $cluster: 1 - backing up tls certificates"
	#harbor secret
	secret=$argocd_subdomain.$domain-tls
	key=$(ssh $cluster_controlplane "kubectl -n $argocd_namespace get secret -o=json $secret | jq -r '.data[\"tls.key\"]'")
	cert=$(ssh $cluster_controlplane "kubectl -n $argocd_namespace get secret -o=json $secret | jq -r '.data[\"tls.crt\"]'")
    cp $scriptpath/tls-secret.yaml $state_app_dir/$secret.yaml
    sed -i "s/##domain##/$argocd_subdomain.$domain/g; s/##secret##/$secret/g; s/##cert##/$cert/g; s/##key##/$key/g;" $state_app_dir/$secret.yaml
	place_file_on_controlplaneserver $state_app_dir/$secret.yaml

	echo "$me ($pid), $cluster: 2 - deleting argocd"
	result=$(ssh $cluster_controlplane "kubectl delete -n $argocd_namespace -f argo-install.yaml" 2>&1)
	echo $result
	result=$(ssh $cluster_controlplane "rm argo-install.yaml")

	echo "$me ($pid), $cluster: 2 - deleting namespace"
	result=$(ssh $cluster_controlplane "kubectl delete namespace $argocd_namespace"  2>&1)

	started=$(get_now)
	last_lap=$started
	namespace_gone=$false
	while [[ $argocd_namespace_gone == $false ]]; do
		result=$(ssh $cluster_controlplane "kubectl get namespace $argocd_namespace  --no-headers"  2>&1)
		echo "deleted namespace $argocd_namespace query - result: $result"
		if [[ $result == $empty || $result == *"not found"* ]]; then
			namespace_gone=$true
		else
			time_lapsed=$(( $(get_now) - $last_lap ))
			wait_time=$(( $(get_now) - $started ))
			if [[ $time_lapsed -gt 60 ]]; then
				echo "$me ($pid), $cluster - remove-argo-workflow: waiting for namespace $argocd_namespace to be gone ($wait_time seconds in)"
				last_lap=$(get_now)

				result=$(ssh $cluster_controlplane "kubectl patch ns/$argocd_namespace -p '{"metadata":{"finalizers":[]}}' --type=merge"  2>&1)
			fi

			sleep 4
		fi
	done
}
function correct_dns_worker_nodeslist() {
	corrected_list=$empty
	for wn in $worker_nodeslist; do
		if [[ $corrected_list != $empty ]]; then corrected_list=$corrected_list" "; fi
		if [[ $wn == *kw* ]]; then
			wn=${wn//kw/w}
		fi
		corrected_list=$corrected_list$wn
	done

	echo $corrected_list
}
function add_dns_registration() {
	if [[ $domain != "-" && $domain != $emptystring ]]; then
		register_fqdn $argocd_subdomain $domain
		register_fqdn $argo_workflow_subdomain $domain
		register_fqdn $argo_events_subdomain $domain
		register_fqdn $argo_rollouts_subdomain $domain
		register_fqdn $argo_keycloak_subdomain $domain
		if [[ $argo_workflow_add_training_projects_setup == $true ]]; then
			register_fqdn training-projects.gitlab.events.argo $domain
		fi
	else
		register_result="no dns registration ordered"
	fi
}
function add_workflow() {
	echo "$me ($pid), $cluster: adding argo-workflow"
	state_workflow_dir=$state_app_dir/workflows
	validate_dir_existence $state_workflow_dir

	validate_existence_namespace $argo_workflow_namespace
	
	add_workflow_ingress

	if [[ $argo_workflow_enable_offloading == $true ]]; then
		if [[ $argo_workflow_offloading_database == "postgres" ]]; then
			echo -e "$me ($pid), $cluster: argo-workflow offloading enabled, adding postgres db, ${BLUE}bash $k8s_dir/add_postgres.tsk $cluster --argo-install${NC}"
			result=$(bash $k8s_dir/add_postgres.tsk $cluster --argo-install 2>&1)
			echo "$result"
		fi
	fi
	#reload runtime variables to take in the new created secrets
	load_runtime_state $cluster

	argo_wf_version=$(curl --silent "https://api.github.com/repos/argoproj/argo-workflows/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')
	echo "$me ($pid), $cluster: latest argo-workflow version found - $argo_wf_version"

	internetfile_to_cluster https://github.com/argoproj/argo-workflows/releases/download/$argo_wf_version/install.yaml $state_workflow_dir $argo_workflow_namespace workflow_"$argo_wf_version"_install.yaml
	
	if [[ $argo_all_sso_enabled == $true || $argo_workflows_sso_enabled == $true ]]; then
		enable_argoworkflow_sso
	else
		enable_argoworkflow_server_mode
	fi
	if [[ $argo_workflow_enable_offloading == $true ]]; then
		enable_argoworkflow_offloading
	fi
	if [[ $argo_workflow_enable_artifact_repository == $true ]]; then
		enable_argoworkflow_artifact_repository
	fi
function enable_argoworkflow_artifact_repository() {
	echo "$me ($pid), $cluster: enabling argo-workflow artifact repository"
	if [[ $argo_workflow_artifact_repository_type == "storage_provider" ]]; then
		if [[ $storage_provider == "rook" ]]; then
			result=$(ssh $cluster_controlplane "kubectl -n $rook_namespace get cephobjectstores --no-headers | grep Connected" 2>&1)
			if [[ $result == $empty ]]; then
				end_result="iaw13 - setting up argo-workflows artifact repository failed, no connected ceph object store found in rook namespace $rook_namespace"
				result_value=19
				finish
			fi
			add_rook_artifact_repository
		else
			end_result="iaw6 - setting up argo-workflows artifact repository failed, due to an invalid cluster configuration: storage provider $storage_provider does not support artifact repository (S3) native"
			result_value=19
			finish
		fi
	elif [[ $argo_workflow_artifact_repository_type == "minio" ]]; then
		x=1 #TODO
	fi
}
function add_rook_artifact_repository() {
	echo "$me ($pid), $cluster: adding rook artifact repository"
	s3_creds=argo-workflows-objectstore-creds
	state_artifact_dir=$state_app_dir/artifact-repository
	validate_dir_existence $state_artifact_dir

	cat <<EOF > $state_artifact_dir/bucket-claim.yaml
apiVersion: objectbucket.io/v1alpha1
kind: ObjectBucketClaim
metadata:
  name: $argo_workflow_artifact_bucket_name
  namespace: $argo_workflow_namespace
spec:
  generateBucketName: $argo_workflow_artifact_bucket_name
  storageClassName: ceph-bucket
EOF
	place_file_on_controlplaneserver $state_artifact_dir/bucket-claim.yaml
	apply_yaml_to_cluster bucket-claim.yaml --no-namespace

	recursive_itter=0
	create_bucket_access_secret
	host=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get cm $argo_workflow_artifact_bucket_name -o jsonpath='{.data.BUCKET_HOST}'" 2>&1).cluster.local
	bucket=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get configmap $argo_workflow_artifact_bucket_name -o jsonpath={.data.BUCKET_NAME}" 2>&1)
	port=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get cm $argo_workflow_artifact_bucket_name -o jsonpath='{.data.BUCKET_PORT}'" 2>&1)

	#add s3 artifact persistence to configmap
	cp $scriptpath/argoworkflow-cm-persistence-s3.yaml $state_artifact_dir/argoworkflow-cm-artifact-patch.yaml
	sed -i 's/#bucket#/'$bucket'/g; s/#endpoint#/'$host':'$port'/g; s/#insecure#/'$argo_workflow_artifact_insecure_bucket'/g;' $state_artifact_dir/argoworkflow-cm-artifact-patch.yaml
	sed -i 's/#s3-creds#/'$s3_creds'/g;' $state_artifact_dir/argoworkflow-cm-artifact-patch.yaml

	place_file_on_controlplaneserver $state_artifact_dir/argoworkflow-cm-artifact-patch.yaml
	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace patch cm workflow-controller-configmap --patch-file argoworkflow-cm-artifact-patch.yaml"  2>&1)
	if [[ $result != *"patched"* && $result != *"unchanged"*  && $result != *"created"* ]]; then
		end_result="iaw14 - setting up s3 artifact, patching argocd configmap argocd-cm failed: $result"
		result_value=21
		finish
	fi
	result=$(ssh $cluster_controlplane "rm argoworkflow-cm-artifact-patch.yaml")

}
function remove_argo_workflow() {

	echo "$me ($pid), $cluster: deleting workflow namespace"
	result=$(ssh $cluster_controlplane "kubectl delete -n $argo_workflow_namespace -f argo-workflow-install.yaml" 2>&1)
	result=$(ssh $cluster_controlplane "kubectl delete namespace $argo_workflow_namespace"  2>&1)
	
	started=$(get_now)
	last_lap=$started
	namespace_gone=$false
	while [[ $namespace_gone == $false ]]; do
		result=$(ssh $cluster_controlplane "kubectl get namespace $argo_workflow_namespace  --no-headers"  2>&1)
		echo "$me ($pid), $cluster: deleted namespace $argo_workflow_namespace query - result: $result"
		if [[ $result == $empty || $result == *"not found"* ]]; then
			namespace_gone=$true
		else
			time_lapsed=$(( $(get_now) - $last_lap ))
			wait_time=$(( $(get_now) - $started ))
			if [[ $time_lapsed -gt 60 ]]; then
				echo "$me ($pid), $cluster - remove-argo-workflow: waiting for namespace $argo_workflow_namespace to be gone ($wait_time seconds in)"
				last_lap=$(get_now)

				result=$(ssh $cluster_controlplane "kubectl patch ns/$argo_workflow_namespace -p '{"metadata":{"finalizers":[]}}' --type=merge"  2>&1)
			fi

			sleep 4
		fi
	done
}
function add_workflow_ingress() {
	#set and add ingress
	#echo "$me ($pid), $cluster: cp $scriptpath/ing.yaml $state_workflow_dir/$argo_workflow_subdomain.$domain-ing.yaml"
	cp $scriptpath/ing.yaml $state_workflow_dir/$argo_workflow_subdomain.$domain-ing.yaml
	sed -i "s/#subdomain#/$argo_workflow_subdomain/g; s/#domain#/$domain/g;" $state_workflow_dir/$argo_workflow_subdomain.$domain-ing.yaml
	sed -i "s/#service#/$wf_servicename/g; s/#serviceport#/$wf_serviceport/g;" $state_workflow_dir/$argo_workflow_subdomain.$domain-ing.yaml
	
	place_file_on_controlplaneserver $state_workflow_dir/$argo_workflow_subdomain.$domain-ing.yaml

	apply_yaml_to_cluster $argo_workflow_subdomain.$domain-ing.yaml $argo_workflow_namespace
}
function add_events() {
	echo "$me ($pid), $cluster: adding argo-events"

	validate_dir_existence $state_event_dir

	validate_existence_namespace $argo_events_namespace
	#nats dockerhub container used by eventbus: secure login to avoid rate limits
	sa_add_dockerhub_login default $argo_events_namespace

	add_events_ingress

	#crds
	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install.yaml $state_event_dir $argo_events_namespace events-crds.yaml
	
	#eventbus
	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-events/stable/examples/eventbus/native.yaml $state_event_dir $argo_events_namespace events-eventbus.yaml

	#webhook
	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-events/stable/manifests/install-validating-webhook.yaml $state_event_dir $argo_events_namespace events-validating-webhook.yaml
	

	#add serviceaccount to manipulate workflows
	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-events/master/examples/rbac/sensor-rbac.yaml $state_event_dir $argo_events_namespace
	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-events/master/examples/rbac/workflow-rbac.yaml $state_event_dir $argo_events_namespace

	if [[ $argo_workflow_add_training_projects_setup == $true ]]; then
		enable_argoworkflow_training_projects
	fi
}
function enable_argoworkflow_training_projects(){
	echo "$me ($pid), $cluster: adding eventsource,sensor,workflow(template)s for training projects"
	cp $scriptpath/training-setup/training-projects.yaml $state_workflow_dir/training-projects.yaml
	if [[ $production_cluster == $true ]]; then
		sed -i "s/#domain#/$cluster_operational_tld/g;" $state_workflow_dir/training-projects.yaml
	else
		sed -i "s/#domain#/$cluster_operational_tld/g;" $state_workflow_dir/training-projects.yaml
	fi
	placeholder_replace $state_workflow_dir/training-projects.yaml
	apply_yaml_to_cluster training-projects.yaml $argo_events_namespace
}
function remove_argo_events() {

	echo "$me ($pid), $cluster: deleting events namespace ($argo_events_namespace)"
	result=$(ssh $cluster_controlplane "kubectl delete -n $argo_events_namespace -f argo-install.yaml" 2>&1)
	result=$(ssh $cluster_controlplane "kubectl delete namespace $argo_events_namespace"  2>&1)
	
	started=$(get_now)
	last_lap=$started
	namespace_gone=$false
	while [[ $namespace_gone == $false ]]; do
		result=$(ssh $cluster_controlplane "kubectl get namespace $argo_events_namespace  --no-headers"  2>&1)
		echo "deleted namespace $argo_events_namespace query - result: $result"
		if [[ $result == $empty || $result == *"not found"* ]]; then
			namespace_gone=$true
		else
			time_lapsed=$(( $(get_now) - $last_lap ))
			wait_time=$(( $(get_now) - $started ))
			if [[ $time_lapsed -gt 60 ]]; then
				echo "$me ($pid), $cluster - remove-argo-workflow: waiting for namespace $argo_events_namespace to be gone ($wait_time seconds in)"
				last_lap=$(get_now)

				result=$(ssh $cluster_controlplane "kubectl patch ns/$argo_events_namespace -p '{"metadata":{"finalizers":[]}}' --type=merge"  2>&1)
			fi

			sleep 4
		fi
	done
}
function add_events_ingress() {
	#set and add ingress
	cp $scriptpath/ing-events.yaml $state_event_dir/$argo_events_subdomain.$domain-ing.yaml
	sed -i "s/#subdomain#/$argo_events_subdomain/g; s/#domain#/$domain/g;" $state_event_dir/$argo_events_subdomain.$domain-ing.yaml
	sed -i "s/#service#/$servicename/g; s/#serviceport#/$serviceport/g;" $state_event_dir/$argo_events_subdomain.$domain-ing.yaml

	place_file_on_controlplaneserver $state_event_dir/$argo_events_subdomain.$domain-ing.yaml
	apply_yaml_to_cluster $argo_events_subdomain.$domain-ing.yaml $argo_events_namespace
}
function add_rollouts() {
	echo "$me ($pid), $cluster: adding argo-rollouts"

	validate_dir_existence $state_rollout_dir
	validate_existence_namespace $argo_rollouts_namespace

	add_rollouts_ingress

	internetfile_to_cluster https://raw.githubusercontent.com/argoproj/argo-rollouts/stable/manifests/install.yaml $state_rollout_dir $argo_rollouts_namespace rollouts-install.yaml
}
function remove_argo_rollouts() {

	echo "$me ($pid), $cluster: deleting rollouts namespace"
	result=$(ssh $cluster_controlplane "kubectl delete -n $argo_rollouts_namespace -f argo-install.yaml" 2>&1)
	result=$(ssh $cluster_controlplane "kubectl delete namespace $argo_rollouts_namespace"  2>&1)

	started=$(get_now)
	last_lap=$started
	namespace_gone=$false
	while [[ $namespace_gone == $false ]]; do
		result=$(ssh $cluster_controlplane "kubectl get namespace $argo_rollouts_namespace  --no-headers"  2>&1)
		echo "deleted namespace $argo_rollouts_namespace query - result: $result"
		if [[ $result == $empty || $result == *"not found"* ]]; then
			namespace_gone=$true
		else
			time_lapsed=$(( $(get_now) - $last_lap ))
			wait_time=$(( $(get_now) - $started ))
			if [[ $time_lapsed -gt 60 ]]; then
				echo "$me ($pid), $cluster - remove-argo-workflow: waiting for namespace $argo_rollouts_namespace to be gone ($wait_time seconds in)"
				last_lap=$(get_now)

				result=$(ssh $cluster_controlplane "kubectl patch ns/$argo_rollouts_namespace -p '{"metadata":{"finalizers":[]}}' --type=merge"  2>&1)
			fi

			sleep 4
		fi
	done
}
function add_rollouts_ingress() {
	#set and add ingress
	#echo "$me ($pid), $cluster: cp $scriptpath/ing.yaml $state_app_dir/$argo_rollouts_subdomain.$domain-ing.yaml"
	cp $scriptpath/ing.yaml $state_rollout_dir/$argo_rollouts_subdomain.$domain-ing.yaml
	sed -i "s/#subdomain#/$argo_rollouts_subdomain/g; s/#domain#/$domain/g;" $state_rollout_dir/$argo_rollouts_subdomain.$domain-ing.yaml
	sed -i "s/#service#/$servicename/g; s/#serviceport#/$serviceport/g;" $state_rollout_dir/$argo_rollouts_subdomain.$domain-ing.yaml

	place_file_on_controlplaneserver $state_rollout_dir/$argo_rollouts_subdomain.$domain-ing.yaml

	apply_yaml_to_cluster $argo_rollouts_subdomain.$domain-ing.yaml $argo_rollouts_namespace
}
function add_server_gui_ingress() {
	#set and add ingress
	#echo "$me ($pid), $cluster: cp $scriptpath/ing.yaml $state_argocd_dir/$argocd_subdomain.$domain-ing.yaml"
	cp $scriptpath/ing.yaml $state_argocd_dir/$argocd_subdomain.$domain-ing.yaml
	sed -i "s/#subdomain#/$subdomain/g; s/#domain#/$domain/g;" $state_argocd_dir/$argocd_subdomain.$domain-ing.yaml
	sed -i "s/#service#/$servicename/g; s/#serviceport#/$serviceport/g;" $state_argocd_dir/$argocd_subdomain.$domain-ing.yaml

	place_file_on_controlplaneserver $state_argocd_dir/$argocd_subdomain.$domain-ing.yaml

	apply_yaml_to_cluster $argocd_subdomain.$domain-ing.yaml $argocd_namespace
}
function workflow_roles_and_bindings_to_namespace() {
	i_namespace=$1

	#add rbac service accounts
	if [[ $i_namespace == $argo_workflow_namespace ]]; then
		cp $scriptpath/argo-workflow-sso-rbac-r_and_rb-generic.yaml $state_workflow_dir/sso/"$i_namespace"_sso_roles-and-bindings.yaml
	else
		cp $scriptpath/argo-workflow-sso-rbac-r_and_rb-namespaces.yaml $state_workflow_dir/sso/"$i_namespace"_sso_roles-and-bindings.yaml
	fi
    sed -i 's/#namespace#/'$i_namespace'/g;' $state_workflow_dir/sso/"$i_namespace"_sso_roles-and-bindings.yaml

	place_file_on_controlplaneserver $state_workflow_dir/sso/"$i_namespace"_sso_roles-and-bindings.yaml
	apply_yaml_to_cluster "$i_namespace"_sso_roles-and-bindings.yaml --no-namespace
}
function workflow_service_accounts_to_namespace() {
	i_namespace=$1
	itter=$2

	#add rbac service accounts
	if [[ $i_namespace == $argo_workflow_namespace ]]; then
		cp $scriptpath/argo-workflow-sso-rbac-sa-generic.yaml $state_workflow_dir/sso/"$i_namespace"_sso_service-accounts.yaml
	else
		cp $scriptpath/argo-workflow-sso-rbac-sa-namespaces.yaml $state_workflow_dir/sso/"$i_namespace"_sso_service-accounts.yaml
    	sed -i 's/#itter#/'$itter'/g;' $state_workflow_dir/sso/"$i_namespace"_sso_service-accounts.yaml
	fi
    sed -i 's/#namespace#/'$i_namespace'/g;' $state_workflow_dir/sso/"$i_namespace"_sso_service-accounts.yaml

	place_file_on_controlplaneserver $state_workflow_dir/sso/"$i_namespace"_sso_service-accounts.yaml
	apply_yaml_to_cluster "$i_namespace"_sso_service-accounts.yaml --no-namespace
}
function add_argoworkflow_rbac() {
	namespaces=$(ssh $cluster_controlplane "kubectl get namespaces --no-headers" 2>&1) 
	namespaces=${namespaces// /#}
	namespaces=($namespaces)

	itter=1
	for i_namespace in "${namespaces[@]}"; do
		i_namespace=(${i_namespace//#/ })
		if [[ $i_namespace != *"kube-"* && $i_namespace != "default" ]]; then
			workflow_service_accounts_to_namespace $i_namespace $itter
			workflow_roles_and_bindings_to_namespace $i_namespace
			itter=$((itter+1))
		fi
	done
}
function enable_argoworkflow_sso() {
	echo "$me ($pid) - cluster $cluster: enabling argo-workflow sso"
	secure_dir_existence $state_workflow_dir/sso

	result=$(ssh $cluster_controlplane "kubectl create secret -n $argo_workflow_namespace generic argo-workflows-sso --from-literal=client-id=kubernetes --from-literal=client-secret=6878f3cc-b199-4895-b419-4f2be7d0da05"  2>&1)
	if [[ $result != *"already exists"* && $result != *"created"* ]]; then
		end_result="iaw7 - setting up sso, adding secret argo-workflows-sso failed: $result"
		result_value=19
		finish
	fi

	#add oidc data to configmap
	cp $scriptpath/argoworkflow-cm-sso-patch.yaml $state_workflow_dir/sso/argoworkflow-cm-sso-patch.yaml
    sed -i 's/#subdomain#/'$argo_workflow_subdomain'/g; s/#domain#/'$domain'/g;' $state_workflow_dir/sso/argoworkflow-cm-sso-patch.yaml
    sed -i 's/#keycloak-subdomain#/'$argo_keycloak_subdomain'/g; s/#keycloak-domain#/'$argo_keycloak_domain'/g;' $state_workflow_dir/sso/argoworkflow-cm-sso-patch.yaml
    sed -i 's/#keycloak-realm#/'$argo_keycloak_realm'/g; s/#keycloak-clientid#/'$argo_keycloak_clientid'/g;' $state_workflow_dir/sso/argoworkflow-cm-sso-patch.yaml
	place_file_on_controlplaneserver $state_workflow_dir/sso/argoworkflow-cm-sso-patch.yaml
	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace patch cm workflow-controller-configmap --patch-file argoworkflow-cm-sso-patch.yaml"  2>&1)
	if [[ $result != *"patched"* && $result != *"unchanged"*  && $result != *"created"* ]]; then
		end_result="iaw8 - setting up sso, patching argocd configmap argocd-cm failed: $result"
		result_value=21
		finish
	fi
	result=$(ssh $cluster_controlplane "rm argoworkflow-cm-sso-patch.yaml")

	add_argoworkflow_rbac

	#if not already there, activate sso during startup
	#kubectl patch array
	#jq replace array
	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get deployment argo-server -o json | jq '.spec.template.spec.containers[0].args = [\"server\", \"--auth-mode=sso\", \"--access-control-allow-origin=true\"]' | kubectl apply -f -"  2>&1) 
	if [[ $result != *"configured"* && $result != *"created"* ]]; then
		end_result="iaw9 - setting up sso, adding secret client-id-secret failed: $result"
		result_value=24
		finish
	fi
}
function enable_argoworkflow_server_mode() {
	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get deployment argo-server -o json | jq '.spec.template.spec.containers[0].args = [\"server\", \"--auth-mode=server\", \"--access-control-allow-origin=true\"]' | kubectl apply -f -"  2>&1) 
	if [[ $result != *"configured"* && $result != *"created"* ]]; then
		end_result="iaw10 - setting up sso, adding secret client-id-secret failed: $result"
		result_value=24
		finish
	fi
}
function create_postgres_access_secret() {
	secret_name=$1

	if [[ $postgress_operator == "cloudnative" ]]; then
		#echo "$me ($pid) - cluster $cluster: kubectl -n $argo_workflow_namespace create secret generic $secret_name --from-literal=username=$runtime_pg_dbuser --from-literal=password=$runtime_pg_user_password"
		result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace create secret generic $secret_name --from-literal=username=$runtime_pg_dbuser --from-literal=password=$runtime_pg_user_password" 2>&1)
	else
		result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace create secret generic $secret_name --from-literal=username=postgres --from-literal=password=$postgres_superuser_password" 2>&1)
	fi
	echo "$result"
	if [[ $result != *"configured"* && $result != *"created"* && $result != *"unchanged"* ]]; then
		if [[ $result != *"already exists"* || $recursive_itter -gt 2 ]]; then
			end_result="iaw11 - setting up offloading, adding secret $secret_name failed: $result"
			result_value=24
			finish
		else
			recursive_itter=$(($recursive_itter+1))
			result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace delete secret $secret_name" 2>&1)
			create_postgres_access_secret $secret_name
		fi
	fi
}
function create_bucket_access_secret() {

	S3_HOST=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get cm $argo_workflow_artifact_bucket_name -o jsonpath='{.data.BUCKET_HOST}'" 2>&1)
	S3_BUCKET=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get configmap $argo_workflow_artifact_bucket_name -o jsonpath={.data.BUCKET_NAME}" 2>&1)
	S3_PORT=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get cm $argo_workflow_artifact_bucket_name -o jsonpath='{.data.BUCKET_PORT}'" 2>&1)
	S3_ACCESS_KEY_ID=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get secret $argo_workflow_artifact_bucket_name -o jsonpath='{.data.AWS_ACCESS_KEY_ID}' | base64 --decode" 2>&1)
	S3_SECRET_ACCESS_KEY=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace get secret $argo_workflow_artifact_bucket_name -o jsonpath='{.data.AWS_SECRET_ACCESS_KEY}' | base64 --decode" 2>&1)

	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace create secret generic $s3_creds --from-literal=bucket=$S3_BUCKET --from-literal=endpoint=$S3_HOST:$S3_PORT  --from-literal=insecure=true --from-literal=access_key=$S3_ACCESS_KEY_ID --from-literal=secret_key=$S3_SECRET_ACCESS_KEY" 2>&1)
	echo "$result"
	if [[ $result != *"configured"* && $result != *"created"* && $result != *"unchanged"* ]]; then
		if [[ $result != *"already exists"* || $recursive_itter -gt 2 ]]; then
			end_result="iaw15 - setting up rook s3 bucket, adding secret $s3_creds failed: $result"
			result_value=24
			finish
		else
			recursive_itter=$(($recursive_itter+1))
			result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace delete secret $s3_creds" 2>&1)
			create_bucket_access_secret
		fi
	fi
}
function enable_argoworkflow_offloading() {
	echo "$me ($pid) - cluster $cluster: enabling argo-workflow offloading"
	recursive_itter=0
	secure_dir_existence $state_workflow_dir/offloading
	p_secret_name=argo-workflow-postgres

	create_postgres_access_secret $p_secret_name

	#add oidc data to configmap
	cp $scriptpath/argoworkflow-cm-persistence-postgres.yaml $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	if [[ $postgress_operator == "cloudnative" ]]; then
		sed -i 's/#service#/'$argo_workflow_offloading_postgress_service_name'-rw/g;' $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	else
		sed -i 's/#service#/'$argo_workflow_offloading_postgress_service_name'/g;' $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	fi
	sed -i 's/#serviceport#/'$argo_workflow_offloading_postgress_service_port'/g;' $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	sed -i 's/#database#/'$argo_workflow_offloading_postgress_dbname'/g; s/#tablename#/'$argo_workflow_offloading_postgress_db_tablename'/g;' $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	sed -i 's/#k8s-secret#/'$p_secret_name'/g;' $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	place_file_on_controlplaneserver $state_workflow_dir/offloading/argoworkflow-cm-offloading-patch.yaml
	result=$(ssh $cluster_controlplane "kubectl -n $argo_workflow_namespace patch cm workflow-controller-configmap --patch-file argoworkflow-cm-offloading-patch.yaml"  2>&1)
	if [[ $result != *"patched"* && $result != *"unchanged"*  && $result != *"created"* ]]; then
		end_result="iaw12- setting up postgres offloading, patching argocd configmap argocd-cm failed: $result"
		result_value=21
		finish
	fi
	result=$(ssh $cluster_controlplane "rm argoworkflow-cm-offloading-patch.yaml")
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

argindex=1
args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
	#echo "argument is $argument"
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
		if [[ $argument == "--training" ]]; then
			training=$true
		fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
		if [[ $argument == "--logon-needs-k" ]]; then
			kuber_logon_required=$true
            kuber_precursor=k
		fi
    else
		if [[ $domain == $empty ]]; then
			domain=$argument
		elif [[ $argument == *"w"* ]]; then
			worker_nodeslist=$worker_nodeslist" "$argument
		else
			controlplane_nodeslist=$controlplane_nodeslist" "$argument
		fi
	fi
    argindex=$(( $argindex + 1 ))
done
create_list_count controlplane_nodeslist
create_list_count worker_nodeslist
cluster_controlplane=($controlplane_nodeslist) #get first item of list
if [[ $kuber_logon_required == $true ]]; then
	cluster_controlplane=k$cluster_controlplane
fi
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
initiate_state

load_settings_k8s
if [[ $argo_keycloak_domain == $empty ]]; then
	argo_keycloak_domain=$domain
fi
#end initiating task variables
#######################################
report_start_to_file

# bash .utility/loadbalancer-wrappers/cluster/add-endpoint.tsk $argo_events_subdomain $domain $cluster
# add_events
# dev_break

# c_worker_nodeslist=$(correct_dns_worker_nodeslist)
# add_dns_registration

# if [[ $reinstall == $true ]]; then #reinstall: delete release and refresh volumes
# 	remove_argo
# fi

# echo "$me ($pid) - cluster $cluster: adding argocd"
# add_argocd

# add_server_gui_ingress

# if [[ $add_argoworkflows == $true ]]; then
# 	#add argo workflow
# 	echo "$me ($pid) - cluster $cluster: adding argo-workflows"
# 	add_workflow
# fi

if [[ $add_argoevents == $true ]]; then
	#add argo events
	echo "$me ($pid) - cluster $cluster: adding argo-events"
	add_events
fi

if [[ $add_argorollouts == $true ]]; then
	#add argo 
	add_rollouts
fi

end_result="successfully added $app to cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish