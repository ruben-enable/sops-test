#!/bin/bash
#version:1.0
######################################
#arguments: website masternodes-logonlist workernodes-urllist
#optional switches: --reinstall --no-import
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
app=wordpress-application
task=install_helm
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk

garuantee_new_install=$true
variable_by_string=$empty
cluster_controlplane=$empty
masterservers=$empty
workerservers=$empty
register_result=$empty
reinstall=$false
reinstall=$false
recursive=0
wrapper_recursion=0
dbhostname=$empty
wp_backoffice_release=$empty
wp_frontoffice_release=$empty
db_release=$empty
dbhost=$empty
db_storage_class=$empty
file_storage_class=$empty
frontoffice_file_storage_class=$empty
mail_error=$false
import_existing_website=$true
helm_listing=$empty
initiated_scripts=0
pids=$empty
cluster=$empty
add_nfs_provider=$true
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app/$wordpresstype
    progress_file=$state_dir/$cluster/software/cluster-software/$app/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    if [[ ! -d $state_app_dir ]]; then mkdir -p $state_app_dir; fi
    if [[ ! -f $progress_file ]]; then touch $progress_file; fi
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function set_helm_listing() {
		helm_listing=$(ssh $cluster_controlplane helm -n $NAMESPACE list)
}
function delete_helm_release() {
	releasename=$1

	if [[ $helm_listing == $empty ]]; then
		set_helm_listing
	fi

	if [[ $helm_listing == *"$releasename"* ]]; then
		echo "$me ($pid), $cluster - $wordpresstype: deleting wordpress backup instances: helm -n $NAMESPACE del $releasename"
		result=$(ssh $cluster_controlplane helm -n $NAMESPACE del $releasename 2>&1)
		if [[ $result != *"release \"$releasename\" uninstalled"* ]]; then
			echo "$me ($pid), $cluster - $wordpresstype: deleting wordpress backup helm release $releasename failed, result returned: $result"
		fi
	else
		echo "$me ($pid), $cluster - $wordpresstype: information - namespace $NAMESPACE does not have a release $releasename: doing nothing."
	fi
}
function add_setting() {
	#echo "$me ($pid), $cluster - $wordpresstype: adding setting $1"
	eval "$1"
}
function get_settings() {
	echo "$me ($pid), $cluster - $wordpresstype: function: get_settings"
	sourcefile=$1

	#get website meta
	meta_data=$(cat $scriptpath/setting-files/$sourcefile.settings)
	meta_data=($meta_data)
	meta_count=${#meta_data[@]}
	if [[ $meta_count -gt 0 ]]; then
		for result in "${meta_data[@]}";
		do
			argument=$( cut -d '=' -f 1 <<< "$result" ) #get al before the first occurence of =
			value=${result#*=} #get all after =
			add_setting "$argument=$value"
		done
	else
		end_result="unable to load import metadata from file $sourcefile"
		result_value=99
		finish
	fi
    add_setting FILE_STORAGE_GISIZE=$FILE_STORAGE_GISIZE"Gi"
    add_setting DB_STORAGE_GISIZE=$DB_STORAGE_GISIZE"Gi"
    NAMESPACE=${NAMESPACE//_/-}
    RELEASE=${RELEASE//_/-}
    wp_backoffice_release=$RELEASE
    wp_frontoffice_release=$FRONTOFFICE_RELEASE
    db_release=db
    dbhost=$db_release"-"$HADB"-0."$db_release"-"$HADB

	file_storage_class=$FILE_STORAGE_STORAGECLASS
	db_storage_class=$DB_STORAGE_STORAGECLASS
	if [[ $add_nfs_provider == $true ]]; then
		file_storage_class=$NFS_CSTOR_STORAGECLASS
		frontoffice_file_storage_class=$empty
		db_storage_class=$CSTOR_STORAGECLASS
	fi

	if [[ $SKIP_IMPORT == $true ]]; then 
		import_existing_website=$false
	fi
}
#end functions
######################################
#intiating task variables
in_arguments=$@
argindex=1

minimal_number_of_arguments=2
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
	#echo "$me ($pid), cluster $cluster  - $wordpresstype:argument $argindex: $argument"
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--reinstall" ]]; then
            reinstall=$true
        fi
        if [[ $argument == "--no-import" ]]; then
			import_existing_website=$false
        fi
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
			echo "$me ($pid), cluster $cluster  - $wordpresstype:routerip is $routerip"
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
        if [[ $argument == *"--helm-install"* ]]; then
			helm_install=(${argument/=/ })
            helm_install=${helm_install[1]}
        fi
        if [[ $argument == *"--storage-provider"* ]]; then
			storage_provider=(${argument/=/ })
            storage_provider=${storage_provider[1]}
        fi
        if [[ $argument == *"--no-nfs"* ]]; then
			add_nfs_provider=$false
        fi
    else
		if [[ $wordpresstype == $empty ]]; then
        	wordpresstype=$argument
		else
			if [[ $argument == *"w"* && $argument != *"--"* ]]; then
				workerservers=$workerservers" "$argument
			else
				if [[ $argument != *"--"* ]]; then
					masterservers=$masterservers" "$argument
				fi
			fi
		fi
	fi
    argindex=$(( $argindex + 1 ))
done
cluster_controlplane=($masterservers)
cluster_controlplane=$cluster_controlplane #clustermaster is first item clustermaster array
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
cluster=($masterservers)
cluster=(${cluster//-/ })
cluster=${cluster[1]}

#get and set import variables
initiate_state
if [[ $routerip != $empty ]]; then load_settings; fi
get_settings $wordpresstype

#end initiating task variables
#######################################
report_start_to_file


end_result="successfully added mysqladmin to  $app '$wordpresstype' in cluster ($masterservers $workerservers). $register_result"
result_value=0
finish