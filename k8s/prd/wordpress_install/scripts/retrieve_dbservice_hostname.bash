#!/bin/bash
#version:1.0
#
######################################
#arguments: retrieve_dbservice_hostname namespace servicename
##########################################
#set general variables
servicename='nothing'

initiated_scripts=0
pids=$empty
cluster=$empty
#end general variables
##########################################
#functions

#End Functions
####################################################
#get input

namespace=$1
servicename=$2

#end input
#######################################
#do the heavy lifting
if [[ -f ~/database_hostname ]]; then rm ~/database_hostname; fi
found_hostnames=$empty
hostname=$empty

results=$(kubectl -n $namespace get service | grep $servicename)
for result in $results; do
    if [[ $result == *"$servicename"* ]]; then
        if [[ $result != *"readonly"* ]]; then
            found_hostnames=$found_hostnames" "$result
        fi
    fi
done
if [[ $found_hostnames != $empty ]]; then
    found_hostnames=($found_hostnames)
    if [[ ${#found_hostnames[@]} -eq 1 ]]; then
        hostname=${found_hostnames[0]}
    fi
fi

echo $hostname
exit 0