#!/bin/bash
#version:1.0
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
config_synchr_task=~/kubectl_config_synchronize.tsk

one_controlplane=$false
controlplane_nodeslist=$empty
cluster_controlplane=$empty
controlplane_set=$false
controlplane_node=$empty
controlplane_logon=$empty
failresult=$empty
ssh_precursor=$empty
arguments=$empty
reinstall=$false
reinstall_database=$false
argo_install=false
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function report_time() {
    text=$*
    now=$(date +%s)
    time_taken=$(( $now - $timer_start ))
    echo $text" "$time_taken" seconds"

}
function count() {
    count=($*)
    echo ${#count[@]}
}
function report_delta_time() {
    delta=$1
    itter=2
    text=$empty
    while [[ $itter -lt $(( $# + 1 )) ]]; do
        if [[ $text != $empty ]]; then text=$text" "; fi
        text=$text$(eval echo "\${$itter}")
        itter=$(( $itter + 1 ))
    done

    time_taken=$(( $delta - $timer_start ))
    echo $text" "$time_taken" seconds"

}
function update_lb_access() {
    website_subdomain=$1
    echo "bash .utility/loadbalancer-wrappers/cluster/remove-endpoint.tsk $website_subdomain $cluster_operational_tld"
    bash .utility/loadbalancer-wrappers/cluster/remove-endpoint.tsk $website_subdomain $cluster_operational_tld
    echo "bash .utility/loadbalancer-wrappers/cluster/add-endpoint.tsk $website_subdomain $cluster_operational_tld $cluster"
    bash .utility/loadbalancer-wrappers/cluster/add-endpoint.tsk $website_subdomain $cluster_operational_tld $cluster
}
function create_serverslist() {
    servers=$empty

    teller=1
    cpc=$(( $control_planes + 1 ))
    while [[ $teller -lt $cpc ]]; do
        server="m"$teller
        servers=$servers" "$server
        teller=$(( $teller + 1 ))
    done

    teller=1
    cpc=$(( $workers + 1 ))
    while [[ $teller -lt $cpc ]]; do
        server="w"$teller
        servers=$servers" "$server
        teller=$(( $teller + 1 ))
    done
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

argindex=1
args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
    if [[ $argument == *"--"* ]]; then
		if [[ $argument == "--reinstall" ]]; then
			reinstall_commandline_argument=$true
		fi
		if [[ $argument == "--reinstall-database" ]]; then
			reinstall_database=$true
		fi
		if [[ $argument == "--argo-install" ]]; then
			argo_install=$true
		fi
	else
		if [[ $cluster == $empty ]]; then
	        cluster=$argument
		fi
	fi
    argindex=$(( $argindex + 1 ))
done

load_settings_k8s
if [[ $kuber_logon_required == $true ]]; then ssh_precursor=k; fi
if [[ $reinstall_commandline_argument == $true ]]; then reinstall=$true; fi

create_serverslist
#retrieve a controlplane_node (from the cluster settings)
for server in $servers; do
  if [[ $server == *"m"* ]]; then
      controlplane_node=$server
      controlplane_logon=$kuber_precursor$controlplane_node-$cluster
      if [[ $one_controlplane == $true ]]; then 
        if [[ $controlplane_set == $false ]]; then
          controlplane_set=$true
          logons=$logons" "$controlplane_logon
          controlplane_nodeslist=$controlplane_nodeslist" "$controlplane_logon
        fi
      else
        logons=$logons" "$controlplane_node"-"$cluster
         controlplane_nodeslist=$controlplane_nodeslist" "$controlplane_node"-"$cluster
      fi
  else
    logons=$logons" "$server"-"$cluster
  fi
done
echo "servers are $servers, controlplane_node is $controlplane_node"
if [[ $controlplane_node == $empty ]]; then
    echo "$me ($pid) failed: unable to continue, not able to pinpoint a valid cluster ($cluster) controlplane_node. -incomplete cluster settings?-"
    exit 70
fi
cluster_controlplane=($controlplane_nodeslist) #get first item of list

production=$empty
if [[ $production_cluster == $true ]]; then
  production="--prd-install"
fi
#end initiating task variables
#######################################
#start cluster install

if [[ $virtualisation_type == "virsh" ]]; then
    arguments=$arguments" "--routerip=$external_ip_router
fi
if [[ $cluster_operational_tld == *"part"* && $cluster_operational_tld == *"xyz"* ]]; then
    arguments=$arguments" "--training
fi
if [[ $kuber_logon_required == $true ]]; then arguments=$arguments" --logon-needs-k"; fi
if [[ $reinstall == $true ]]; then arguments=$arguments" --reinstall"; fi
if [[ $reinstall_database == $true ]]; then arguments=$arguments" --reinstall-database"; fi
if [[ $argo_install == $true ]]; then arguments=$arguments" --argo-install"; fi

#add postgres
echo "bash k8s/databases/postgres/$postgress_operator/manage_install.tsk $cluster_operational_tld $logons $arguments $production "
bash k8s/databases/postgres/$postgress_operator/manage_install.tsk $cluster_operational_tld $logons $arguments $production

exit 0