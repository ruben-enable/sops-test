#!/bin/bash
#version:1.0
#
######################################
#arguments: website controlplane_nodes-logonlist workernodes-urllist
#optional switches: --reinstall --do_not_retain_tls --simple --clair-pvc
##########################################
function generate_random_str() {
	result=$(head -c 99000 /dev/urandom| tr -dc a-z-0-9 | cut -c1-$1)
	echo ${result//-/t}
}
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
before_start_success=0;successes=$empty
task=install_helm
app=harbor
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
namespace=harbor
releasename=$namespace
postgresql_release=postgresql-$(generate_random_str 6)
postgresql_passwd=$empty
core=core
notary=notary
clair_pvc=false
simple_install=false

variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
reinstall=false
recursive=0
dbhostname=$empty
wp_release=$empty
db_release=$empty
dbhost=$empty
mail_error=false
training=$empty
retain_tls=true
ha_checkdone=false
core_domain=$empty
notary_domain=$empty
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function add_helm_repo() {
	repo_present=$(ssh $cluster_controlplane "helm repo list | grep harbor" 2>&1)
	if [[ $repo_present == $empty || $repo_present == *"Error"* ]]; then
		echo "$me ($pid) - cluster $cluster: missing ingress nginx repo, adding: helm repo add harbor https://helm.goharbor.io"
		result=$(ssh $cluster_controlplane "helm repo add harbor https://helm.goharbor.io" 2>&1)
		echo $result

		echo "$me ($pid) - cluster $cluster: updating helm repos"
		result=$(ssh $cluster_controlplane "helm repo update" 2>&1)
		echo $result
	fi
}
function secure_bitnami_repo_presence() {
	repo_present=$(ssh $cluster_controlplane "helm repo list | grep bitnami" 2>&1)
	if [[ $repo_present == $empty || $repo_present == *"Error"* ]]; then
		echo "$me ($pid) - cluster $cluster: missing ingress nginx repo, adding: helm repo add bitnami https://charts.bitnami.com/bitnami"
		result=$(ssh $cluster_controlplane "helm repo add bitnami https://charts.bitnami.com/bitnami" 2>&1)
		echo $result

		echo "$me ($pid) - cluster $cluster: updating helm repos"
		result=$(ssh $cluster_controlplane "helm repo update" 2>&1)
		echo $result
	fi
}
function add_ha_postgres() {

	existing_install=($(ssh $cluster_controlplane "helm -n $namespace list | grep postgresql"))
	if [[ $existing_install != $empty ]]; then #already a postgresql installation there...
		remove_old_harbor_install
	fi

	secure_bitnami_repo_presence

	echo "$me ($pid), $cluster: helm install $postgresql_release -n $namespace bitnami/postgresql-ha --set postgresql.replicaCount=5,persistence.size=4Gi"
	result=$(ssh $cluster_controlplane helm install $postgresql_release -n $namespace bitnami/postgresql-ha --set postgresql.replicaCount=5,persistence.size=4Gi 2>&1)
	if [[ $result != *"STATUS: deployed"* ]]; then
		if [[ $result == *"Error"* && $result == *"name that is still in use"* ]]; then
			if [[ $recursive -lt 3 ]]; then
				remove_old_harbor_install
				recursive=$(( $recursive + 1 ))
				add_ha_postgres
			else
				end_result="failed to remove old harbor install from cluster, unable to install harbor, aboarding task"
				result_value=60
				finish
			fi
		else
			end_result="installing postgresql-ha failed: $result"
			result_value=90
			finish
		fi
	fi
	if [[ $ha_checkdone == $false ]]; then #check only ones
		ha_checkdone=true
		sleep 10
		postgresql_passwd=$(ssh $cluster_controlplane "kubectl get secret --namespace $namespace $postgresql_release-postgresql-ha-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode" 2>&1)
		echo "$me ($pid), $cluster: retrieved postgresql passwd: $postgresql_passwd"
		echo "$me ($pid), $cluster: waiting for postgresql to come up..."
		psql_available=false
		wait=$empty
		while [[ $psql_available == $false ]]; do
			result=$(ssh $cluster_controlplane "kubectl -n harbor get pods | grep ha-pgpool" 2>&1)
			status=($result)
			status=${status[1]}
			if [[ $status == "1/1" ]]; then
				psql_available=true
			else
				wait=$wait"."
				echo $wait
				sleep 5
			fi
		done
		echo "$me ($pid), $cluster: postgresql ha is available"

		echo "$me ($pid), $cluster: checking postgresql ha on working..."
		psql_available=false
		while [[ $psql_available == $false ]]; do
			result=$(ssh $cluster_controlplane "kubectl run postgresql-client-availability-check --rm --tty -i --restart='Never' --namespace $namespace --image bitnami/postgresql:11 --env="PGPASSWORD=$postgresql_passwd" --command -- psql -h $postgresql_release-postgresql-ha-pgpool -p 5432 -U postgres -d postgres -c \"\l\"" 2>&1)
			if [[ $result == *"List of databases"* ]]; then
				psql_available=true
				echo "$me ($pid), $cluster: postgresql ha is available"
			else
				echo "$me ($pid), $cluster: postgresql unavailable: $result"
				sleep 2
			fi
		done

		dbs="Harbor_core Clair Notary_signer Notary_server registry"
		for database in $dbs; do
			dblc=${database,,} #database name to lowercase
			dblc=${dblc//_/-} #_ to -
			echo "$me ($pid), $cluster: creating postgresql database: $database"
			result=$(ssh $cluster_controlplane "kubectl run postgresql-dbcreate-$dblc --rm --tty -i --restart='Never' --namespace harbor --image bitnami/postgresql:11 --env="PGPASSWORD=$postgresql_passwd" --command -- psql -h $postgresql_release-postgresql-ha-pgpool -p 5432 -U postgres -d postgres -c 'CREATE DATABASE '$database';'" 2>&1)
			#echo "$database $pid: $result"
		done
	fi
}
function install_harbor() {
	echo "$me ($pid), $cluster: installing harbor"
	
	if [[ $simple_install == $false ]]; then
		#high availability install
		add_ha_postgres
	fi

	if [[ $clair_pvc == $true ]]; then
		#temp: clair eviction issue: add pvc
		echo "$me ($pid), $cluster: clair eviction issue - step 1, add pvc - scp $scriptpath/clair-pvc.yaml $cluster_controlplane:clair-pvc.yaml"
		result=$(scp $scriptpath/clair-pvc.yaml $cluster_controlplane:clair-pvc.yaml 2>&1)
		if [[ $result != $empty ]]; then
			end_result="failed to scp clair-pvc.yaml to controlplane: $result"
			result_value=88
			finish
		fi
		result=$(ssh $cluster_controlplane kubectl apply -f clair-pvc.yaml 2>&1)
		echo $result
		result=$(ssh $cluster_controlplane rm clair-pvc.yaml 2>&1)
	fi

	if [[ $simple_install == $false ]]; then
    	#add correct domainname to values yaml
		cp $scriptpath/helm-values.yaml $state_app_dir/tmp-helm-values.yaml
		sed -i "s/##postgreservice##/$postgresql_release-postgresql-ha-postgresql/g; s/##databasepassword##/$postgresql_passwd/g; " $state_app_dir/tmp-helm-values.yaml
	else
		cp $state_app_dir/helm-values-simple.yaml $state_app_dir/tmp-helm-values.yaml
	fi
	sed -i "s/##domainname##/$domain/g;" $state_app_dir/tmp-helm-values.yaml
    sed -i "s/##namespace##/$namespace/g;" $state_app_dir/tmp-helm-values.yaml

	#add values yaml to server
	echo "$me ($pid), $cluster: scp $state_app_dir/tmp-helm-values.yaml $cluster_controlplane:harbor-helm-values.yaml"
	result=$(scp $state_app_dir/tmp-helm-values.yaml $cluster_controlplane:harbor-helm-values.yaml 2>&1)
	if [[ $result != $empty ]]; then
		end_result="failed to scp harbor-helm-values to controlplane: $result"
		result_value=88
		finish
	fi

	if [[ $simple_install == $true ]]; then
		#no postgresql install: so check for installed helm releases here, if already there remove all and start again...
		helminstalls=$(ssh $cluster_controlplane "echo \$(helm -n harbor list | wc -l)")
		if [[ $helminstalls -gt 1 ]]; then
			if [[ $recursive -lt 3 ]]; then
				remove_old_harbor_install
				recursive=$(( $recursive + 1 ))
				install_harbor
			else
				end_result="failed to remove old harbor install from cluster, unable to install harbor, aboarding task"
				result_value=60
				finish
			fi
		fi
	fi

	echo "$me ($pid), $cluster: helm install $releasename -n $namespace harbor/harbor -f harbor-helm-values.yaml"
	result=$(ssh $cluster_controlplane helm install $releasename -n $namespace harbor/harbor -f harbor-helm-values.yaml 2>&1)
	if [[ $result != *"STATUS: deployed"* ]]; then
			end_result="installing harbor failed: $result"
			result_value=90
			finish
	fi
	result=$(ssh $cluster_controlplane rm harbor-helm-values.yaml)
	sleep 3

	if [[ $clair_pvc == $true ]]; then
		#temp: clair eviction issue: patch volumes and mount
		echo "$me ($pid), $cluster: clair eviction issue - step 2, add volume and mount - scp $scriptpath/clair-patch.json $cluster_controlplane:clair-patch.json"
		result=$(scp $scriptpath/clair-patch.json $cluster_controlplane:clair-patch.json 2>&1)
		if [[ $result != $empty ]]; then
			end_result="failed to scp clair-patch.json to controlplane: $result"
			result_value=88
			finish
		fi
		echo "$pid patching: kubectl -n $namespace patch deploy harbor-harbor-clair --patch \"\$(cat clair-patch.json)\""
		result=$(ssh $cluster_controlplane "kubectl -n $namespace patch deploy harbor-harbor-clair --patch \"\$(cat clair-patch.json)\"" 2>&1)
		echo $result
		result=$(ssh $cluster_controlplane rm clair-patch.json 2>&1)
	fi

	old_tls_files_present=$(ssh $cluster_controlplane "if [[ -f $core_domain-tls.yaml ]]; then echo 'true'; else echo 'false';fi")
	echo "$me ($pid), $cluster: old-tls-present is $old_tls_files_present, retain-tls is $retain_tls"
	if [[ $old_tls_files_present == $true ]]; then
		if [[ $retain_tls == $true ]]; then
			echo "$me ($pid), $cluster: reinstalling old tls certificates"
			result=$(ssh $cluster_controlplane kubectl -n $namespace apply -f $core_domain-tls.yaml)
			result=$(ssh $cluster_controlplane kubectl -n $namespace apply -f $notary_domain-tls.yaml)
		else
			echo "$me ($pid), $cluster: not retaining tls, retain tls is set to false"
		fi
		result=$(ssh $cluster_controlplane rm $core_domain-tls.yaml)
		result=$(ssh $cluster_controlplane rm $notary_domain-tls.yaml)
	fi

}
function remove_old_harbor_install() {
	echo "$me ($pid), $cluster: removing old harbor installation"

	postgresql_helm_release=($(ssh $cluster_controlplane "helm -n $namespace list | grep postgresql"))
	postgresql_helm_release=${postgresql_helm_release[0]}
	
	echo "$me ($pid), $cluster: 1 - backing up tls certificates"
	#harbor secret
	secret=$core-tls-secret
	key=$(ssh $cluster_controlplane "kubectl -n $namespace get secret -o=json $secret | jq -r '.data[\"tls.key\"]'")
	cert=$(ssh $cluster_controlplane "kubectl -n $namespace get secret -o=json $secret | jq -r '.data[\"tls.crt\"]'")
	if [[ $key != $empty ]]; then
		cp $scriptpath/tls-secret.yaml $state_app_dir/$core_domain-tls.yaml
		sed -i "s/##domain##/$core_domain/g; s/##secret##/$secret/g; s/##cert##/$cert/g; s/##key##/$key/g;" $state_app_dir/$core_domain-tls.yaml
		result=$(scp $state_app_dir/$core_domain-tls.yaml $cluster_controlplane:$core_domain-tls.yaml 2>&1)
		if [[ $result != $empty ]]; then
			end_result="failed to scp $core_domain-tls.yaml to controlplane: $result"
			result_value=31
			finish
		fi
	fi
	#notary secret
	secret=$notary-tls-secret
	key=$(ssh $cluster_controlplane "kubectl -n $namespace get secret -o=json $secret | jq -r '.data[\"tls.key\"]'")
	cert=$(ssh $cluster_controlplane "kubectl -n $namespace get secret -o=json $secret | jq -r '.data[\"tls.crt\"]'")
	if [[ $key != $empty ]]; then
		cp $scriptpath/tls-secret.yaml $state_app_dir/$notary_domain-tls.yaml
		sed -i "s/##domain##/$notary_domain/g; s/##secret##/$secret/g; s/##cert##/$cert/g; s/##key##/$key/g;" $state_app_dir/$notary_domain-tls.yaml
		result=$(scp $state_app_dir/$notary_domain-tls.yaml $cluster_controlplane:$notary_domain-tls.yaml 2>&1)
		if [[ $result != $empty ]]; then
			end_result="failed to scp $notary_domain-tls.yaml to controlplane: $result"
			result_value=31
			finish
		fi
	fi

	echo "$me ($pid), $cluster: 2 - deleting $namespace helm release $releasename"
	result=$(ssh $cluster_controlplane helm -n $namespace del $releasename)
	echo "$me ($pid), $cluster: 3 - deleting $namespace helm release $postgresql_helm_release"
	result=$(ssh $cluster_controlplane helm -n $namespace del $postgresql_helm_release)
	echo "$me ($pid), $cluster: 4 - extra: removing old wood, kubectl delete all,pvc -all"
	result=$(ssh $cluster_controlplane kubectl -n $namespace delete all,pvc --all)
	echo "$me ($pid), $cluster: 5 - deleting namespace $namespace"
	result=$(ssh $cluster_controlplane kubectl delete namespace $namespace)
	sleep 10
	validate_existence_namespace $namespace
}
#end functions
######################################
#intiating task variables
in_arguments=$@
argindex=1

minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

args=$(( $# + 1 ))
while [[ $argindex -lt $args ]]; do
	argument=$(echo $(eval echo "\${$argindex}"))  #read incomming argument to string
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
        if [[ $argument == "--do_not_retain_tls" ]]; then
            retain_tls=false
        fi
        if [[ $argument == "--simple" ]]; then
            simple_install=true
        fi
        if [[ $argument == "--clair-pvc" ]]; then
            clair_pvc=true
        fi
    else
		if [[ $domain == $empty ]]; then
			domain=$argument
		elif [[ $argument == *"w"* ]]; then
			worker_nodeslist=$worker_nodeslist" "$argument
		else
			controlplane_nodeslist=$controlplane_nodeslist" "$argument
		fi
	fi
    argindex=$(( $argindex + 1 ))
done
create_list_count controlplane_nodeslist
create_list_count worker_nodeslist
cluster_controlplane=($controlplane_nodeslist) #get first item of list
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
initiate_state

load_settings_k8s

#end initiating task variables
#######################################
report_start_to_file
core_domain=$core.$namespace.$domain
notary_domain=$notary.$namespace.$domain

validate_existence_namespace $namespace

add_helm_repo

register_fqdn $core.$namespace $domain
register_fqdn $notary.$namespace $domain

install_harbor

end_result="successfully added $app $type to cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish