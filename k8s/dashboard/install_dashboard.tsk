#!/bin/bash
#version:1.0
#
######################################
#arguments: domainname controlplane_nodes-logonlist workernodes-urllist --training
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
app=kubernetes-dashboard

end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
subdomain=dashboard
releasename=kubernetes-dashboard

variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
namespace=kube-system
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function install_dashboard() {
	echo "$me ($pid) installing k8s dashboard: ssh $cluster_controlplane helm repo add k8s-dashboard https://kubernetes.github.io/dashboard"
	result=$(ssh $cluster_controlplane helm repo add k8s-dashboard https://kubernetes.github.io/dashboard 2>&1)
	echo "$me ($pid), $cluster: installing k8s dashboard: helm repo update"
	result=$(ssh $cluster_controlplane "helm repo update" 2>&1)
	#echo "$me ($pid), repo update result: $result"

	echo "$me ($pid), installing dashboard: helm install $releasename k8s-dashboard/kubernetes-dashboard --namespace $namespace"
	result=$(ssh $cluster_controlplane helm install $releasename k8s-dashboard/kubernetes-dashboard --namespace $namespace 2>&1)
	echo "$me ($pid), helm install result: $result"
	if [[ $result != *"Get the Kubernetes Dashboard URL by running"* ]]; then
		if [[ $result == *"cannot re-use a name that is still in use"* ]]; then
			if [[ $reciproce -lt 3 ]]; then
				remove_old_dashboard_install
				reciproce=$(( $reciproce + 1 ))
				install_dashboard
			else
				end_result="failed to remove old kubernetes-dashboard install from cluster, unable to installing dashboard, aboarding task"
				result_value=60
				finish
			fi
		else
			end_result="dashboard failed: $result"
			result_value=90
			finish
		fi
	fi
}
function remove_old_dashboard_install() {
	echo "$me ($pid), $cluster: removing old dashboard installation, helm -n $namespace uninstall $releasename"
	result=$(ssh $cluster_controlplane helm -n $namespace uninstall $releasename)
}
#end functions
######################################
#intiating task variables
minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

training=$false
itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
    argument=$(echo $(eval echo "\${$itter}")) #read incomming argument to string
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
    else
        if [[ $domain == $empty ]]; then
            domain=$argument
		elif [[ $argument == *"w"* ]]; then
			worker_nodeslist=$worker_nodeslist" "$argument
		else
			controlplane_nodeslist=$controlplane_nodeslist" "$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
create_list_count logons
clusterdomains=${domain//#/ }
clusterdomains=($clusterdomains)
create_list_count clusterdomains
cluster_controlplane=($controlplane_nodeslist) #get first item of list
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
initiate_state

load_settings_k8s
#end initiating task variables
#######################################
report_start_to_file

echo "registering: $subdomain.$domain"
register_fqdn $subdomain $domain

install_dashboard

cp $scriptpath/ing.yaml $state_app_dir/$subdomain-$domain-ing.yaml
sed -i 's/#subdomain#/'$subdomain'/g; s/#domain#/'$domain'/g;' $state_app_dir/$subdomain-$domain-ing.yaml
sed -i 's/#release#/'$releasename'/g;' $state_app_dir/$subdomain-$domain-ing.yaml

echo "$me ($pid), $cluster: adding dashboard ingress"
result=$(scp $state_app_dir/$subdomain-$domain-ing.yaml $cluster_controlplane:$subdomain-$domain-ing.yaml 2>&1)
if [[ $result != "" ]]; then
	end_result="failed to scp $subdomain-$domain-ing.yaml to controlplane: $result"
	result_value=91
	finish
fi
result=$(ssh $cluster_controlplane "kubectl apply -f $subdomain-$domain-ing.yaml")
if [[ $result != *"ingress"* && $result != *"created"* ]]; then
    if [[ $result != *"ingress"* && $result != *"unchanged"* ]]; then
		end_result="failed adding $subdomain-$domain-ing to controlplane: $result"
		result_value=92
		finish
	fi
fi
result=$(ssh $cluster_controlplane "rm $subdomain-$domain-ing.yaml")

end_result="successfully installed $app on cluster $cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish