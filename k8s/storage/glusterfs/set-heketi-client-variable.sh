namespace=storage
pods=$(kubectl -n $namespace get pods -o wide)
pods=(${pods// /#})
#get glusterfs ips
g_ips=$empty
for pod in ${pods[@]}; do
	if [[ $pod == *"heketi"* ]]; then #only glusterfs pods
		pod=(${pod//#/ })
		g_ips=$g_ips" "${pod[5]}
	fi
done
heketi_cli_ip=($g_ips)
heketi_cli_ip=${heketi_cli_ip[0]}
heketi_cli_server="http://$heketi_cli_ip:8080"
echo "export HEKETI_CLI_SERVER=$heketi_cli_server"
eval export HEKETI_CLI_SERVER=$heketi_cli_server