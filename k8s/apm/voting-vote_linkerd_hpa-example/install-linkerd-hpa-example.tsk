#!/bin/bash
#version:1.0
#
######################################
#arguments: domainname controlplane_nodes-logonlist workernodes-urllist
##########################################
#set general variables
source .generic-code/variables/start-variables
source .generic-code/variables/k8s-variables
task=install_helm
app=linkerd-hpa-example
end_result=$empty
result_value=0
register_domain_task=.utility/dns-internet/register_dme_domain.tsk
loadbalancer_passthrough_task=.utility/loadbalancer-wrappers/cluster/add-endpoint.tsk
subdomain=linkerd-viz
releasename=linkerd-hpa-example

variable_by_string=$empty
cluster_controlplane=$empty
controlplane_nodeslist=$empty
worker_nodeslist=$empty
register_result=$empty
training=$empty
initiated_scripts=0
pids=$empty
cluster=$empty
kuber_precursor=$empty
state_app_dir=$empty
arguments=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
source .generic-code/functions/k8s-provisioning-functions
function initiate_state() {
    echo "$me ($pid), $cluster - function initiate state"
	k8s_initiate

    state_app_dir=$state_dir/$cluster/software/cluster-software/$app
    progress_file=$state_app_dir/.progress

    if [[ -d $state_app_dir ]]; then rm -rf $state_app_dir; fi
    mkdir -p $state_app_dir
    touch $progress_file
    progress_dir=$(dirname $progress_file)
	runtime_state_file=$state_dir/$cluster/$runtime_state
	if [[ ! -f $runtime_state_file ]]; then touch $runtime_state_file; fi
}
function secure_adapter_repo_presence() {
	repo_present=$(ssh $cluster_controlplane "helm repo list | grep prometheus-community" 2>&1)
	if [[ $repo_present == $empty || $repo_present == *"Error"* ]]; then
		echo "$me ($pid), $cluster: missing elastic repo, adding: helm repo add prometheus-community https://prometheus-community.github.io/helm-charts"
		result=$(ssh $cluster_controlplane "helm repo add prometheus-community https://prometheus-community.github.io/helm-charts" 2>&1)
		echo $result

		echo "$me ($pid), $cluster: updating helm repos"
		result=$(ssh $cluster_controlplane "helm repo update" 2>&1)
		echo $result
	fi
}
#end functions
######################################
#intiating task variables
minimal_number_of_arguments=3
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

training=$false
itter=1
end=$(( $# + 1 ))
while [[ $itter -lt $end  ]]; do
    argument=$(echo $(eval echo "\${$itter}")) #read incomming argument to string
	#echo "$itter: argument is $argument"
    if [[ $argument == *"--"* ]]; then
        if [[ $argument == "--training" ]]; then
            training=$true
        fi
        if [[ $argument == "--logon-needs-k" ]]; then
            kuber_logon_required=$true
            kuber_precursor=k
        fi
        if [[ $argument == *"--routerip"* ]]; then
			routerip=(${argument/=/ })
            routerip=${routerip[1]}
        fi
        if [[ $argument == "--reinstall" ]]; then
            reinstall=true
        fi
    else
        if [[ $domain == $empty ]]; then
            domain=$argument
		elif [[ $argument == *"w"* ]]; then
			worker_nodeslist=$worker_nodeslist" "$argument
		else
			controlplane_nodeslist=$controlplane_nodeslist" "$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
create_list_count logons
clusterdomains=${domain//#/ }
clusterdomains=($clusterdomains)
create_list_count clusterdomains
cluster_controlplane=($controlplane_nodeslist) #get first item of list
cluster=($controlplane_nodeslist)
cluster=(${cluster//-/ })
cluster=${cluster[1]}
if [[ $kuber_logon_required == $true ]]; then cluster_controlplane=$kuber_precursor$cluster_controlplane; fi
initiate_state

load_settings_k8s
#end initiating task variables
#######################################
report_start_to_file

#add prometheus adapter
secure_adapter_repo_presence
echo "$me ($pid), $cluster: adding linkerd-prometheus-adapter.yaml to $cluster_controlplane"
result=$(scp $scriptpath/linkerd-prometheus-adapter.yaml $cluster_controlplane:linkerd-prometheus-adapter.yaml 2>&1)
if [[ $result != $empty ]]; then
	end_result="failed to scp $scriptpath/linkerd-prometheus-adapter.yaml to controlplane: $result"
	result_value=15
	finish
fi
echo "$me ($pid), $cluster: installing prometheus adapter"
result=$(ssh $cluster_controlplane "helm install linkerd-prometheus-adapter prometheus-community/prometheus-adapter -n linkerd-viz -f linkerd-prometheus-adapter.yaml" 2>&1)
if [[ $result != *"has been deployed"* ]]; then
	end_result="failed injecting linkerd proxy into the default namespace deployments. returned result: $result"
	result_value=16
	finish
fi
echo "$me ($pid), $cluster: prometheus adapter installed"
result=$(ssh $cluster_controlplane rm linkerd-prometheus-adapter.yaml)

echo "$me ($pid), $cluster: adding voting-hpa.yaml to $cluster_controlplane"
result=$(scp $scriptpath/voting-hpa.yaml $cluster_controlplane:voting-hpa.yaml 2>&1)
if [[ $result != $empty ]]; then
	end_result="failed to scp $scriptpath/voting-hpa.yaml to controlplane: $result"
	result_value=15
	finish
fi
result=$(ssh $cluster_controlplane "kubectl apply -f voting-hpa.yaml" 2>&1)
if [[ $result != *"HorizontalPodAutoscaler"* && $result != *"created"* ]]; then
	if [[ $result != *"HorizontalPodAutoscaler"* && $result != *"unchanged"* ]]; then
		end_result="failed adding voting-application hpa: $result"
		result_value=16
		finish
	fi
fi
echo "$me ($pid), $cluster: added voting-application hpa to cluster"
result=$(ssh $cluster_controlplane rm voting-hpa.yaml)

end_result="successfully installed $app example on cluster $cluster ($controlplane_nodeslist $worker_nodeslist). $register_result"
result_value=0
finish