#!/bin/bash
#version:1.0
#
######################################
#arguments: cluster state_storage_dir(from .state/... onwards)
##########################################
#set general variables
source .generic-code/variables/start-variables
minimal_number_of_arguments=0
end_result=$empty
result_value=0

cri_unix_socket=$empty
internal_ip=$empty
external_ip=$empty
cluster=$empty
servers=$empty
node=$empty
kc_state_dir=$empty
enable_external_interface_on_apiserver=$false
logon_internal_ip=$empty
minimal_number_of_arguments=0
external_extra_sans_ip_list=$empty
internal_extra_sans_list=$empty
retrieved_interfaces=$empty
sans_teller=0
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function report_start_to_file() {
  reporting="pid $app $pid executing: $me $*"
  echo -e "${GREEN}$reporting${NC}"
  echo $reporting >> $progress_file
}
function cleanup_joinstatement() {
  #initiation (controlplane) function
	text=$*

	text=${text// \\/} #removed code enters
	text=${text// \ /} #removed code enters
	text=${text//$'\n'/ } #put all on one line
	text=${text//$'\r'/ } #put all on one line
	text=${text// /###} #removed white spaces so it can be used as argument later
	text=${text//###\\/###} #removed code enters
	text=${text//###/ } #return white spaces

	echo $text
}
function get_kubeadm_version() {
	kubeadm_version=$(ssh $logon "kubeadm version")
	kubeadm_version=${kubeadm_version#*GitVersion:\"} #get all after
	kubeadm_version=${kubeadm_version%%\",*} #get all before first ,
	kubeadm_version=${kubeadm_version:1} #remove first character
	echo $kubeadm_version
}
function retrieve_interface() {
	#pattern needs to end with a dot!
	retrieved_interfaces=$empty
	ilogon=$1
	pattern=$2
	disqualifiers=$3
	disqualifiers=${disqualifiers//#/ }

	first_lob=${pattern%%.*} #get all before first .
	rest_pattern=${pattern#*.} #get all after .
	second_lob=${rest_pattern%%.*} #get all before first .
	rest_pattern=${rest_pattern#*.} #get all after .
	if [[ $second_lob != $empty ]]; then
		third_lob=${rest_pattern%%.*} #get all before first .
		if [[ $third_lob != $empty ]]; then
			s_pattern="\"inet $first_lob\.$second_lob\.$third_lob\.\""
		else
			s_pattern="\"inet $first_lob\.$second_lob\.\""
		fi
	else
		s_pattern="\"inet $first_lob\.\""
	fi
	#echo "$me ($pid), retrieve_interface - $pattern: executed search - ssh -o StrictHostKeyChecking=accept-new $ilogon \"ip a | grep $s_pattern\""
	results=$(ssh $ilogon -o StrictHostKeyChecking=accept-new "ip a | grep $s_pattern")

	results=${results// /#}
	for entry in ${results[@]}; do
		disqualified=$false
		entry=${entry//#/ }
		for disqualifier in $kubeadm_config_interface_diskwalifiers; do
			if [[ $entry == *"$disqualifier"* ]]; then
				disqualified=$true
			fi
		done
		if [[ $disqualified == $false ]]; then
			for item in ${entry[@]}; do
				if [[ $item == *"$pattern"* ]]; then
					if [[ $item == *"/"* ]]; then
						item=${item%%\/*} #get all before first /
						retrieved_interfaces=$retrieved_interfaces" "$item
					fi
				fi
			done
		fi
	done
	create_list_count retrieved_interfaces
	if [[ $retrieved_interfaces_count -ne 1 ]]; then
		end_result="$nbf - gkc01 - failed retrieving internal ip address for logon $ilogon, on pattern $pattern. found result: $retrieved_interfaces"
		result_value=98
		finish
	fi
}
function retrieve_logon_internal_ip() {
	ilogon=$1
	
	if [[ $high_available == $true ]]; then
		if [[ $production_cluster == $true && $hybride_cluster == $true ]]; then
			#wiregaurd stiged
			retrieve_interface $ilogon 172.16. $kubeadm_config_interface_diskwalifiers
		elif [[ $virtualisation_type == "google-training" ]]; then
			#wiregaurd stiged
			retrieve_interface $ilogon 172.16. $kubeadm_config_interface_diskwalifiers
		else
			retrieve_interface $ilogon 192.168.1. $kubeadm_config_interface_diskwalifiers
		fi
	else
		if [[ $virtualisation_type == "google-training" ]]; then
			retrieve_interface $ilogon 10. $kubeadm_config_interface_diskwalifiers
		elif [[ $production_cluster == $true && $hybride_cluster == $true ]]; then
			retrieve_interface $ilogon 172.16. $kubeadm_config_interface_diskwalifiers
		else
			retrieve_interface $ilogon 192.168.1. $kubeadm_config_interface_diskwalifiers
		fi
	fi

	internal_ip=$retrieved_interfaces
}
function set_internal_extra_sans_ips() {
	#ha get masters internal ips
	for server in $servers; do
		masternode=$false
		if [[ $server == *"m"* ]]; then masternode=$true; fi
		if [[ $kuber_logon_required == $true ]]; then server=k$server-$cluster; fi

		if [[ $masternode == $true && $server != $logon ]]; then
			retrieve_logon_internal_ip $server
			internal_extra_sans_list=$internal_extra_sans_list" "$internal_ip
		fi
	done
}
function get_hostip() {
	external_ip=$empty
    in_url=$1
    host_result=$(host $in_url)
    if [[ $host_result == *"127.0"* ]]; then
		end_result="$nbf - gkc02 - failed retrieving external interface ip for $in_url. result: $host_result"
		result_value=11
		finish
    else
      if [[ $host_result == *"has address"* ]]; then #if ipadress returned
				#get last entry (which is ip)
				for item in $host_result; do
					external_ip=$item
				done
      else
		end_result="$nbf - gkc03 - failed retrieving external interface ip for $in_url. result: $host_result"
		result_value=12
		finish
      fi
    fi
}
function set_external_extra_sans_ips() {
	for server in $servers; do
		masternode=$false
		if [[ $server == *"m"* ]]; then masternode=$true; fi
		if [[ $kuber_logon_required == $true ]]; then server_logon=k$server-$cluster; fi

		if [[ $masternode == $true && $server_logon != $logon ]]; then
			fqdn_domain=$server.$cluster.$external_interface_host_tld
			get_hostip $fqdn_domain
			external_extra_sans_ip_list=$external_extra_sans_ip_list" "$external_ip
		fi
	done
}
function add_hosts_endpoints_as_extra_sans() {
	for server in $servers; do
		if [[ $server == *"m"* ]]; then
			internal_extra_sans_list=$internal_extra_sans_list" "$server"."$cluster".n-able.hosts"
		fi
	done
}
function add_extra_sans_to_config() {
	extra_sans_list=$*

	for extra_sans in $extra_sans_list; do
		if [[ $sans_teller -eq 0 ]]; then
			cat <<EOF >> $kc_state_dir/kubeadm-config.yaml
apiServer:
  certSANs:
EOF
		fi
		cat <<EOF >> $kc_state_dir/kubeadm-config.yaml
  - "$extra_sans"
EOF
		sans_teller=$(( $sans_teller + 1 ))
	done
}
function create_kubeadm_config() {

cat <<EOF > $kc_state_dir/kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
kubernetesVersion: $(get_kubeadm_version)
clusterName: "$cluster"
networking:
  dnsDomain: $kubernetes_tld
EOF

if [[ $set_cidrs_during_kubeadm_init == $true ]]; then
cat <<EOF >> $kc_state_dir/kubeadm-config.yaml
  serviceSubnet: $kubernetes_service_cidr
  podSubnet: $kubernetes_pod_cidr
EOF
fi

if [[ $high_available == $true ]]; then
cat <<EOF >> $kc_state_dir/kubeadm-config.yaml
controlPlaneEndpoint: $logon_internal_ip:$ha_proxy_lb_port
EOF
fi

add_extra_sans_to_config $internal_extra_sans_list
add_extra_sans_to_config $external_extra_sans_ip_list

cat <<EOF >> $kc_state_dir/kubeadm-config.yaml
---
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
localAPIEndpoint:
  advertiseAddress: $logon_internal_ip
  bindPort: $api_server_port
nodeRegistration:
  criSocket: $cri_unix_socket
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: $cgroup_driver
EOF

}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=2
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
max_arguments=$(( $# + 1 ))
while [[ $itter -lt $max_arguments ]]; do
    argument=$(eval echo "\${$itter}")
    if [[ $argument == "--initiate" ]]; then
        initiate_cluster=$true
    else
        if [[ $cluster == $empty ]]; then
            cluster=$argument
        elif [[ $kc_state_dir == $empty ]]; then
            kc_state_dir=$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
load_cluster_and_default_settings
set_servernames

node=($servers)
if [[ $virsh_remote_rack == $true && $kuber_logon_required == $true ]]; then node=k$node; fi
logon=$node-$cluster

if [[ $kubernetes_container_runtime == "docker" ]]; then
	cri_unix_socket=$docker_socket
elif [[ $kubernetes_container_runtime == "crio" ]]; then
	cri_unix_socket=$crio_socket
elif [[ $kubernetes_container_runtime == "containerd" ]]; then
	cri_unix_socket=$containerd_socket
fi

progress_dir="$kc_state_dir/$cluster/initiation"
if [[ ! -d $progress_dir ]]; then mkdir -p $progress_dir; fi
progress_file=$progress_dir/.progress
if [[ ! -f $progress_file ]]; then touch $progress_file; fi
if [[ -f $kc_state_dir/kubeadm-config.yaml.old ]]; then rm  $kc_state_dir/kubeadm-config.yaml.old; fi
if [[ -f $kc_state_dir/kubeadm-config.yaml ]]; then mv $kc_state_dir/kubeadm-config.yaml $kc_state_dir/kubeadm-config.yaml.old; fi

kubernetes_release=${kubernetes_version//\*/}
kubernetes_release=(${kubernetes_release//./ })
kubernetes_major_version=${kubernetes_release[0]}
kubernetes_minor_release=${kubernetes_release[1]}
#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"

retrieve_logon_internal_ip $logon
logon_internal_ip=$internal_ip

if [[ $high_available == $true ]]; then #HA setup
	set_internal_extra_sans_ips
	if [[ $virtualisation_type == "virsh" ]]; then
		add_hosts_endpoints_as_extra_sans
	fi
fi
if [[ $virtualisation_type == "google-training" ]]; then #always setup external extra sans for training
	set_external_extra_sans_ips
elif [[ $enable_external_interface_on_apiserver == $true ]]; then
	set_external_extra_sans_ips
fi

create_kubeadm_config

end_result="successfully created $kc_state_dir/kubeadm-config.yaml"
result_value=0
finish