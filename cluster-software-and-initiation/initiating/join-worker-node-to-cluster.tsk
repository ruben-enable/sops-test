#!/bin/bash
#version:1.0
#
######################################
#arguments: node cluster 
##########################################
#set general variables
source .generic-code/variables/start-variables
end_result=$empty
result_value=0


join_statement=$empty
initiated_scripts=0
pids=$empty
cluster=$empty
internal_ip=$empty
ha_setup=$false
cpserver_join=$empty
servers=$empty
arguments=$empty
cri_unix_socket=$empty
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function cleanup_joinstatement() {
  #initiation (controlplane) function
	text=$*

	text=${text// \\/} #removed code enters
	text=${text// \ /} #removed code enters
	text=${text//$'\n'/ } #put all on one line
	text=${text//$'\r'/ } #put all on one line
	text=${text// /###} #removed white spaces so it can be used as argument later
	text=${text//###\\/###} #removed code enters
	text=${text//###/ } #return white spaces

	echo $text
}
function update_lb_access() {
    website_subdomain=$1
    echo "bash .utility/loadbalancer-wrappers/cluster/remove-endpoint.tsk $website_subdomain $cluster_operational_tld"
    bash .utility/loadbalancer-wrappers/cluster/remove-endpoint.tsk $website_subdomain $cluster_operational_tld
    echo "bash .utility/loadbalancer-wrappers/cluster/add-endpoint.tsk $website_subdomain $cluster_operational_tld $cluster"
    bash .utility/loadbalancer-wrappers/cluster/add-endpoint.tsk $website_subdomain $cluster_operational_tld $cluster
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=1
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
max_arguments=$(( $# + 1 ))
while [[ $itter -lt $max_arguments ]]; do
    argument=$(eval echo "\${$itter}")
    if [[ $argument == "--initiate" ]]; then
        initiate_cluster=$true
    else
        if [[ $node == $empty ]]; then
            node=$argument
        elif [[ $cluster == $empty ]]; then
            cluster=$argument
        fi
    fi
    itter=$(( $itter + 1 ))
done
logon=$node-$cluster

load_cluster_and_default_settings
set_servernames
if [[ $control_planes -gt 1 ]]; then ha_setup=$true; fi
if [[ $kubernetes_container_runtime == "docker" ]]; then
	cri_unix_socket=$docker_socket
elif [[ $kubernetes_container_runtime == "crio" ]]; then
	cri_unix_socket=$crio_socket
elif [[ $kubernetes_container_runtime == "containerd" ]]; then
	cri_unix_socket=$containerd_socket
fi

progress_dir="$state_dir/$cluster/initiation"
if [[ ! -d $progress_dir ]]; then mkdir -p $progress_dir; fi
progress_file=$progress_dir/.progress
cluster_initiation_dir=$progress_dir/cluster-initiation
if [[ ! -d $cluster_initiation_dir ]]; then mkdir -p $cluster_initiation_dir; fi
kubernetes_release=${kubernetes_version//\*/}
kubernetes_release=(${kubernetes_release//./ })
kubernetes_major_version=${kubernetes_release[0]}
kubernetes_minor_release=${kubernetes_release[1]}
#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"

if [[ ! -f $cluster_initiation_dir/worker_join ]]; then
	end_result="$nbf - jwntc01 - error: failure because missing file $cluster_initiation_dir/worker_join, unable to continue, aboarding $cluster initiation."
	result_value=99
	finish
fi

worker_join=$(cat $cluster_initiation_dir/worker_join)

if [[ $kubernetes_major_version -eq 1 && $kubernetes_minor_release -gt 23 ]]; then
	#multiple cri endpoints on server: add cri-socket argument
	worker_join=$worker_join" --cri-socket "$cri_unix_socket
fi
echo "$worker_join" > $cluster_initiation_dir/$node-join

join=$(ssh $logon $worker_join 2>&1)
echo "$join" > $cluster_initiation_dir/$node-join.log
if [[ $join != *"This node has joined the cluster"* ]]; then
	end_result="$nbf - jcntc02 - node $node failed to join $cluster.error-result: $join"
	result_value=98
	finish
fi

end_result="workernode $node successfully joined cluster $cluster"
result_value=0
finish