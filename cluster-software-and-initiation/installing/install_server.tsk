#!/bin/bash
#version:1.0
#
######################################
#arguments: node cluster --initiate
#printresults is optional: default is false. Exepected values: true/false
##########################################
#set general variables
source .generic-code/variables/start-variables
server_script_dir=$scriptpath/install_serversoftware_scripts

server_software_dir=$empty
node=$empty
cluster=$empty
logon=$empty
end_result=$empty
result_value=0
arguments=$empty
start_time=0
time_taken=0
#end general variables
##########################################
#functions
source .generic-code/functions/shared-functions
function get_distribution_release() {
  grep=$1
  echo $(ssh $logon "sudo cat /etc/*-release | grep $grep" 2>&1)
}
function return_distribution_version() {
  result=$(get_distribution_release VERSION_ID)
  result=${result//\"/}
  result=${result//VERSION_ID=/}
  echo $result
}
function return_distribution_name() {
  result=$(get_distribution_release ID)
  for entry in $result; do 
        key=${entry%=*} #get all before =
        value=${entry#*=} #get all after =
        if [[ $key == "ID" ]]; then
            echo $value
        fi
  done
}
function prepare_ha_in_os() {
        server_install_script=$logon.ha-controlplanes.lb-on-vm
        i_server_install_script=$logon.ha-controlplanes.lb-on-vm.$kubernetes_container_runtime.tsk
        cp $server_script_dir/ha-controlplanes.lb-on-vm.$kubernetes_container_runtime.tsk $server_software_dir/$i_server_install_script
        sed -i 's/#router-id#/'$router_id'/g;' $server_software_dir/$i_server_install_script
        sed -i 's/#authentication-pass#/'$authentication_pass'/g;' $server_software_dir/$i_server_install_script
        sed -i 's/#virtual-ip#/'$floating_ip'/g;' $server_software_dir/$i_server_install_script
        sed -i 's/#ha-interface#/'$ha_interface'/g;' $server_software_dir/$i_server_install_script
        sed -i 's/#ha-proxy-lb-port#/'$ha_proxy_lb_port'/g;' $server_software_dir/$i_server_install_script
        sed -i 's/#api-server-port#/'$api_server_port'/g;' $server_software_dir/$i_server_install_script
        if [[ $node = "m1" ]]; then
            sed -i 's/#state#/MASTER/g;' $server_software_dir/$i_server_install_script
            sed -i 's/#priority#/101/g;' $server_software_dir/$i_server_install_script
            first=$false
        else
            sed -i 's/#state#/BACKUP/g;' $server_software_dir/$i_server_install_script
            sed -i 's/#priority#/100/g;' $server_software_dir/$i_server_install_script
        fi
        itter=1
        for cp_node in $controlplane_nodes; do
            sed -i 's/#host'$itter'-id#/'$cp_node'-'$clustername'/g; s/#host'$itter'-address#/'$cp_node'.'$clustername'.'$local_hardwaredomain'/g;' $server_software_dir/$i_server_install_script
            
            itter=$(( $itter + 1 ))
        done
        #remove not used hosts
        while [[ $itter -lt 12 ]]; do
            sed -i '/#host'$itter'-id#/d; /#host'$itter'-address#/d;' $server_software_dir/$i_server_install_script
            sed -i '/\${host'$itter'_id}/d' $server_software_dir/$i_server_install_script

            itter=$(( $itter + 1 ))
        done
}
function update_server() {
    teller=0;success=$false
    
    #unlock when dependencies are unsolved
    echo "$me ($pid), $node.$cluster: unlocking dependencies"
    echo $(ssh $logon "sudo dpkg --configure -a"  2>&1) > $needrestart_software_dir/$node.$cluster-dpkg.result

    #disable needrestart messages
    echo "$me ($pid), $node.$cluster: disable needrestart pop-ups"
    disable_popups_script=$needrestart_software_dir/disable_popups.tsk
    cat << EOF > $disable_popups_script
#!/bin/bash
sed -i "s/#NR_NOTIFYD_DISABLE_NOTIFY_SEND='1'/NR_NOTIFYD_DISABLE_NOTIFY_SEND='1'/g;" /etc/needrestart/notify.conf
EOF
    result=$(scp $disable_popups_script $logon:disable_popups.tsk 2>&1)
    if [[ $result != $empty ]]; then
        end_result="$nbf - is01 - error, failed to scp $disable_popups_script script to server $logon, result received: $result"
        result_value=91
        finish
    fi
    echo $(ssh $logon "sudo bash disable_popups.tsk"  2>&1) > $needrestart_software_dir/$node.$cluster-disable-popups.result
    result=$(cat $needrestart_software_dir/$node.$cluster-disable-popups.result)
    if [[ $result != $empty ]]; then
        end_result="$nbf - is02 - error, disabling needrestart pop-ups on server $logon failed, result received: $result"
        result_value=90
        finish
    fi
    result=$(ssh $logon "rm disable_popups.tsk" 2>&1)

    teller=0
    while [[ $success == $false && $teller -lt 30 ]]; do
        teller=$(( $teller + 1 ))

        echo "$me ($pid), $node.$cluster: updating server $logon (attempt $teller)"
        echo $(ssh $logon "sudo DEBIAN_FRONTEND=noninteractive apt-get update"  2>&1) > $needrestart_software_dir/$node.$cluster-apt-update.result
        
        echo "$me ($pid), $node.$cluster: upgrading server $logon (attempt $teller)"
        echo $(ssh $logon "sudo DEBIAN_FRONTEND=noninteractive apt-get upgrade -y"  2>&1) > $needrestart_software_dir/$node.$cluster-apt-upgrade.result
        result=$(cat $needrestart_software_dir/$node.$cluster-apt-upgrade.result)
        #echo "### update-server-result ($teller) ###"
        #echo $result
        #echo "### update-server-result end ($teller) ###"
        if [[ $result != *"Unable to locate package"* && $result != *"Could not get lock"* && $result != *"Unable to acquire the dpkg frontend lock"* ]]; then
            success=$true
        else 
            echo "$me ($pid), $node.$cluster: upgrading server $logon locked (attempt $teller)"
            sleep 10
        fi
    done
    

    if [[ $success == $false ]]; then
        echo "$me ($pid), $node.$cluster: error upgrading server $logon, dpkg is locked unable to update server"
        exit 99
    fi
}
function wait_server_to_be_responsive(){
    successfull_logon=$false
    while [[ $successfull_logon == $false ]]; do
        result=$(ssh $logon "echo 'success'" 2>&1)
        if [[ $result == "success" ]]; then
            successfull_logon=$true
        else
            #echo "$me ($pid), $node.$cluster: testing ssh logon, result is $result"
            sleep 3
        fi
    done
}
#end functions
######################################
#intiating task variables

minimal_number_of_arguments=2
if [[ "$#" -lt $minimal_number_of_arguments ]];then
	invalid_number_of_arguments
fi

itter=1
max_arguments=$(( $# + 1 ))
while [[ $itter -lt $max_arguments ]]; do
    argument=$(eval echo "\${$itter}")
    if [[ $argument == "--initiate" ]]; then
        initiate_cluster=$true
    else
        if [[ $node == $empty ]]; then
            node=$argument
        elif [[ $cluster == $empty ]]; then
            cluster=$argument
            clustername=$cluster
        fi
    fi
    itter=$(( $itter + 1 ))
done
#echo "$me ($pid), node $node, clustername $clustername"

load_cluster_and_default_settings
if [[ $cluster == "default" ]]; then clustername=$cluster_name; fi
logon=$node-$clustername
set_servernames

progress_dir="$state_dir/$cluster/software"
progress_file=$progress_dir/.progress
if [[ ! -f $progress_file ]]; then touch $progress_file; fi
server_software_dir=$progress_dir/server-software
if [[ ! -d $server_software_dir ]]; then mkdir -p $server_software_dir; fi
dist_version=$(return_distribution_version)
dist=$(return_distribution_name)
#end initiating task variables
#######################################
echo "pid $app $pid executing: $me $*"

#copy server install script to server
if [[ $high_available == $true && $node == *"m"* ]]; then
    if [[ $api_lb_option == "external" ]]; then
        prepare_ha_in_os
    else
        server_install_script=ha-controlplanes
    fi
else
    server_install_script=k8s
fi
server_install_script=$server_install_script.$kubernetes_container_runtime.tsk
echo -e "${EXCLAMATION} ${LIGHT_CYAN}${ITALIC}$me ($pid), for $node.$clustername: server-install-script is ${NC}${LIGHT_BLUE}${BOLD}$server_install_script${NC}"

echo "$me ($pid), for $node.$clustername: copying provisioning file $server_install_script to server"

#provide generic code
result=$(ssh $logon "mkdir .generic-code" 2>&1)
echo "scp -r .generic-code/* $logon:.generic-code"
result=$(scp -r .generic-code/* $logon:.generic-code 2>&1)

if [[ $high_available == $true && $node == *"m"* ]]; then
    echo "scp $server_software_dir/$server_install_script $logon:server_provisioning.tsk"
    result=$(scp $server_software_dir/$server_install_script $logon:server_provisioning.tsk 2>&1)
    if [[ $result != $empty ]]; then
        end_result="$nbf - is03 -error, failed to scp $node.$clustername server_provisioning (file:$server_software_dir/$server_install_script), result received: $result"
        result_value=91
        finish
    fi
else
    if [[ $result != $empty ]]; then
        end_result="$nbf - is06 - error, failed to scp .generic-code directory to server $logon, result received: $result"
        result_value=91
        finish
    fi

    echo "scp $server_script_dir/$server_install_script $logon:server_provisioning.tsk"
    result=$(scp $server_script_dir/$server_install_script $logon:server_provisioning.tsk 2>&1)
    if [[ $result != $empty ]]; then
        end_result="$nbf - is04 - error, failed to scp $node.$clustername server_provisioning (file:$server_script_dir/$server_install_script), result received: $result"
        result_value=91
        finish
    fi
fi

#execute install
#22.04 and up - do upgrade from remote to compensate for needrestart behavoir
if [[ $dist == "ubuntu" ]]; then
    dist_year=${dist_version%.*} #get all before =
    if [[ $dist_year -gt 21 ]]; then
        echo -e "$me ($pid), $node.$cluster: ${BLUE}NEEDRESTART active in distribution: upgrading from control server${NC}"
        needrestart_software_dir=$server_software_dir/needrestart_solution
        if [[ ! -d $needrestart_software_dir ]]; then mkdir -p $needrestart_software_dir; fi

        update_server

        echo "$me ($pid), $node.$cluster: check on needinstall"
        echo $(ssh $logon "sudo needrestart -u NeedRestart::UI::stdio -r l"  2>&1) > $needrestart_software_dir/$node.$cluster-needinstall-check.result
        result=$(cat $needrestart_software_dir/$node.$cluster-needinstall-check.result)

        if [[ $result == *"Pending kernel upgrade"* ]]; then #reboot needed
            echo -e "$me ($pid), $node.$cluster: ${RED}restart required - rebooting server $logon${NC}"
            result=$(ssh $logon "sudo reboot now" 2>&1)
            sleep 10

            wait_server_to_be_responsive
            echo "$me ($pid), $node.$cluster: server $logon has been rebooted"
        fi
        
        echo -e "$me ($pid), $node.$cluster: ${BLUE}NEEDRESTART active in distribution: upgrading from control server $logon - ${NC}${WHITE}execution ended${NC}"
    fi
fi

start_time=$(get_now)
#run server script on server
touch $server_software_dir/$node.$clustername-server-install-command
echo "bash server_provisioning.tsk $node.$clustername $kubernetes_version" > $server_software_dir/$node.$clustername-server-install-command
if [[ $stop_before_infrastructure_server_install == $true ]]; then #testing: leave and stop execution before installing servers with server-scripts
    end_result="'stop_before_infrastructure_server_install', early aboard ordered"
    result_value=15
    finish
fi

#execute install
argumentslist=$empty
echo "$me ($pid), $node.$clustername: starting server provisioning"
for argument in $server_installscript_argumentslist; do
    value=$(eval echo "\$$argument")
    echo -e "$me ($pid), $node.$clustername: ${ORANGE}key-$argument, value-$value${NC}"
    argumentslist=$argumentslist" "$value
done
result=$(ssh $logon "bash server_provisioning.tsk $node.$clustername $kubernetes_version" 2>&1)

result=$(ssh $logon "if [[ -f .server-install.log ]]; then echo 'successfull'; else echo 'failed'; fi" 2>&1)
if [[ $result != "successfull" ]]; then
    end_result="$nbf - is06 - error: server $node.$clustername provisioning file .server-install.log is missing"
    result_value=92
    finish
fi
time_taken=$(delta_time $timer_start_time)
echo -e "$me ($pid) - $logon: ${ITALIC}${UNDERLINE}${WHITE}installing server script has taken $time_taken seconds${NC}"
# echo "#### $node.$clustername - INSTALL-SCRIPT ERRORS ####"
# echo "$node.$clustername result is $result"
# echo "#### $node.$clustername - END INSTALL-SCRIPT ERRORS ####"
result=$(scp $logon:.server-install.log $server_software_dir/$node.$clustername-server-install.log 2>&1)
result=$(ssh $logon "rm .server-install.log" 2>&1)
result=$(cat $server_software_dir/$node.$clustername-server-install.log)
##reflect time taken
if [[ $result != *"successfull"* ]]; then
    end_result="$nbf - is05 - error: provisioning server $node.$clustername failed, returned-result: $result"
    result_value=92
    finish
fi

end_result="successfully provisioned server $node.$clustername"
result_value=0
finish